/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.writer;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {
	private static final String OUTPUT = "output";
	private static final String COPY_SOURCE = "copySource";

	public enum PROTOBUF_OUTPUT {
		TEXT,
		JSON,
		BINARY
	}

	public Parameters() {
		super();
	}

	@Override
	public void reset() {
		super.reset();
		setOutput(PROTOBUF_OUTPUT.TEXT);
		setCopySource(false);
	}

	public void setOutput(PROTOBUF_OUTPUT output) {
		setString(OUTPUT, output.name());
	}
	
	public PROTOBUF_OUTPUT getOutput() {
		return PROTOBUF_OUTPUT.valueOf(getString(OUTPUT));
	}

	public void setCopySource(boolean copySource) {
		setBoolean(COPY_SOURCE, copySource);
	}

	public boolean isCopySource() {
		return getBoolean(COPY_SOURCE);
	}
}
