/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.textunitflat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.AltTranslation;
import net.sf.okapi.common.annotation.AltTranslationsAnnotation;
import net.sf.okapi.common.annotation.Note;
import net.sf.okapi.common.annotation.NoteAnnotation;
import net.sf.okapi.common.query.MatchType;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.WhitespaceStrategy;

public class TextUnitFlat2Proto {

	static net.sf.okapi.proto.textunitflat.TextFragment.TagType toTagType(TagType tt) {
		switch (tt) {
		case CLOSING:
			return net.sf.okapi.proto.textunitflat.TextFragment.TagType.CLOSING;
		case OPENING:
			return net.sf.okapi.proto.textunitflat.TextFragment.TagType.OPENING;
		case PLACEHOLDER:
			return net.sf.okapi.proto.textunitflat.TextFragment.TagType.PLACEHOLDER;
		default:
			return net.sf.okapi.proto.textunitflat.TextFragment.TagType.UNRECOGNIZED;
		}
	}

	static net.sf.okapi.proto.textunitflat.Code toCode(Code code, int position, boolean isolated) {
		net.sf.okapi.proto.textunitflat.Code.Builder builder = net.sf.okapi.proto.textunitflat.Code.newBuilder()
				.setTagType(toTagType(code.getTagType()))
				.setId(code.getId()).setCodeType(code.getType())
				.setData(code.getData())
				.setOuterData(code.getOuterData())
				.setFlag(code.getFlag())
				.setPosition(position)
				.setIsolated(isolated)
				.setAdded(code.isAdded());

		if (null != code.getDisplayText()) {
			builder.setDisplayText(code.getDisplayText());
		}

		if (null != code.getOriginalId()) {
			builder.setOriginalId(code.getOriginalId());
		}
		
		if (null != code.getMergedData() && !code.getMergedData().isEmpty()) {
			builder.setMergedData(code.getMergedData());
		}

		for (String propName : code.getPropertyNames()) {
			builder.addProperties(toProperty(code.getProperty(propName)));
		}

		return builder.build();
	}

	static net.sf.okapi.proto.textunitflat.TextFragment toTextFragment(TextFragment tf) {
		net.sf.okapi.proto.textunitflat.TextFragment.Builder builder = net.sf.okapi.proto.textunitflat.TextFragment.newBuilder()
				.setText(tf.getText());

		if (tf.hasCode()) {
			tf.balanceMarkers();	
			List<Code> codes = tf.getCodes(); 
			int codeCount = 0;
			for ( int i=0; i<tf.length(); i++ ) {
				switch (tf.charAt(i)) {
				case TextFragment.MARKER_OPENING:
				case TextFragment.MARKER_CLOSING:
				case TextFragment.MARKER_ISOLATED:
					Code code = codes.get(TextFragment.toIndex(tf.charAt(++i)));
					int newPos = i - (codeCount*2); 
					builder.addCodes(toCode(code, newPos-1, tf.charAt(i - 1) == TextFragment.MARKER_ISOLATED));
					codeCount++;
					break;
				default:
					break;
				}
			}
		}
		return builder.build();
	}
	
	static net.sf.okapi.proto.textunitflat.Property toProperty(Property prop) {
		net.sf.okapi.proto.textunitflat.Property.Builder builder = net.sf.okapi.proto.textunitflat.Property.newBuilder()
				.setName(prop.getName())
				.setValue(prop.getValue() == null ? "" : prop.getValue())
				.setReadOnly(prop.isReadOnly());

		return builder.build();
	}


	static net.sf.okapi.proto.textunitflat.TextContainer toTextContainer(TextContainer tc, LocaleId locale) {
		net.sf.okapi.proto.textunitflat.TextContainer.Builder builder = net.sf.okapi.proto.textunitflat.TextContainer.newBuilder().
				setSegApplied(tc.hasBeenSegmented()).
				setLocale(locale == null ? "und" : locale.toBCP47());

		if (tc.getId() != null) {
			builder.setId(tc.getId());
		}

		for (TextPart part : tc.getParts()) {
			builder.addParts(toTextPart(part));
		}
		
		for (String propName : tc.getPropertyNames()) {
			builder.addProperties(toProperty(tc.getProperty(propName)));
		}

		NoteAnnotation na = tc.getAnnotation(NoteAnnotation.class);
		if (na != null) {
			for (Note note : na) {
				builder.addNotes(toNote(note));
			}
		}

		AltTranslationsAnnotation ats = tc.getAnnotation(AltTranslationsAnnotation.class);
		if (ats != null) {
			for (AltTranslation altTranslation : ats) {
				builder.addAltTrans(toAltTrans(altTranslation));
			}
		}
		return builder.build();
	}
	
	private static net.sf.okapi.proto.textunitflat.TextPart toTextPart(TextPart part) {
		net.sf.okapi.proto.textunitflat.TextPart.Builder builder =
				net.sf.okapi.proto.textunitflat.TextPart.newBuilder()
						.setSegment(part.isSegment())
						.setWhitespaceStrategy(toWhitespaceStrategy(part.getWhitespaceStrategy()))
						.setText(toTextFragment(part.getContent()));

		if (part.getOriginalId() != null) {
			builder.setOriginalId(part.getOriginalId());
		}

		if (part.getId() != null) {
			builder.setId(part.getId());
		}

		for (String propName : part.getPropertyNames()) {
			builder.addProperties(toProperty(part.getProperty(propName)));
		}

		return builder.build();
	}

	private static net.sf.okapi.proto.textunitflat.TextPart.WhitespaceStrategy toWhitespaceStrategy(WhitespaceStrategy whitespaceStrategy) {
		switch (whitespaceStrategy) {
			case INHERIT:
				return net.sf.okapi.proto.textunitflat.TextPart.WhitespaceStrategy.INHERIT;
			case NORMALIZE:
				return net.sf.okapi.proto.textunitflat.TextPart.WhitespaceStrategy.NORMALIZE;
			case PRESERVE:
				return net.sf.okapi.proto.textunitflat.TextPart.WhitespaceStrategy.PRESERVE;
			default:
				return net.sf.okapi.proto.textunitflat.TextPart.WhitespaceStrategy.INHERIT;


		}
	}


	public static net.sf.okapi.proto.textunitflat.Note toNote(Note note) {
		net.sf.okapi.proto.textunitflat.Note.Builder builder =
				net.sf.okapi.proto.textunitflat.Note.newBuilder().
						setNote(note.getNoteText()).
						setAnnotates(toAnnotates(note.getAnnotates() == null ? Note.Annotates.GENERAL :
							note.getAnnotates())).
						setPriority(toPriority(note.getPriority() == null ? Note.Priority.ONE : note.getPriority())).
						setFrom(note.getFrom() == null ? "" : note.getFrom()).
						setXmlLang(note.getXmLang() == null ? "" : note.getXmLang());

		return builder.build();
	}

	public static net.sf.okapi.proto.textunitflat.AltTranslation.MatchType toMatchType(MatchType matchType) {
		switch (matchType) {
			case ACCEPTED: return net.sf.okapi.proto.textunitflat.AltTranslation.MatchType.ACCEPTED;
			case EXACT_UNIQUE_ID: return net.sf.okapi.proto.textunitflat.AltTranslation.MatchType.EXACT_UNIQUE_ID;
			case EXACT_LOCAL_CONTEXT: return net.sf.okapi.proto.textunitflat.AltTranslation.MatchType.EXACT_LOCAL_CONTEXT;
			case EXACT: return net.sf.okapi.proto.textunitflat.AltTranslation.MatchType.EXACT;
			case EXACT_TEXT_ONLY: return net.sf.okapi.proto.textunitflat.AltTranslation.MatchType.EXACT_TEXT_ONLY;
			case FUZZY: return net.sf.okapi.proto.textunitflat.AltTranslation.MatchType.FUZZY;
			default: return net.sf.okapi.proto.textunitflat.AltTranslation.MatchType.UKNOWN;
		}
	}

	// assume only one textfragment per textcontainer
	public static net.sf.okapi.proto.textunitflat.AltTranslation toAltTrans(AltTranslation altTrans) {
		net.sf.okapi.proto.textunitflat.AltTranslation.Builder builder =
				net.sf.okapi.proto.textunitflat.AltTranslation.newBuilder().
						setTextUnit(toTextUnit(altTrans.getTextUnit(), altTrans.getSourceLocale())).
						setSourceLocale(altTrans.getSourceLocale().toBCP47()).
						setTargetLocale(altTrans.getTargetLocale().toBCP47()).
						setType(toMatchType(altTrans.getType())).
						setCombinedScore(altTrans.getCombinedScore()).
						setOrigin(altTrans.getOrigin());

		if (altTrans.getEngine() != null) {
			builder.setEngine(altTrans.getEngine());
		}
		builder.setFromOriginal(altTrans.getFromOriginal());
		builder.setFuzzyScore(altTrans.getFuzzyScore());
		builder.setQualityScore(altTrans.getQualityScore());
		builder.setAltTransType(altTrans.getALttransType());

		return builder.build();
	}

	public static net.sf.okapi.proto.textunitflat.Note.Annotates toAnnotates(Note.Annotates annotates) {
		switch (annotates) {
			case SOURCE: return net.sf.okapi.proto.textunitflat.Note.Annotates.SOURCE;
			case TARGET: return net.sf.okapi.proto.textunitflat.Note.Annotates.TARGET;
			default: return net.sf.okapi.proto.textunitflat.Note.Annotates.GENERAL;
		}
	}

	public static net.sf.okapi.proto.textunitflat.Note.Priority toPriority(Note.Priority priority) {
		switch (priority) {
			case ONE: return net.sf.okapi.proto.textunitflat.Note.Priority.ONE;
			case TWO: return net.sf.okapi.proto.textunitflat.Note.Priority.TWO;
			case THREE: return net.sf.okapi.proto.textunitflat.Note.Priority.THREE;
			case FOUR: return net.sf.okapi.proto.textunitflat.Note.Priority.FOUR;
			case FIVE: return net.sf.okapi.proto.textunitflat.Note.Priority.FIVE;
			case SIX: return net.sf.okapi.proto.textunitflat.Note.Priority.SIX;
			case SEVEN: return net.sf.okapi.proto.textunitflat.Note.Priority.SEVEN;
			case EIGHT: return net.sf.okapi.proto.textunitflat.Note.Priority.EIGHT;
			case NINE: return net.sf.okapi.proto.textunitflat.Note.Priority.NINE;
			default: return net.sf.okapi.proto.textunitflat.Note.Priority.TEN;
		}
	}

	public static net.sf.okapi.proto.textunitflat.TextUnit toTextUnit(ITextUnit tu, LocaleId sourceLocale) {
		net.sf.okapi.proto.textunitflat.TextUnit.Builder builder = net.sf.okapi.proto.textunitflat.TextUnit.newBuilder()				
				.setSource(toTextContainer(tu.getSource(), sourceLocale)).setId(tu.getId())
				.setPreserveWS(tu.preserveWhitespaces()).setTranslatable(tu.isTranslatable());

		if (null != tu.getMimeType()) {
			builder.setMimeType(tu.getMimeType());
		}
		
		if (null != tu.getName()) {
			builder.setName(tu.getName());
		}
		
		if (null != tu.getType()) {
			builder.setTuType(tu.getType());
		}
		
		Map<String, net.sf.okapi.proto.textunitflat.TextContainer> targets = new HashMap<>();
		for (LocaleId locale : tu.getTargetLocales()) {
			targets.put(locale.toBCP47(), toTextContainer(tu.getTarget(locale), locale));
		}
		builder.putAllTargets(targets);
		
		for (String propName : tu.getPropertyNames()) {
			builder.addProperties(toProperty(tu.getProperty(propName)));
		}

		NoteAnnotation na = tu.getAnnotation(NoteAnnotation.class);
		if (na != null) {
			for (Note note : na) {
				builder.addNotes(toNote(note));
			}
		}

		return builder.build();
	}
	
	public static net.sf.okapi.proto.textunitflat.TextUnits toTextUnits(List<ITextUnit> textunits,
																		LocaleId sourceLocale) {
		net.sf.okapi.proto.textunitflat.TextUnits.Builder builder = net.sf.okapi.proto.textunitflat.TextUnits.newBuilder();
		for (ITextUnit tu : textunits) {
			builder.addTextUnits(toTextUnit(tu, sourceLocale));
		}		
		return builder.build();
	}
}
