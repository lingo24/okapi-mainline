/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.step;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.DefaultFilters;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import net.sf.okapi.steps.common.RawDocumentWriterStep;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class OriginalDocumentTextUnitFlatJsonMergerStepTest {
	private HtmlFilter htmlFilter;
	private XLIFFFilter xliffFilter;
	private FileLocation fileLocation;
	private OriginalDocumentTextUnitFlatMergerStep merger;
	private RawDocumentWriterStep writer;

	@Before
	public void setUp() {
		htmlFilter = new HtmlFilter();
		xliffFilter = new XLIFFFilter();		
		merger = new OriginalDocumentTextUnitFlatMergerStep();
		writer = new RawDocumentWriterStep();
		fileLocation = FileLocation.fromClass(getClass());
	}

	@After
	public void tearDown() {
		htmlFilter.close();
		merger.destroy();
		writer.destroy();
	}

	@Test
	public void simpleMerge() {
		String file = "/simple.html";
		String input = fileLocation.in(file).toString();
		String root = fileLocation.in("/").toString();
		String output = fileLocation.in(file+".json").toString();
		String merged = fileLocation.in(file+".merged").toString();

		// Serialize the json file
		MergerUtil.writeJson(FilterTestDriver.getEvents(
					htmlFilter, 
					new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH), null),
				root, output);

		IFilterConfigurationMapper fcm = new FilterConfigurationMapper();
        DefaultFilters.setMappings(fcm, true, true);        
        merger.setFilterConfigurationMapper(fcm);
		merger.setOutputEncoding("UTF-8");
		RawDocument rd = new RawDocument(Util.toURI(input),"UTF-8", LocaleId.ENGLISH);
		rd.setFilterConfigId("okf_html");
		merger.setSecondInput(rd);
		List<LocaleId> ts = new LinkedList<>();
		ts.add(LocaleId.FRENCH);
		merger.setTargetLocales(ts);
		merger.handleEvent(Event.createStartBatchItemEvent());
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT, 
						new RawDocument(Util.toURI(output), "UTF-8", LocaleId.ENGLISH, LocaleId.ENGLISH)));
		
		writer.setOutputURI(Util.toURI(merged));
		writer.handleEvent(e);
		writer.destroy();
		
		RawDocument ord = new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH);
		RawDocument trd = new RawDocument(Util.toURI(merged), "UTF-8", LocaleId.ENGLISH);
		List<Event> o = MergerUtil.getTextUnitEvents(htmlFilter, ord);
		List<Event> t = MergerUtil.getTextUnitEvents(htmlFilter, trd);
		assertTrue(o.size() == t.size());
		assertTrue(FilterTestDriver.compareEvents(o, t, true));
	}
	
	@Test
	public void simpleXliff() {
		String file = "/simple.xlf";
		String input = fileLocation.in(file).toString();
		String root = fileLocation.in("/").toString();
		String output = fileLocation.in(file+".json").toString();
		String merged = fileLocation.in(file+".merged").toString();

		// Serialize the json file
		MergerUtil.writeJson(FilterTestDriver.getEvents(
					xliffFilter, 
					new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH), null),
				root, output);

		IFilterConfigurationMapper fcm = new FilterConfigurationMapper();
        DefaultFilters.setMappings(fcm, true, true);        
        merger.setFilterConfigurationMapper(fcm);
		merger.setOutputEncoding("UTF-8");
		RawDocument rd = new RawDocument(Util.toURI(input),"UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH);
		rd.setFilterConfigId("okf_xliff");
		merger.setSecondInput(rd);
		List<LocaleId> ts = new LinkedList<>();
		ts.add(LocaleId.FRENCH);
		merger.setTargetLocales(ts);
		merger.handleEvent(Event.createStartBatchItemEvent());
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT, 
						new RawDocument(Util.toURI(output), "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH)));
		
		writer.setOutputURI(Util.toURI(merged));
		writer.handleEvent(e);
		writer.destroy();
		
		RawDocument ord = new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH);
		RawDocument trd = new RawDocument(Util.toURI(merged), "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH);
		List<Event> o = MergerUtil.getTextUnitEvents(xliffFilter, ord);
		List<Event> t = MergerUtil.getTextUnitEvents(xliffFilter, trd);
		assertTrue(o.size() == t.size());
		assertTrue(FilterTestDriver.compareEvents(o, t, true));
	}
	
	@Test
	public void simpleSegmentedXliff() {
		String file = "/segmented.xlf";
		String input = fileLocation.in(file).toString();
		String root = fileLocation.in("/").toString();
		String output = fileLocation.in(file+".json").toString();
		String merged = fileLocation.in(file+".merged").toString();

		// Serialize the json file
		MergerUtil.writeJson(FilterTestDriver.getEvents(
					xliffFilter, 
					new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN), null),
				root, output);

		IFilterConfigurationMapper fcm = new FilterConfigurationMapper();
        DefaultFilters.setMappings(fcm, true, true);        
        merger.setFilterConfigurationMapper(fcm);
		merger.setOutputEncoding("UTF-8");
		RawDocument rd = new RawDocument(Util.toURI(input),"UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		rd.setFilterConfigId("okf_xliff");
		merger.setSecondInput(rd);
		List<LocaleId> ts = new LinkedList<>();
		ts.add(LocaleId.GERMAN);
		merger.setTargetLocales(ts);
		merger.handleEvent(Event.createStartBatchItemEvent());
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT, 
						new RawDocument(Util.toURI(output), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN)));
		
		writer.setOutputURI(Util.toURI(merged));
		merger.handleEvent(Event.createStartBatchItemEvent());
		writer.handleEvent(e);
		writer.destroy();
		
		RawDocument ord = new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		RawDocument trd = new RawDocument(Util.toURI(merged), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		List<Event> o = MergerUtil.getTextUnitEvents(xliffFilter, ord);
		List<Event> t = MergerUtil.getTextUnitEvents(xliffFilter, trd);
		assertTrue(o.size() == t.size());
		assertTrue(FilterTestDriver.compareEvents(o, t, true));
	}
	
	@Test
	public void segmentedEmptyTarget() {
		String file = "/segmented_empty_targets.xlf";
		String input = fileLocation.in(file).toString();
		String root = fileLocation.in("/").toString();
		String output = fileLocation.in(file+".json").toString();
		String merged = fileLocation.in(file+".merged").toString();

		// Serialize the json file
		MergerUtil.writeJson(FilterTestDriver.getEvents(
					xliffFilter, 
					new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN), null),
				root, output);

		IFilterConfigurationMapper fcm = new FilterConfigurationMapper();
        DefaultFilters.setMappings(fcm, true, true);        
        merger.setFilterConfigurationMapper(fcm);
		merger.setOutputEncoding("UTF-8");
		RawDocument rd = new RawDocument(Util.toURI(input),"UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		rd.setFilterConfigId("okf_xliff");
		merger.setSecondInput(rd);
		List<LocaleId> ts = new LinkedList<>();
		ts.add(LocaleId.GERMAN);
		merger.setTargetLocales(ts);
		merger.handleEvent(Event.createStartBatchItemEvent());
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT, 
						new RawDocument(Util.toURI(output), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN)));
		
		writer.setOutputURI(Util.toURI(merged));
		merger.handleEvent(Event.createStartBatchItemEvent());
		writer.handleEvent(e);
		writer.destroy();
		
		RawDocument ord = new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		RawDocument trd = new RawDocument(Util.toURI(merged), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		List<Event> o = MergerUtil.getTextUnitEvents(xliffFilter, ord);
		List<Event> t = MergerUtil.getTextUnitEvents(xliffFilter, trd);
		assertTrue(o.size() == t.size());
		assertTrue(FilterTestDriver.compareEvents(o, t, true));
	}
	
	@Test
	public void segmentedInterSegment() {
		String file = "/segmented_inter_segment.xlf";
		String input = fileLocation.in(file).toString();
		String root = fileLocation.in("/").toString();
		String output = fileLocation.in(file+".json").toString();
		String merged = fileLocation.in(file+".merged").toString();

		// Serialize the json file
		MergerUtil.writeJson(FilterTestDriver.getEvents(
					xliffFilter, 
					new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN), null),
				root, output);

		IFilterConfigurationMapper fcm = new FilterConfigurationMapper();
        DefaultFilters.setMappings(fcm, true, true);        
        merger.setFilterConfigurationMapper(fcm);
		merger.setOutputEncoding("UTF-8");
		RawDocument rd = new RawDocument(Util.toURI(input),"UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		rd.setFilterConfigId("okf_xliff");
		merger.setSecondInput(rd);
		List<LocaleId> ts = new LinkedList<>();
		ts.add(LocaleId.GERMAN);
		merger.setTargetLocales(ts);
		merger.handleEvent(Event.createStartBatchItemEvent());
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT, 
						new RawDocument(Util.toURI(output), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN)));
		
		writer.setOutputURI(Util.toURI(merged));
		writer.handleEvent(e);
		writer.destroy();
		
		RawDocument ord = new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		RawDocument trd = new RawDocument(Util.toURI(merged), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		List<Event> o = MergerUtil.getTextUnitEvents(xliffFilter, ord);
		List<Event> t = MergerUtil.getTextUnitEvents(xliffFilter, trd);
		assertTrue(o.size() == t.size());
		assertTrue(FilterTestDriver.compareEvents(o, t, true));
	}
	
	@Test
	public void segmentedExtraTarget() {
		String file = "/segmented_diff_segments.xlf";
		String input = fileLocation.in(file).toString();
		String root = fileLocation.in("/").toString();
		String output = fileLocation.in(file+".json").toString();
		String merged = fileLocation.in(file+".merged").toString();

		// Serialize the json file
		MergerUtil.writeJson(FilterTestDriver.getEvents(
					xliffFilter, 
					new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN), null),
				root, output);

		IFilterConfigurationMapper fcm = new FilterConfigurationMapper();
        DefaultFilters.setMappings(fcm, true, true);        
        merger.setFilterConfigurationMapper(fcm);
		merger.setOutputEncoding("UTF-8");
		RawDocument rd = new RawDocument(Util.toURI(input),"UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		rd.setFilterConfigId("okf_xliff");
		merger.setSecondInput(rd);
		List<LocaleId> ts = new LinkedList<>();
		ts.add(LocaleId.GERMAN);
		merger.setTargetLocales(ts);
		merger.handleEvent(Event.createStartBatchItemEvent());
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT, 
						new RawDocument(Util.toURI(output), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN)));
		
		writer.setOutputURI(Util.toURI(merged));
		writer.handleEvent(e);
		writer.destroy();
		
		RawDocument ord = new RawDocument(Util.toURI(input), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		RawDocument trd = new RawDocument(Util.toURI(merged), "UTF-8", LocaleId.ENGLISH, LocaleId.GERMAN);
		List<Event> o = MergerUtil.getTextUnitEvents(xliffFilter, ord);
		List<Event> t = MergerUtil.getTextUnitEvents(xliffFilter, trd);
		assertTrue(o.size() == t.size());
		assertTrue(FilterTestDriver.compareEvents(o, t, true));
	}
}
