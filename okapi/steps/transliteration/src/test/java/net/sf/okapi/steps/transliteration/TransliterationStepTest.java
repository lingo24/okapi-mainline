/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.transliteration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextUnit;


@RunWith(JUnit4.class)
public class TransliterationStepTest {

	@Test
	public void test() {
		LocaleId locSource = LocaleId.fromString("sr"); // default script is Cyrillic
		LocaleId locTarget = LocaleId.fromString("sr-Latn");

		String source = "Serbian belongs to the Slavic group of languages in the Indo-European language family.";
		String original = "Српски језик припада словенској групи језика породице индоевропских језика.";
		String expected = "Srpski jezik pripada slovenskoj grupi jezika porodice indoevropskih jezika.";

		TransliterationStep step = new TransliterationStep();
		step.setTargetLocale(locTarget);

		Parameters params = step.getParameters();
		params.setFromTargetLocale(locSource);
		params.setToTargetLocale(locTarget);
		params.setIcuTransliteratorId("Serbian-Latin/BGN");

		TextUnit res = new TextUnit("id1", source);
		res.createTarget(locSource, true, 0);
		TextContainer tcTarget = res.createTarget(locSource, true, TextUnit.COPY_ALL);
		tcTarget.setParts(new TextPart(original));
		step.handleEvent(new Event(EventType.TEXT_UNIT, res));

		assertNotNull(res);
		assertTrue(!res.hasTarget(locSource)); // remove the old target
		assertTrue(res.hasTarget(locTarget)); // new target created
		assertEquals(expected, res.getTarget(locTarget).toString());
	}

	@Test
	public void testComplex() {
		LocaleId locSource = LocaleId.fromString("zh");
		LocaleId locTarget = LocaleId.fromString("en");

		String source = "\"Laozi\", also known as \"Tao Te Ching\", is an ancient book in the pre-Qin period,"
				+ " and it is said to be written by Laozi, a thinker in the late Spring and Autumn Period.";
		String original = "《老子》，又名《道德經》，是先秦時期的古籍，相傳為春秋末期思想家老子所著。";
		String expected = "lao zi, you ming dao de jing, shi xian qin shi qi de gu ji, xiang chuan wei chun qiu mo qi si xiang jia lao zi suo zhe";

		TransliterationStep step = new TransliterationStep();
		step.setTargetLocale(locTarget);

		Parameters params = step.getParameters();
		params.setFromTargetLocale(locSource);
		params.setToTargetLocale(locTarget);
		// Using compound IDs:
		// - convert Chinese to Latin
		// - decompose (to separate the accents from the base letters
		// - remove all non-ASCII using a regex filter
		// See https://unicode-org.github.io/icu/userguide/transforms/general/#compound-ids
		params.setIcuTransliteratorId("Any-Latin;NFD;[^\\u0000-\\u007f] Remove");

		TextUnit res = new TextUnit("id1", source);
		res.createTarget(locSource, true, 0);
		TextContainer tcTarget = res.createTarget(locSource, true, TextUnit.COPY_ALL);
		tcTarget.setParts(new TextPart(original));
		step.handleEvent(new Event(EventType.TEXT_UNIT, res));

		assertNotNull(res);
		assertTrue(!res.hasTarget(locSource)); // remove the old target
		assertTrue(res.hasTarget(locTarget)); // new target created
		assertEquals(expected, res.getTarget(locTarget).toString());
	}

}
