/*===========================================================================
  Copyright (C) 2008-2012 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.filters;

import net.sf.okapi.common.*;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.encoder.IEncoder;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.*;

import java.util.HashMap;
import java.util.Map;

public class SubFilterEventConverter {

    private final IdGenerator idGenerator;
    private final SubFilter subFilter;
    private final IEncoder parentEncoder;
    private final Map<String, IResource> referents;

    public SubFilterEventConverter(SubFilter subFilter, IEncoder parentEncoder) {
        this.subFilter = subFilter;
        this.parentEncoder = parentEncoder;
        this.idGenerator = new IdGenerator(null);
        referents = new HashMap<>();
    }

    public void reset() {
        idGenerator.reset(null);
        referents.clear();
    }

    // public to be able to call from subclasses of SubFilter
    public String convertRefIds(String str) {
        final StringBuilder in = new StringBuilder(str);
        final StringBuilder out = new StringBuilder();
        Object[] marker;
        while ((marker = TextFragment.getRefMarker(in)) != null) {
            final String id = (String) marker[0];
            final int start = (Integer) marker[1];
            final int end = (Integer) marker[2];
            out.append(in.substring(0, start));
            final IResource res = this.referents.get(id);
            if (res == null) {
                out.append(in.substring(start, end));
            } else {
                out.append(TextFragment.makeRefMarker(
                                this.subFilter.buildResourceId(id, res.getClass()))
                );
            }
            in.delete(0, end);
        }
        out.append(in);
        return out.toString();
    }

    private void convertTextContainer(TextContainer tc) {
        for (TextPart textPart : tc) {
            TextFragment tf = textPart.getContent();
            for (Code code : tf.getCodes()) {
                if (!code.hasReference()) continue;

                String data = code.getOuterData();
                String newData = convertRefIds(data);
                if (code.hasOuterData()) {
                    code.setOuterData(newData);
                } else {
                    code.setData(newData);
                }
            }
        }
    }

    private void convertRefs(Event event) {
        if (event.isMultiEvent()) {
            MultiEvent me = event.getMultiEvent();
            for (Event e : me) {
                convertRefs(e);
            }
        } else {
            if (event.isTextUnit()) {
                ITextUnit tu = event.getTextUnit();
                tu.getSource().setMimeType(tu.getMimeType());
                convertTextContainer(tu.getSource());
                for (LocaleId locId : tu.getTargetLocales()) {
                    tu.getTarget(locId).setMimeType(tu.getMimeType());
                    convertTextContainer(tu.getTarget(locId));
                }
            }

            ISkeleton skel = event.getResource().getSkeleton();
            subFilter.convertRefsInSkeleton(skel);
        }
    }

    /**
     * Converts an event.
     *
     * @param event the event coming from the sub-filter.
     * @return the event after possible conversion.
     */
    public Event convertEvent(Event event) {
        IResource res = event.getResource();
        if (res instanceof IReferenceable) {
            IReferenceable r = (IReferenceable) res;
            // referents are keyed by original Id
            if (r.isReferent()) referents.put(res.getId(), res);
        }

        // we convert START_DOCUMENT to START_SUBFILTER
        // and END_DOCUMENT to END_SUBFILTER
        switch (event.getEventType()) {
            case START_DOCUMENT:
                StartDocument sd = event.getStartDocument();
                IFilterWriter filterWriter = sd.getFilterWriter();
                // Initialize encoder manager and skeleton writer
                if (filterWriter != null) {
                    EncoderManager em = filterWriter.getEncoderManager();
                    if (em != null) {
                        //TODO: the encoding of sd is the input encoding, that may not work in all cases
                        em.setDefaultOptions(sd.getFilterParameters(), sd.getEncoding(), sd.getLineBreak());
                        em.updateEncoder(sd.getMimeType());
                    }
                }

                StartSubfilter startSubfilter =
                        new StartSubfilter(
                                subFilter.buildResourceId(null, StartSubfilter.class),
                                sd,
                                parentEncoder);
                subFilter.startSubfilter = startSubfilter;
                startSubfilter.setName(subFilter.buildResourceName(null, false, StartSubfilter.class));
                startSubfilter.setType(subFilter.getParentType());
                startSubfilter.setMimeType(subFilter.getMimeType());
                event = new Event(EventType.START_SUBFILTER, startSubfilter);
                break;

            case END_DOCUMENT:
                EndSubfilter endSubfilter = new EndSubfilter(subFilter.buildResourceId(null, EndSubfilter.class));
                subFilter.endSubfilter = endSubfilter;
                //TODO: we need to get the skeleton from this event into the string output
                endSubfilter.setSkeleton(event.getEnding().getSkeleton());
                event = new Event(EventType.END_SUBFILTER, endSubfilter);
                break;

            default: // TU, DP, etc.
                // Convert resource Id
                res.setId(subFilter.buildResourceId(res.getId(), res.getClass()));

                // Convert resource name
                if (event.getResource() instanceof INameable) {
                    INameable nres = (INameable)event.getResource();
                    String name = nres.getName();

                    if (Util.isEmpty(nres.getMimeType())) {
                        nres.setMimeType(subFilter.getMimeType());
                    }

                    if (Util.isEmpty(nres.getType())) {
                        nres.setType(subFilter.getParentType());
                    }

                    boolean isEmpty = Util.isEmpty(name);
                    if (isEmpty) name = idGenerator.createId();
                    nres.setName(subFilter.buildResourceName(name, isEmpty, nres.getClass()));
                }

                // Convert references in resources
                convertRefs(event);
                break;
        }

        return event;
    }

}
