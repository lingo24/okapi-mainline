/*===========================================================================
  Copyright (C) 2008-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.pipeline;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Common set of methods for a step within a {@link IPipeline} pipeline.
 */
public interface IPipelineStep extends AutoCloseable, Function<Stream<Event>, Stream<Event>> {

	/**
	 * Given a collection of {@link IPipelineStep} execute them in sequence given a Stream<Event> input.
	 * functions take the output of the previous step as the input of the current step etc. Similar to running a pipeline.
	 * @param functions Collection of Function<Stream<Event>, Stream<Event>>
	 * @param t Stream<Event>
	 * @return Stream<Event>
	 */
	static <T> T runPipelineSteps(Collection<Function<T, T>> functions, T t) {
		return functions.stream()
				.reduce(Function.identity(), Function::andThen)
				.apply(t);
	}

	/**
	 * Gets the current parameters for this step.
	 *
	 * @return the current parameters for this step or null if there are no
	 * parameters.
	 */
	IParameters getParameters();

	/**
	 * Sets new parameters for this step.
	 *
	 * @param params the new parameters to use.
	 */
	void setParameters(IParameters params);

	/**
	 * Gets the localizable name of this step.
	 *
	 * @return the localizable name of this step.
	 */
	String getName();

	/**
	 * Gets a short localizable description of what this step does.
	 *
	 * @return the text of a short description of what this step does.
	 */
	String getDescription();

	/**
	 * Gets the relative directory location for the help of this step. The main help file
	 * for the step must be at that location and its name must be the name of the class
	 * implementing the step in lowercase with a .html extension.
	 *
	 * @return the relative directory location for the help of this step.
	 */
	String getHelpLocation();

	/**
	 * Processes each event sent though the pipeline.
	 *
	 * @param event the event to process.
	 * @return the event to pass down the pipeline.
	 */
	Event handleEvent(Event event);

	/**
	 * Processes a stream of events sent though the pipeline.
	 *
	 * @param event the event to process.
	 * @return the stream to pass down the pipeline.
	 */
	default Stream<Event> handleStream(Event event) {
		handleEvent(event);
		return Stream.of(event);
	}

	@Override
	default Stream<Event> apply(Stream<Event> events) {
		return events.flatMap(e -> handleStream(e));
	}

	/**
	 * Steps that can generate {@link Event}s such as {@link IFilter}s return
	 * false until no more events can be created.
	 * Steps which do not create {@link Event}s always return true.
	 *
	 * @return false if can generate more events, true otherwise.
	 */
	boolean isDone();

	/**
	 * Executes any cleanup code for this step. Called once at the end of the
	 * pipeline lifecycle.
	 *
	 * @deprecated use {@link IPipelineStep#close()}  method
	 */
	@Deprecated
	default void destroy() {}

	@Override
	default void close() {
		destroy();
	}

	/**
	 * Cancel processing on this pipeline. Each {@link IPipelineStep} is responsible
	 * to implement a cancel method that will interrupt long running operations
	 */
	void cancel();

	/**
	 * Is this step the last step with output in the pipeline?
	 *
	 * @return true if last step with output, false otherwise.
	 */
	boolean isLastOutputStep();

	/**
	 * Tell the step if it is the last one on the pipeline with output.
	 *
	 * @param isLastStep true if last step with output, false otherwise.
	 */
	void setLastOutputStep(boolean isLastStep);

	/**
	 * Delegate to concrete class
	 *
	 * @return LocaleId
	 */
	default LocaleId getSourceLocale() {
		return null;
	}

	/**
	 * Delegate to concrete class
	 *
	 * @param sourceLocale
	 */
	@StepParameterMapping(parameterType = StepParameterType.SOURCE_LOCALE)
	default void setSourceLocale(LocaleId sourceLocale) {
	}

	/**
	 * Delegate to concrete class
	 *
	 * @return LocaleId
	 */
	default LocaleId getTargetLocale() {
		return null;
	}

	@StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALE)
	default void setTargetLocale(LocaleId targetLocale) {
	}
}
