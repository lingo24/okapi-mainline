package net.sf.okapi.common.filters;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.IParametersEditor;
import net.sf.okapi.common.exceptions.OkapiFilterCreationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * <b>Will replace {@link FilterConfigurationMapper} in the future as the default implementation</b>
 * Implementation of IFilterConfigurationMapper, without some of the eccentricities of
 * {@link FilterConfigurationMapper}.
 * In particular,this one is thread safe by allowing thread local custom configs to be registered.
 * Should be used in most server-based implementations.
 */
public class ThreadSafeFilterConfigurationMapper implements IFilterConfigurationMapper {
    private static final Logger logger = LoggerFactory.getLogger(ThreadSafeFilterConfigurationMapper.class);

    /**
     * Each time a thread starts up, initialize it with a static set of default configs.
     * These are persistent and are reused in subsequent calls to the same thread.
     */
    private final ThreadLocal<Map<String, FilterConfiguration>> configMap;

    /**
     * Create an empty {@link IFilterConfigurationMapper}
     */
    public ThreadSafeFilterConfigurationMapper() {
        this.configMap = ThreadLocal.withInitial(ThreadSafeFilterConfigurationMapper::loadConfigs);
    }

    /**
     * Create {@link IFilterConfigurationMapper} using the {@link Supplier}
     * Example:
     * <code><pre>
         private Map<String, FilterConfiguration> myConfigLoad() {
           // this is my implementation loading the filters it needs
         }

         // client code
         IFilterConfigurationMapper fcMapper =
           new ThreadSafeFilterConfigurationMapper(() -> MyClass::myConfigLoad);
     * </code></pre>
     * @param fcSupplier user implemented {@link FilterConfiguration} map.
     */
    public ThreadSafeFilterConfigurationMapper(Supplier<Map<String, FilterConfiguration>> fcSupplier) {
        this.configMap = ThreadLocal.withInitial(fcSupplier);
    }

    /**
     * Load one or more filter classes and prepare their FilterConfiguration objects. This code lives
     * here because of overlap with filter instantiation code (it needs to instantiate short-lived
     * instances in order to query them for their pre-packaged configurations).
     *
     * <p>The result of building is a map of configId (for example, "okf_html@foo" or "okf_openxml")
     * to FilterConfiguration objects.
     */
    public static class ConfigBuilder {
        private final Map<String, FilterConfiguration> configs = new HashMap<>();

        public ConfigBuilder addConfigurations(Class<? extends IFilter> filterClass) {
            try {
                IFilter filter = filterClass.getDeclaredConstructor().newInstance();
                for (FilterConfiguration config : filter.getConfigurations()) {
                    configs.put(config.configId, config);
                }
                return this;
            } catch (Exception e) {
                throw new IllegalArgumentException("Couldn't add configurations for " + filterClass.getName(), e);
            }
        }

        public ConfigBuilder addConfiguration(String configId, String paramString) {
            String[] parts = FilterConfigurationMapper.splitFilterFromConfiguration(configId);
            // Look up the base filter
            FilterConfiguration baseFilter = configs.get(parts[0]);
            if (baseFilter == null) {
                throw new IllegalArgumentException("Couldn't find base filter for " + configId);
            }
            try (IFilter instance = instantiateFilter(baseFilter.filterClass, null)) {
                IParameters params = instance.getParameters();
                params.fromString(paramString);
                FilterConfiguration config = new FilterConfiguration(configId, "", baseFilter.filterClass, configId, "", null, params, "");
                configs.put(configId, config);
                return this;
            }
        }

        public Map<String, FilterConfiguration> build() {
            return Collections.synchronizedMap(configs);
        }
    }

    private static Map<String, FilterConfiguration> loadConfigs() {
        // For now, we depend on outside callers to populate the configMap
        return Collections.synchronizedMap(new HashMap<>());
    }

    private static IFilter instantiateFilter(String filterClass, IFilter existingInstance) {
        try {
            if (existingInstance != null && Objects.equals(filterClass, existingInstance.getClass().getName())) {
                // Diagnostics - log when we're passed an existing filter instance so
                // we can get a better sense of when this happens.
                logger.info("Reusing existing filter instance: {}", existingInstance.getClass());
                return existingInstance;
            }
            return (IFilter) Class.forName(filterClass).getDeclaredConstructor().newInstance();
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException | NoSuchMethodException |
                 InvocationTargetException e) {
            throw new IllegalArgumentException("Could not instantiate " + filterClass, e);
        }
    }

    @Override
    public void addCustomConfiguration(String configId, IParameters parameters) {
        FilterConfiguration fc;
        fc = new FilterConfiguration();
        fc.custom = true;
        fc.configId = configId;

        // Get the filter
        String[] res = FilterConfigurationMapper.splitFilterFromConfiguration(fc.configId);

        // Create the filter (this assumes the base-name is the default config ID)
        try (IFilter filter = createFilter(res[0])) {
            if (filter == null) {
                logger.error("Cannot find filter with ID: {}. Cannot add configuration", res[0]);
                return;
            }

            // Set the data
            fc.filterClass = filter.getClass().getName();
            fc.mimeType = filter.getMimeType();
            fc.description = "Configuration " + fc.configId; // Temporary
            fc.name = fc.configId;
            fc.parameters = parameters;
            configMap.get().put(fc.configId, fc);
        }
    }

    @Override
    public void addCustomConfiguration(String configId, String parameters) {
        // FIXME: refactor out duplicate code from addCustomConfiguration(String configId, IParameters parameters)
        FilterConfiguration fc;
        fc = new FilterConfiguration();
        fc.custom = true;
        fc.configId = configId;

        // Get the filter
        String[] res = FilterConfigurationMapper.splitFilterFromConfiguration(fc.configId);

        // Create the filter (this assumes the base-name is the default config ID)
        try (IFilter filter = createFilter(res[0])) {
            if (filter == null) {
                logger.error("Cannot find filter with ID: {}. Cannot add configuration", res[0]);
                return;
            }

            // Set the data
            fc.filterClass = filter.getClass().getName();
            fc.mimeType = filter.getMimeType();
            fc.description = "Configuration " + fc.configId; // Temporary
            fc.name = fc.configId;
            IParameters p = filter.getParameters();
            p.fromString(parameters);
            fc.parameters = p;
            configMap.get().put(fc.configId, fc);
        }
    }

    @Override
    public void addConfigurations(String filterClass) {
        IFilter filter;

        // Add the filter to the list
        try {
            filter = (IFilter) Class.forName(filterClass).getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | NoSuchMethodException |
                 InvocationTargetException e) {
            return;
        }

        // Get the available configurations for this filter
        List<FilterConfiguration> list = filter.getConfigurations();
        if ((list == null) || (list.size() == 0)) {
            logger.warn("No configuration provided for '{}'", filterClass);
            return;
        }
        // Add the configurations to the mapper
        filter.setFilterConfigurationMapper(this);
        for (FilterConfiguration config : list) {
            if (config.filterClass == null) {
                logger.warn("Configuration without filter class name in '{}'", config);
                config.filterClass = filterClass;
            }
            if (config.name == null) {
                logger.warn("Configuration without name in '{}'", config);
                config.name = config.toString();
            }
            if (config.description == null) {
                logger.warn("Configuration without description in '{}'", config);
                config.description = config.toString();
            }
            configMap.get().put(config.configId, config);
        }
    }

    @Override
    public void addConfiguration(FilterConfiguration config) {
        configMap.get().put(config.configId, config);
    }

    @Override
    public IFilter createFilter(String configId, IFilter existingFilter) {
        // Get the configuration object for the given configId
        FilterConfiguration fc = getConfiguration(configId);
        if (fc == null) {
            logger.error("Cannot find filter configuration '{}'", configId);
            return null;
        }

        // Instantiate the filter (or re-use one)
        IFilter filter = instantiateFilter(fc.filterClass, existingFilter);

        IParameters params = filter.getParameters();
        if (params == null) {
            if (fc.custom) {
                throw new OkapiFilterCreationException(String.format("Cannot create default parameters for '%s'.",
                        fc.configId));
            } else {
                // No parameters, nothing more to do
                return filter;
            }
        }

        // check non-default parameters first
        if (fc.parametersLocation != null) {
            // Note that we cannot assume the parameters are the same
            // if we re-used an existing filter, as we cannot compare the
            // configuration identifiers
            params.load(filter.getClass().getResourceAsStream(fc.parametersLocation), false);
        } else if (fc.parameters != null) {
            // default parameters
            params.fromString(fc.parameters.toString());
        } else {
            params.reset();
        }
        filter.setFilterConfigurationMapper(this);

        return filter;
    }

    @Override
    public void clearConfigurations(boolean customOnly) {
        if (customOnly) {
            Map.Entry<String, FilterConfiguration> entry;
            Iterator<Map.Entry<String, FilterConfiguration>> iter = configMap.get().entrySet().iterator();
            while (iter.hasNext()) {
                entry = iter.next();
                if (entry.getValue().custom) {
                    iter.remove();
                }
            }
        } else {
            configMap.get().clear();
        }
    }

    @Override
    public IFilter createFilter(String configId) {
        return createFilter(configId, null);
    }

    @Override
    public FilterConfiguration getConfiguration(String configId) {
        // Load baseline configs if they haven't been previously loaded in this thread
        Map<String, FilterConfiguration> cm = configMap.get();
        if (cm == null) {
            throw new IllegalStateException("No filter configurations have been loaded loaded");
        }
        return cm.get(configId);
    }

    @Override
    public FilterConfiguration getDefaultConfiguration(String mimeType) {
        // xliff1.2 and xliff2 use the same mimetype use the autoxliff
        // filter to disambiguate.
        FilterConfiguration autoXliff = getConfiguration("okf_autoxliff");
        if (autoXliff != null && autoXliff.mimeType.equals(mimeType)) {
            return autoXliff;
        }

        for (FilterConfiguration config : configMap.get().values()) {
            // make sure this is default config
            if (config.mimeType != null && config.parametersLocation == null) {
                if (config.mimeType.equals(mimeType)) {
                    return config;
                }
            }
        }
        return null;
    }

    @Override
    public FilterConfiguration getDefaultConfigurationFromExtension(String ext) {
        String tmp = ext.toLowerCase() + ";";
        if (tmp.charAt(0) != '.') {
            tmp = ".".concat(tmp);
        }

        // xliff1.2 and xliff2 use the same file extensions use the autoxliff
        // filter to disambiguate.
        FilterConfiguration autoXliff = getConfiguration("okf_autoxliff");
        if (autoXliff != null && autoXliff.extensions.contains(tmp)) {
            return autoXliff;
        }

        for (FilterConfiguration config : configMap.get().values()) {
            if (config.extensions != null) {
                // Just in case some plugin forgot the trailing semicolon
                String configExtensions = config.extensions + ";";
                if (configExtensions.contains(tmp)) {
                    return config;
                }
            }
        }
        return null;
    }

    @Override
    public IParameters getParameters(FilterConfiguration config) {
        return getParameters(config, null);
    }

    @Override
    public IParameters getParameters(FilterConfiguration config, IFilter existingFilter) {
        IFilter filter = createFilter(config.configId, existingFilter);
        return filter.getParameters();
    }

    @Override
    public FilterConfiguration createCustomConfiguration(FilterConfiguration baseConfig) {
        // This one we do a different way
        throw new UnsupportedOperationException();
    }

    @Override
    public IParameters getCustomParameters(FilterConfiguration config, IFilter existingFilter) {
        throw new UnsupportedOperationException();
    }

    @Override
    public IParameters getCustomParameters(FilterConfiguration config) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<FilterConfiguration> getAllConfigurations() {
        return configMap.get().values().iterator();
    }

    @Override
    public List<FilterConfiguration> getFilterConfigurations(String filterClass) {
        ArrayList<FilterConfiguration> list = new ArrayList<>();
        for (FilterConfiguration config : configMap.get().values()) {
            if (config.filterClass != null) {
                if (config.filterClass.equals(filterClass)) {
                    list.add(config);
                }
            }
        }
        return list;
    }

    @Override
    public IParametersEditor createConfigurationEditor(String configId, IFilter existingFilter) {
        throw new UnsupportedOperationException();
    }

    @Override
    public IParametersEditor createConfigurationEditor(String configId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<FilterInfo> getFiltersInfo() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<FilterConfiguration> getMimeConfigurations(String mimeType) {
        ArrayList<FilterConfiguration> list = new ArrayList<>();
        for (FilterConfiguration config : configMap.get().values()) {
            if (config.mimeType != null) {
                if (config.mimeType.equals(mimeType)) {
                    list.add(config);
                }
            }
        }
        return list;
    }

    @Override
    public void removeConfiguration(String configId) {
        configMap.get().remove(configId);
    }

    @Override
    public void removeConfigurations(String filterClass) {
        Map.Entry<String, FilterConfiguration> entry;
        Iterator<Map.Entry<String, FilterConfiguration>> iter = configMap.get().entrySet().iterator();
        while (iter.hasNext()) {
            entry = iter.next();
            if (entry.getValue().filterClass.equals(filterClass)) {
                iter.remove();
            }
        }
    }

    @Override
    public void saveCustomParameters(FilterConfiguration config, IParameters params) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteCustomParameters(FilterConfiguration config) {
        throw new UnsupportedOperationException();
    }
}
