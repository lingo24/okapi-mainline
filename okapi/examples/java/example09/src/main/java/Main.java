/*===========================================================================
  Copyright (C) 2018 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.HtmlEncoder;
import net.sf.okapi.common.filters.*;
import net.sf.okapi.common.filterwriter.XLIFFWriter;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.TextUnitUtil;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.properties.Parameters;
import net.sf.okapi.filters.properties.PropertiesFilter;
import net.sf.okapi.filters.xliff.XLIFFFilter;

public class Main {

	private static final LocaleId TARGET_LOCALE = LocaleId.FRENCH;
	private static final LocaleId SOURCE_LOCALE = LocaleId.ENGLISH;
	private static final String ENCODING = "UTF-8";
	private static final String INPUT_FILE = "testhtml.properties";
	private static final String OUT_ROOT = new File(Main.class.getResource(INPUT_FILE).getPath()).getParent();

	static void printEvent(Event e) {
		String text;
		text = e.isTextUnit()
			? TextUnitUtil.toText(e.getTextUnit().getSource().getFirstContent())
			: e.getResource().toString();
		if (e.isTextUnit()) {
			System.out.printf("MimeType: %s\n", e.getTextUnit().getMimeType());
			System.out.printf("Name: %s\n", e.getTextUnit().getName());
			System.out.printf("Type: %s\n", e.getTextUnit().getType());
		}
		System.out.printf("%s // %s = \"%s\"%n", e.toString(), e.getResource().getId(), text);
	}

	static void olderSubfilter() {
		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
		DefaultFilters.setMappings(fcMapper, false, true);

		try (InputStream fis = Main.class.getResourceAsStream(INPUT_FILE);
				RawDocument doc = new RawDocument(fis, ENCODING, SOURCE_LOCALE, TARGET_LOCALE);
				IFilter filter = fcMapper.createFilter("okf_properties-html-subfilter")) {

			filter.open(doc, true);
			filter.stream()
					.forEach(Main::printEvent);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void newSubfilterWrapper() {
		try (InputStream fis = Main.class.getResourceAsStream(INPUT_FILE);
				RawDocument doc = new RawDocument(fis, ENCODING, SOURCE_LOCALE, TARGET_LOCALE);
				PropertiesFilter filter = new PropertiesFilter()) {

			// Making sure that the html sub-filter does not interfere with the code finder
			Parameters param = filter.getParameters();
			param.setUseCodeFinder(false);

			filter.open(doc, true);

			SubFilterWrapperStep sub = new SubFilterWrapperStep(new HtmlFilter(), new HtmlEncoder());

			filter.stream()
					.peek((e) -> System.out.println("\033[37m")) // normal color
					.peek(Main::printEvent)
					.flatMap(sub::handleStream)
					.peek((e) -> System.out.print("\033[96m")) // cyan
					.forEach(Main::printEvent);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void compareFiles(Path gold, Path out) {
		try {
			byte[] outBytes = Files.readAllBytes(out);
			byte[] goldBytes = Files.readAllBytes(gold);
			if (!Arrays.equals(outBytes, goldBytes)) {
				System.out.printf("\033[91mDifferent bytes\033[m%n");
				System.out.printf("\033[91m===== Expected =====\033[m%n");
				System.out.printf(new String(goldBytes, StandardCharsets.UTF_8));
				System.out.printf("\033[91m===== But got =====\033[m%n");
				System.out.printf(new String(outBytes, StandardCharsets.UTF_8));
				System.out.printf("\033[91m===================\033[m%n");
			} else {
				System.out.printf("\033[92mSuccess, output and gold files are identical\033[m%n");
			}
		} catch (IOException e) {
			System.out.print("\033[91m");
			e.printStackTrace();
			System.out.print("\033[m");
		}
	}

	static void xliffWithHtmlSubfilter() {
		File outFile = new File(OUT_ROOT, "xliffWithHtml.out.xliff");

		try (InputStream fis = Main.class.getResourceAsStream("xliffWithHtml.xliff");
				RawDocument doc = new RawDocument(fis, ENCODING, SOURCE_LOCALE, TARGET_LOCALE);
				XLIFFFilter filter = new XLIFFFilter();
				XLIFFWriter writer = new XLIFFWriter()) {

			filter.open(doc, true);
			writer.setOptions(TARGET_LOCALE, ENCODING);
			writer.setOutput(outFile.toString());

			SubFilterWrapperStep sub = new SubFilterWrapperStep(new HtmlFilter(), new HtmlEncoder());

			filter.stream()
					.flatMap(sub::handleStream)
					.forEach(writer::handleEvent);

			// Compare output and gold
			URL gold = Main.class.getResource("xliffWithHtml.gold.xliff");
			compareFiles(Paths.get(gold.toURI()), outFile.toPath());
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}
	}

	public static void main (String[] args) {
		System.out.println("===== olderSubfilter ===================");
		olderSubfilter();
		System.out.println("===== newSubfilterWrapper ===================");
		newSubfilterWrapper();
		System.out.println("===== xliffWithHtmlSubfilter ===================");
		xliffWithHtmlSubfilter();
	}
}
