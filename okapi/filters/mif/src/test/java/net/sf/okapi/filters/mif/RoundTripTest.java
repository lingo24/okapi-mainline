/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.mif;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.filters.RoundTripComparison;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.RawDocument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(DataProviderRunner.class)
public class RoundTripTest {
    private static final LocaleId DEFAULT_LOCALE = LocaleId.ENGLISH;
    private static final String DEFAULT_CHARSET = StandardCharsets.UTF_8.name();
    private final FileLocation fileLocation;

    public RoundTripTest() {
        this.fileLocation = FileLocation.fromClass(this.getClass());
    }

    @DataProvider
    public static Object[][] hardReturnsAsNonTextualProvider() {
        return new Object[][]{
            {"987.mif"},
            {"990-marker.mif"},
            {"990-pgf-num-format-1.mif"},
            {"990-pgf-num-format-2.mif"},
            {"990-ref-format-1.mif"},
            {"990-ref-format-2.mif"},
            {"990-text-line.mif"},
        };
    }

    @Test
    @UseDataProvider("hardReturnsAsNonTextualProvider")
    public void hardReturnsAsNonTextualRoundTripped(final String documentName) {
        final List<InputDocument> list = new ArrayList<>();
        list.add(new InputDocument(this.fileLocation.in("/" + documentName).toString(), "okf_mif@non-textual-hard-returns.fprm"));

        final RoundTripComparison rtc = new RoundTripComparison(true); // Do compare skeletons
        assertTrue(rtc.executeCompare(new MIFFilter(), list, DEFAULT_CHARSET, DEFAULT_LOCALE, DEFAULT_LOCALE, "output"));
    }

    @DataProvider
    public static Object[][] roundTripsWithDifferentParametersProvider() {
        return new Object[][] {
            {"893.mif"},
            {"895.mif"},
            {"896.mif"},
            {"896-changed.mif"},
            {"896-autonumber-building-blocks.mif"},
            {"902-1.mif"},
            {"902-2.mif"},
            {"902-3.mif"},
            {"904.mif"},
            {"909-1.mif"},
            {"909-2.mif"},
            {"909-3.mif"},
            {"938-2.mif"},
            {"942-1.mif"},
            {"942-2.mif"},
            {"945.mif"},
            {"987.mif"},

            {"ImportedText.mif"},
            {"JATest.mif"},
            {"Test01.mif"},
            {"Test01-v8.mif"},
            {"Test02-v9.mif"},
            {"Test03.mif"},
            {"Test04.mif"},
            {"TestEncoding-v9.mif"},
            {"TestEncoding-v10.mif"},
            {"TestFootnote.mif"},
            {"TestMarkers.mif"},
            {"TestParaLines.mif"},
        };
    }

    @Test
    @UseDataProvider("roundTripsWithDifferentParametersProvider")
    public void roundTripsWithDifferentParameters(final String documentName) {
        final List<InputDocument> list = new ArrayList<>();
        list.add(new InputDocument(this.fileLocation.in("/" + documentName).toString(), null));
        list.add(new InputDocument(this.fileLocation.in("/" + documentName).toString(), "okf_mif@common.fprm"));
        list.add(new InputDocument(this.fileLocation.in("/" + documentName).toString(), "okf_mif@inline-pgf-num-formats.fprm"));

        final RoundTripComparison rtc = new RoundTripComparison(false); // Do not compare skeleton
        assertTrue(rtc.executeCompare(new MIFFilter(), list, DEFAULT_CHARSET, DEFAULT_LOCALE, DEFAULT_LOCALE, "output"));
    }

    @Test
    public void consequentialEmptyParaLinesMerged() {
        roundTripAndCheck(new Parameters(), "1187_crlf.mif", "1187_crlf.mif");
    }

    @Test
    public void tabsEncodedOnExtractionAndHardReturnsEncodedOnMerge() {
        final Parameters p = new Parameters();
        p.setExtractHardReturnsAsText(true);
        roundTripAndCheck(p, "1188_crlf.mif", "1188_crlf.mif");
        p.setExtractHardReturnsAsText(false);
        roundTripAndCheck(p, "1188_crlf.mif", "1188_crlf.mif");
    }

    private void roundTripAndCheck(final Parameters parameters, final String documentName, final String expectedDocumentName) {
        final FileLocation.In input = this.fileLocation.in("/" + documentName);
        final FileLocation.Out actualOutput = this.fileLocation.out("/actual/" + expectedDocumentName);
        final FileLocation.In expectedOutput = this.fileLocation.in("/expected/" + expectedDocumentName);
        try (final RawDocument rawDocument = new RawDocument(input.asUri(), DEFAULT_CHARSET, DEFAULT_LOCALE);
             final MIFFilter filter = new MIFFilter()) {
            filter.setParameters(parameters);
            filter.open(rawDocument, true);
            try (final IFilterWriter filterWriter = filter.createFilterWriter()) {
                filterWriter.setOutput(actualOutput.toString());
                filterWriter.setOptions(DEFAULT_LOCALE, DEFAULT_CHARSET);
                filter.forEachRemaining(filterWriter::handleEvent);
            }
        }
        final FileCompare fc = new FileCompare();
        assertTrue(fc.compareFilesPerLines(actualOutput.toString(), expectedOutput.toString(), DEFAULT_CHARSET));
    }
}
