/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Iterator;
import java.util.LinkedList;

interface CellFormat {
    String NAME = "xf";
    boolean idAvailable();
    int id();
    int numberFormatId();
    int fontId();
    int fillId();
    int borderId();
    CellAlignment alignment();
    CellProtection protection();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    /**
     * A default cell format implementation.
     * As MS Excel application sets default numberFormatId, fontId, fillId, borderId to "0", the
     * same behaviour is reflected.
     */
    final class Default implements CellFormat {
        private static final String XF_ID = "xfId";
        private static final String NUM_FMT_ID = "numFmtId";
        private static final String FONT_ID = "fontId";
        private static final String FILL_ID = "fillId";
        private static final String BORDER_ID = "borderId";
        private final XMLEventFactory eventFactory;
        private final StartElement startElement;
        private boolean idAvailable;
        private int id;
        private int numberFormatId;
        private int fontId;
        private int fillId;
        private int borderId;
        private CellAlignment alignment;
        private CellProtection protection;
        private EndElement endElement;
        private boolean startElementAttributesRead;

        Default(final XMLEventFactory eventFactory, final StartElement startElement) {
            this.eventFactory = eventFactory;
            this.startElement = startElement;
        }

        @Override
        public boolean idAvailable() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return this.idAvailable;
        }

        @Override
        public int id() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return this.id;
        }

        @Override
        public int numberFormatId() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return this.numberFormatId;
        }

        @Override
        public int fontId() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return this.fontId;
        }

        @Override
        public int fillId() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return this.fillId;
        }

        @Override
        public int borderId() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return this.borderId;
        }

        @Override
        public CellAlignment alignment() {
            if (null == this.alignment) {
                this.alignment = new CellAlignment.Empty();
            }
            return this.alignment;
        }

        @Override
        public CellProtection protection() {
            if (null == this.protection) {
                this.protection = new CellProtection.Empty();
            }
            return this.protection;
        }

        /**
         * Reads essential start element attributes.
         * As any of applyNumberFormat, applyFont, applyFill, applyBorder, applyAlignment,
         * applyProtection is not used by MS Excel application when an ending style for
         * rendering is determined, they are not considered as well.
         */
        private void readStartElementAttributes() {
            final Iterator iterator = this.startElement.getAttributes();
            while (iterator.hasNext()) {
                final Attribute a = (Attribute) iterator.next();
                switch (a.getName().getLocalPart()) {
                    case XF_ID:
                        this.idAvailable = true;
                        this.id = Integer.parseUnsignedInt(a.getValue());
                        break;
                    case NUM_FMT_ID:
                        this.numberFormatId = Integer.parseUnsignedInt(a.getValue());
                        break;
                    case FONT_ID:
                        this.fontId = Integer.parseUnsignedInt(a.getValue());
                        break;
                    case FILL_ID:
                        this.fillId = Integer.parseUnsignedInt(a.getValue());
                        break;
                    case BORDER_ID:
                        this.borderId = Integer.parseUnsignedInt(a.getValue());
                        break;
                }
            }
            this.startElementAttributesRead = true;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                if (CellAlignment.NAME.equals(se.getName().getLocalPart())) {
                    this.alignment = new CellAlignment.Default(this.eventFactory, se);
                    this.alignment.readWith(reader);
                } else if (CellProtection.NAME.equals(se.getName().getLocalPart())) {
                    this.protection = new CellProtection.Default(se);
                    this.protection.readWith(reader);
                }
            }
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new LinkedList<>()));
            mb.add(this.startElement);
            mb.add(alignment().asMarkup());
            mb.add(protection().asMarkup());
            mb.add(this.endElement);
            return mb.build();
        }
    }
}
