/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndDocument;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

interface ContentTypes {
    String PART_NAME = "[Content_Types].xml";

    Iterator<ContentType> iterator();
    ContentTypes with(final String partPath);
    void readWith(final XMLEventReader eventReader) throws XMLStreamException;
    void addFrom(final String partPath, final String type);
    Markup asMarkup();

    class Default implements ContentTypes {
        private static final String NAME = "Types";

        private final XMLEventFactory eventFactory;
        private final List<ContentType> contentTypes;
        private StartDocument startDocument;
        private StartElement startElement;
        private EndElement endElement;
        private EndDocument endDocument;

        Default(final XMLEventFactory eventFactory) {
            this(eventFactory, new LinkedList<>());
        }

        Default(final XMLEventFactory eventFactory, final List<ContentType> contentTypes) {
            this.eventFactory = eventFactory;
            this.contentTypes = contentTypes;
        }

        @Override
        public Iterator<ContentType> iterator() {
            return this.contentTypes.iterator();
        }

        @Override
        public ContentTypes with(final String partPath) {
            final String wellformedPath = wellformedPath(partPath);
            List<ContentType> types = this.contentTypes.stream()
                .filter(ct -> ct.id().equals(wellformedPath))
                .collect(Collectors.toList());
            if (types.isEmpty()) {
                types = this.contentTypes.stream()
                    .filter(ct -> ct.id().equals(extensionOf(wellformedPath)))
                    .collect(Collectors.toList());
            }
            if (types.isEmpty()) {
                final List<Attribute> attributes = Arrays.asList(
                    this.eventFactory.createAttribute(
                        ContentType.Default.EXTENSION,
                        ""
                    ),
                    this.eventFactory.createAttribute(
                        ContentType.Default.CONTENT_TYPE,
                        "application/octet-stream"
                    )
                );
                types = new LinkedList<>(
                    Arrays.asList(
                        new ContentType.Default(
                            this.eventFactory,
                            this.eventFactory.createStartElement(
                                this.startElement.getName().getPrefix(),
                                this.startElement.getName().getNamespaceURI(),
                                ContentType.Default.NAME,
                                attributes.iterator(),
                                null
                            )
                        )
                    )
                );
            }
            final ContentTypes.Default contentTypes = new ContentTypes.Default(this.eventFactory, types);
            contentTypes.startDocument = this.startDocument;
            contentTypes.startElement = startElement;
            contentTypes.endElement = this.endElement;
            contentTypes.endDocument = this.endDocument;
            return contentTypes;
        }

        private static String wellformedPath(final String p) {
            return p.startsWith("/") ? p : "/" + p;
        }

        private static String extensionOf(final String partName) {
            final int i = partName.lastIndexOf('.');
            final String extension;
            if (i != -1) {
                extension = partName.substring(i + 1);
            } else {
                extension = partName;
            }
            return extension;
        }

        @Override
        public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
            while (eventReader.hasNext()) {
                final XMLEvent e = eventReader.nextEvent();
                if (e.isEndDocument()) {
                    this.endDocument = (EndDocument) e;
                    break;
                }
                if (e.isStartDocument()) {
                    this.startDocument = (StartDocument) e;
                    continue;
                }
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    continue;
                }
                if (e.isStartElement() && ContentTypes.Default.NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    this.startElement = e.asStartElement();
                    continue;
                }
                if (e.isStartElement() && ContentType.Default.NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    final ContentType ct = new ContentType.Default(
                        this.eventFactory,
                        e.asStartElement()
                    );
                    ct.readWith(eventReader);
                    this.contentTypes.add(ct);
                    continue;
                }
                if (e.isStartElement() && ContentType.Override.NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    final ContentType ct = new ContentType.Override(
                        new ContentType.Default(
                            this.eventFactory,
                            e.asStartElement()
                        )
                    );
                    ct.readWith(eventReader);
                    this.contentTypes.add(ct);
                }
            }
        }

        @Override
        public void addFrom(final String partPath, final String type) {
            final List<Attribute> attributes = Arrays.asList(
                this.eventFactory.createAttribute(
                    ContentType.Override.PART_NAME,
                    partPath
                ),
                this.eventFactory.createAttribute(
                    ContentType.Default.CONTENT_TYPE,
                    type
                )
            );
            this.contentTypes.add(
                new ContentType.Override(
                    new ContentType.Default(
                        this.eventFactory,
                        this.eventFactory.createStartElement(
                            this.startElement.getName().getPrefix(),
                            this.startElement.getName().getNamespaceURI(),
                            ContentType.Override.NAME,
                            attributes.iterator(),
                            null
                        )
                    )
                )
            );
        }

        @Override
        public Markup asMarkup() {
            final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new ArrayList<>()));
            mb.add(this.startDocument);
            mb.add(this.startElement);
            this.contentTypes.forEach(ct -> mb.add(ct.asMarkup()));
            mb.add(this.endElement);
            mb.add(this.endDocument);
            return mb.build();
        }
    }

    class Values {
        private static final String APPLICATION_PREFIX = "application/vnd.";
        private static final String DOCUMENT_PREFIX = APPLICATION_PREFIX + "openxmlformats-officedocument.";

        static class Common {
            static final String CORE_PROPERTIES_TYPE =
                APPLICATION_PREFIX + "openxmlformats-package.core-properties+xml";
            static final String PACKAGE_RELATIONSHIPS =
                APPLICATION_PREFIX + "openxmlformats-package.relationships+xml";
        }

        static class Word {
            static final String MAIN_DOCUMENT_TYPE =
                DOCUMENT_PREFIX + "wordprocessingml.document.main+xml";
            static final String MACRO_ENABLED_MAIN_DOCUMENT_TYPE =
                APPLICATION_PREFIX + "ms-word.document.macroEnabled.main+xml";

            static final String TEMPLATE_DOCUMENT_TYPE =
                DOCUMENT_PREFIX + "wordprocessingml.template.main+xml";
            static final String MACRO_ENABLED_TEMPLATE_DOCUMENT_TYPE =
                APPLICATION_PREFIX + "ms-word.template.macroEnabledTemplate.main+xml";

            static final String SETTINGS_TYPE =
                DOCUMENT_PREFIX + "wordprocessingml.settings+xml";
            static final String STYLES_TYPE =
                DOCUMENT_PREFIX + "wordprocessingml.styles+xml";
            static final String FOOTER_TYPE =
                DOCUMENT_PREFIX + "wordprocessingml.footer+xml";
            static final String ENDNOTES_TYPE =
                DOCUMENT_PREFIX + "wordprocessingml.endnotes+xml";
            static final String HEADER_TYPE =
                DOCUMENT_PREFIX + "wordprocessingml.header+xml";
            static final String FOOTNOTES_TYPE =
                DOCUMENT_PREFIX + "wordprocessingml.footnotes+xml";
            static final String COMMENTS_TYPE =
                DOCUMENT_PREFIX + "wordprocessingml.comments+xml";
            static final String GLOSSARY_DOCUMENT_TYPE =
                DOCUMENT_PREFIX + "wordprocessingml.document.glossary+xml";
        }

        static class Drawing {
            static final String CHART_TYPE =
                DOCUMENT_PREFIX + "drawingml.chart+xml";
            static final String DIAGRAM_DATA_TYPE =
                DOCUMENT_PREFIX + "drawingml.diagramData+xml";
            static final String THEME_TYPE =
                DOCUMENT_PREFIX + "theme+xml";
            static final String THEME_OVERRIDE_TYPE =
                DOCUMENT_PREFIX + "themeOverride+xml";
        }

        static class Powerpoint {
            static final String MAIN_DOCUMENT_TYPE =
                DOCUMENT_PREFIX + "presentationml.presentation.main+xml";
            static final String MACRO_ENABLED_MAIN_DOCUMENT_TYPE =
                APPLICATION_PREFIX + "ms-powerpoint.presentation.macroEnabled.main+xml";

            static final String SLIDE_SHOW_DOCUMENT_TYPE =
                DOCUMENT_PREFIX + "presentationml.slideshow.main+xml";
            static final String MACRO_ENABLED_SLIDE_SHOW_DOCUMENT_TYPE =
                APPLICATION_PREFIX + "ms-powerpoint.slideshow.macroEnabled.main+xml";

            static final String TEMPLATE_DOCUMENT_TYPE =
                DOCUMENT_PREFIX + "presentationml.template.main+xml";
            static final String MACRO_ENABLED_TEMPLATE_DOCUMENT_TYPE =
                APPLICATION_PREFIX + "ms-powerpoint.template.macroEnabled.main+xml";

            static final String SLIDE_TYPE =
                DOCUMENT_PREFIX + "presentationml.slide+xml";
            static final String SLIDE_LAYOUT_TYPE =
                DOCUMENT_PREFIX + "presentationml.slideLayout+xml";
            static final String SLIDE_MASTER_TYPE =
                DOCUMENT_PREFIX + "presentationml.slideMaster+xml";
            static final String COMMENTS_TYPE =
                DOCUMENT_PREFIX + "presentationml.comments+xml";
            static final String NOTES_SLIDE_TYPE =
                DOCUMENT_PREFIX + "presentationml.notesSlide+xml";
            static final String NOTES_MASTER_TYPE =
                DOCUMENT_PREFIX + "presentationml.notesMaster+xml";
        }

        static class Excel {
            static final String MAIN_DOCUMENT_TYPE =
                DOCUMENT_PREFIX + "spreadsheetml.sheet.main+xml";
            static final String MACRO_ENABLED_MAIN_DOCUMENT_TYPE =
                APPLICATION_PREFIX + "ms-excel.sheet.macroEnabled.main+xml";

            static final String TEMPLATE_DOCUMENT_TYPE =
                DOCUMENT_PREFIX + "spreadsheetml.template.main+xml";
            static final String MACRO_ENABLED_TEMPLATE_DOCUMENT_TYPE =
                APPLICATION_PREFIX + "ms-excel.template.macroEnabled.main+xml";

            static final String SHARED_STRINGS_TYPE =
                DOCUMENT_PREFIX + "spreadsheetml.sharedStrings+xml";
            static final String WORKSHEET_TYPE =
                DOCUMENT_PREFIX + "spreadsheetml.worksheet+xml";
            static final String COMMENT_TYPE =
                DOCUMENT_PREFIX + "spreadsheetml.comments+xml";
            static final String TABLE_TYPE =
                DOCUMENT_PREFIX + "spreadsheetml.table+xml";
            static final String PIVOT_TABLE_TYPE =
                DOCUMENT_PREFIX + "spreadsheetml.pivotTable+xml";
            static final String PIVOT_CACHE_DEFINITION_TYPE =
                DOCUMENT_PREFIX + "spreadsheetml.pivotCacheDefinition+xml";
            static final String STYLES_TYPE =
                DOCUMENT_PREFIX + "spreadsheetml.styles+xml";
            static final String DRAWINGS_TYPE =
                DOCUMENT_PREFIX + "drawing+xml";
        }

        static class Visio {
            static final String MAIN_DOCUMENT_TYPE = APPLICATION_PREFIX + "ms-visio.drawing.main+xml";
            static final String MACRO_ENABLED_MAIN_DOCUMENT_TYPE = APPLICATION_PREFIX + "ms-visio.drawing.macroEnabled.main+xml";

            static final String MASTER_TYPE = APPLICATION_PREFIX + "ms-visio.master+xml";
            static final String PAGE_TYPE = APPLICATION_PREFIX + "ms-visio.page+xml";
        }
    }
}
