/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

interface ElementsClarification {
    void performFor(final List<Property> properties);

    final class Bypass implements ElementsClarification {
        @Override
        public void performFor(final List<Property> properties) {
        }
    }

    final class TableBlockPropertyDefault implements ElementsClarification {
        private final ClarificationContext clarificationContext;
        private final String elementName;

        TableBlockPropertyDefault(
            final ClarificationContext clarificationContext,
            final String elementName
        ) {
            this.clarificationContext = clarificationContext;
            this.elementName = elementName;
        }

        @Override
        public void performFor(final List<Property> properties) {
            // todo provide support for table styles hierarchy processing
            final ListIterator<Property> iterator = properties.listIterator();
            while (iterator.hasNext()) {
                final Property blockProperty = iterator.next();
                if (this.elementName.equals(blockProperty.getName().getLocalPart())) {
                    if (!this.clarificationContext.targetRtl()) {
                        iterator.remove();
                        return;
                    }
                    return;
                }
            }
            if (clarificationContext.targetRtl()) {
                iterator.add(
                    new BlockProperty(
                        this.elementName,
                        Collections.emptyMap(),
                        this.clarificationContext.creationalParameters(),
                        this.clarificationContext.conditionalParameters(),
                        this.clarificationContext.presetColorValues(),
                        this.clarificationContext.highlightColorValues(),
                        new StrippableAttributes.DrawingRunProperties(
                            this.clarificationContext.conditionalParameters(),
                            this.clarificationContext.creationalParameters().getEventFactory()
                        )
                    )
                );
            }
        }
    }

    final class ParagraphBlockPropertyDefault implements ElementsClarification {
        private final ClarificationContext clarificationContext;
        private final String elementName;
        private final String defaultValue;
        private final String defaultValueWhenAbsent;
        private final Set<String> falseValues;
        private final Set<String> trueValues;

        ParagraphBlockPropertyDefault(
            final ClarificationContext clarificationContext,
            final String elementName,
            final String defaultValue,
            final String defaultValueWhenAbsent,
            final Set<String> falseValues,
            final Set<String> trueValues
        ) {
            this.clarificationContext = clarificationContext;
            this.elementName = elementName;
            this.defaultValue = defaultValue;
            this.defaultValueWhenAbsent = defaultValueWhenAbsent;
            this.falseValues = falseValues;
            this.trueValues = trueValues;
        }

        @Override
        public void performFor(final List<Property> properties) {
            if (this.clarificationContext.sourceLtr() && this.clarificationContext.targetLtr()
                || this.clarificationContext.sourceRtl() && this.clarificationContext.targetRtl()) {
                // source and target locales are both LTR or RTL - no clarification needed
                return;
            }
            final String combinedValue = this.clarificationContext.combinedParagraphProperties().properties().stream()
                .filter(p -> p.getName().getLocalPart().equals(this.elementName))
                .map(p -> {
                    final String value = p.value();
                    return null == value
                        ? this.defaultValue
                        : value;
                })
                .findFirst()
                .orElse(this.defaultValueWhenAbsent);
            if (this.falseValues.contains(combinedValue)) {
                if (this.clarificationContext.sourceLtr() && this.clarificationContext.targetRtl()) {
                    // add/change to true
                    adjustPropertyValueToTrue(properties);
                }
            } else if (this.trueValues.contains(combinedValue)) {
                if (this.clarificationContext.sourceRtl() && this.clarificationContext.targetLtr()) {
                    // add/change to false
                    adjustPropertyValueToFalse(properties);
                }
            }
        }

        private void adjustPropertyValueToFalse(final List<Property> properties) {
            final Iterator<Property> iterator = properties.iterator();
            while (iterator.hasNext()) {
                final Property property = iterator.next();
                if (this.elementName.equals(property.getName().getLocalPart())) {
                    iterator.remove();
                    return;
                }
            }
            final String falseValue = this.falseValues.stream().findFirst().orElseThrow(
                () -> new IllegalStateException("True values are empty")
            );
            properties.add(
                new BlockProperty(
                    this.elementName,
                    values(falseValue),
                    this.clarificationContext.creationalParameters(),
                    this.clarificationContext.conditionalParameters(),
                    this.clarificationContext.presetColorValues(),
                    this.clarificationContext.highlightColorValues(),
                    new StrippableAttributes.DrawingRunProperties(
                        this.clarificationContext.conditionalParameters(),
                        this.clarificationContext.creationalParameters().getEventFactory()
                    )
                )
            );
        }

        void adjustPropertyValueToTrue(final List<Property> properties) {
            final String trueValue = this.trueValues.stream().findFirst().orElseThrow(
                () -> new IllegalStateException("True values are empty")
            );
            final ListIterator<Property> iterator = properties.listIterator();
            while (iterator.hasNext()) {
                final Property runProperty = iterator.next();
                if (this.elementName.equals(runProperty.getName().getLocalPart())) {
                    iterator.set(
                        new BlockProperty(
                            this.elementName,
                            values(trueValue),
                            this.clarificationContext.creationalParameters(),
                            this.clarificationContext.conditionalParameters(),
                            this.clarificationContext.presetColorValues(),
                            this.clarificationContext.highlightColorValues(),
                            new StrippableAttributes.DrawingRunProperties(
                                this.clarificationContext.conditionalParameters(),
                                this.clarificationContext.creationalParameters().getEventFactory()
                            )
                        )
                    );
                    return;
                }
            }
            properties.add(
                new BlockProperty(
                    this.elementName,
                    values(trueValue),
                    this.clarificationContext.creationalParameters(),
                    this.clarificationContext.conditionalParameters(),
                    this.clarificationContext.presetColorValues(),
                    this.clarificationContext.highlightColorValues(),
                    new StrippableAttributes.DrawingRunProperties(
                        this.clarificationContext.conditionalParameters(),
                        this.clarificationContext.creationalParameters().getEventFactory()
                    )
                )
            );
        }

        private Map<String, String> values(final String trueValue) {
            final Map<String, String> values;
            if (this.defaultValue.contains(trueValue)) {
                values = Collections.emptyMap();
            } else {
                values = Collections.singletonMap("val", trueValue);
            }
            return values;
        }
    }

    final class RunPropertyDefault implements ElementsClarification {
        private final ClarificationContext clarificationContext;
        private final String elementName;
        private final String defaultValue;
        private final String defaultValueWhenAbsent;
        private final Set<String> falseValues;
        private final Set<String> trueValues;

        RunPropertyDefault(
            final ClarificationContext clarificationContext,
            final String elementName,
            final String defaultValue,
            final String defaultValueWhenAbsent,
            final Set<String> falseValues,
            final Set<String> trueValues
        ) {
            this.clarificationContext = clarificationContext;
            this.elementName = elementName;
            this.defaultValue = defaultValue;
            this.defaultValueWhenAbsent = defaultValueWhenAbsent;
            this.falseValues = falseValues;
            this.trueValues = trueValues;
        }

        @Override
        public void performFor(final List<Property> properties) {
            if (this.clarificationContext.sourceLtr() && this.clarificationContext.targetLtr()
                || this.clarificationContext.sourceRtl() && this.clarificationContext.targetRtl()) {
                // source and target locales are both LTR or RTL - no clarification needed
                return;
            }
            final String combinedValue = this.clarificationContext.combinedRunProperties().properties().stream()
                .filter(p -> p.getName().getLocalPart().equals(this.elementName))
                .map(p -> {
                    final String value = p.value();
                    return null == value
                        ? this.defaultValue
                        : value;
                })
                .findFirst()
                .orElse(this.defaultValueWhenAbsent);
            if (this.falseValues.contains(combinedValue)) {
                if (this.clarificationContext.sourceLtr() && this.clarificationContext.targetRtl()) {
                    // add/change to true
                    adjustPropertyValueToTrue(properties);
                }
            } else if (this.trueValues.contains(combinedValue)) {
                if (this.clarificationContext.sourceRtl() && this.clarificationContext.targetLtr()) {
                    // add/change to false
                    adjustPropertyValueToFalse(properties);
                }
            }
        }

        private void adjustPropertyValueToFalse(final List<Property> properties) {
            final Iterator<Property> iterator = properties.iterator();
            while (iterator.hasNext()) {
                final Property runProperty = iterator.next();
                if (this.elementName.equals(runProperty.getName().getLocalPart())) {
                    iterator.remove();
                    return;
                }
            }
            properties.add(
                RunPropertyFactory.createRunProperty(
                    this.clarificationContext.creationalParameters(),
                    this.elementName,
                    values(
                        this.falseValues.stream().findFirst().orElseThrow(
                            () -> new IllegalStateException("False values are empty")
                        )
                    )
                )
            );
        }

        void adjustPropertyValueToTrue(final List<Property> properties) {
            final String trueValue = this.trueValues.stream().findFirst().orElseThrow(
                () -> new IllegalStateException("True values are empty")
            );
            final ListIterator<Property> iterator = properties.listIterator();
            while (iterator.hasNext()) {
                final Property runProperty = iterator.next();
                if (this.elementName.equals(runProperty.getName().getLocalPart())) {
                    iterator.set(
                        RunPropertyFactory.createRunProperty(
                            this.clarificationContext.creationalParameters(),
                            this.elementName,
                            values(trueValue)
                        )
                    );
                    return;
                }
            }
            properties.add(
                RunPropertyFactory.createRunProperty(
                    this.clarificationContext.creationalParameters(),
                    this.elementName,
                    values(trueValue)
                )
            );
        }

        private Map<String, String> values(final String trueValue) {
            final Map<String, String> values;
            if (this.defaultValue.contains(trueValue)) {
                values = Collections.emptyMap();
            } else {
                values = Collections.singletonMap("val", trueValue);
            }
            return values;
        }
    }

    final class RunPropertyLang implements ElementsClarification {
        private final ElementsClarification otherElementsClarification;
        private final ClarificationContext clarificationContext;
        private final String elementName;
        private final String attributeName;

        RunPropertyLang(
            final ElementsClarification otherElementsClarification,
            final ClarificationContext clarificationContext,
            final String elementName,
            final String attributeName
        ) {
            this.otherElementsClarification = otherElementsClarification;
            this.clarificationContext = clarificationContext;
            this.elementName = elementName;
            this.attributeName = attributeName;
        }

        @Override
        public void performFor(final List<Property> properties) {
            this.otherElementsClarification.performFor(properties);
            if (this.clarificationContext.sourceLtr() && this.clarificationContext.targetLtr()
                || this.clarificationContext.sourceRtl() && this.clarificationContext.targetRtl()
                && !this.elementName.isEmpty()) {
                // source and target locales are both LTR
                // or they are both RTL and the element name is not empty - no clarification needed
                return;
            }
            if (this.elementName.isEmpty()) {
                properties.add(
                    RunPropertyFactory.createRunProperty(
                        new QName(
                            this.clarificationContext.creationalParameters().getNamespaceUri(),
                            this.attributeName
                        ),
                        this.clarificationContext.targetLocale().toString()
                    )
                );
            } else if (this.clarificationContext.targetHasCharactersAsNumeralSeparators()) {
                properties.add(
                    RunPropertyFactory.createRunProperty(
                        this.clarificationContext.creationalParameters(),
                        this.elementName,
                        Collections.singletonMap(
                            this.attributeName,
                            this.clarificationContext.targetLocale().toString()
                        )
                    )
                );
            }
        }
    }

    final class RunPropertyBoldItalicsFontSize implements ElementsClarification {
        private final ElementsClarification otherElementsClarification;
        private final ClarificationContext clarificationContext;
        private final ContentCategoriesDetection contentCategoriesDetection;
        private final String boldName;
        private final String complexScriptBoldName;
        private final String italicsName;
        private final String complexScriptItalicsName;
        private final String fontSizeName;
        private final String complexScriptFontSizeName;

        RunPropertyBoldItalicsFontSize(
            final ElementsClarification otherElementsClarification,
            final ContentCategoriesDetection contentCategoriesDetection,
            final ClarificationContext clarificationContext,
            final String boldName,
            final String complexScriptBoldName,
            final String italicsName,
            final String complexScriptItalicsName,
            final String fontSizeName,
            final String complexScriptFontSizeName
        ) {
            this.otherElementsClarification = otherElementsClarification;
            this.contentCategoriesDetection = contentCategoriesDetection;
            this.clarificationContext = clarificationContext;
            this.boldName = boldName;
            this.complexScriptBoldName = complexScriptBoldName;
            this.italicsName = italicsName;
            this.complexScriptItalicsName = complexScriptItalicsName;
            this.fontSizeName = fontSizeName;
            this.complexScriptFontSizeName = complexScriptFontSizeName;
        }

        @Override
        public void performFor(final List<Property> properties) {
            this.otherElementsClarification.performFor(properties);
            if (Namespace.PREFIX_W.equals(this.clarificationContext.creationalParameters().getPrefix())
                && !this.clarificationContext.runText().isEmpty()) {
                final RunProperties combinedRunProperties = this.clarificationContext.combinedRunProperties();
                final AvailableRunProperties combined = availableRunPropertiesFrom(combinedRunProperties.properties());
                final RunFonts sourceRunFonts = this.clarificationContext.detectedRunFonts();
                final RunFonts targetRunFonts = newRunFonts();
                this.contentCategoriesDetection.performFor(targetRunFonts, this.clarificationContext.runText());
                if (sourceRunFonts.containsDetectedComplexScriptContentCategories()
                    && targetRunFonts.containsDetectedNonComplexScriptContentCategories()) {
                    addNonComplexScriptProperties(properties, combined);
                }
                if (sourceRunFonts.containsDetectedNonComplexScriptContentCategories()
                    && targetRunFonts.containsDetectedComplexScriptContentCategories()) {
                    addComplexScriptProperties(properties, combined);
                }
            }
        }

        private AvailableRunProperties availableRunPropertiesFrom(final List<Property> properties) {
            final AvailableRunProperties ap = new AvailableRunProperties();
            properties.forEach(p -> {
                if (this.boldName.equals(p.getName().getLocalPart())) {
                    ap.boldProperty = (RunProperty) p;
                } else if (this.complexScriptBoldName.equals(p.getName().getLocalPart())) {
                    ap.complexScriptBoldProperty = (RunProperty) p;
                } else if (this.italicsName.equals(p.getName().getLocalPart())) {
                    ap.italicsProperty = (RunProperty) p;
                } else if (this.complexScriptItalicsName.equals(p.getName().getLocalPart())) {
                    ap.complexScriptItalicsProperty = (RunProperty) p;
                } else if (this.fontSizeName.equals(p.getName().getLocalPart())) {
                    ap.fontSizeProperty = (RunProperty) p;
                } else if (this.complexScriptFontSizeName.equals(p.getName().getLocalPart())) {
                    ap.complexScriptFontSizeProperty = (RunProperty) p;
                }
            });
            return ap;
        }

        private RunFonts newRunFonts() {
            return new RunFonts(
                this.clarificationContext.creationalParameters().getEventFactory(),
                startElementWith(RunFonts.NAME, Collections.emptyIterator()),
                new EnumMap<>(RunFonts.ContentCategory.class),
                EnumSet.noneOf(RunFonts.ContentCategory.class)
            );
        }

        private void addNonComplexScriptProperties(final List<Property> properties, final AvailableRunProperties combined) {
            if (combined.complexScriptBoldPresent() && !combined.complexScriptBoldProperty.equals(combined.boldProperty)) {
                replaceOrAdd(
                    properties,
                    wpmlToggleRunPropertyWith(
                        this.boldName,
                        combined.complexScriptBoldProperty.getEvents().get(0).asStartElement().getAttributes()
                    )
                );
            }
            if (combined.complexScriptItalicsPresent() && !combined.complexScriptItalicsProperty.equals(combined.italicsProperty)) {
                replaceOrAdd(
                    properties,
                    wpmlToggleRunPropertyWith(
                        this.italicsName,
                        combined.complexScriptItalicsProperty.getEvents().get(0).asStartElement().getAttributes()
                    )
                );
            }
            if (combined.complexScriptFontSizePresent() && !combined.complexScriptFontSizeProperty.equals(combined.fontSizeProperty)) {
                replaceOrAdd(
                    properties,
                    genericRunPropertyWith(
                        this.fontSizeName,
                        combined.complexScriptFontSizeProperty.getEvents().get(0).asStartElement().getAttributes()
                    )
                );
            }
        }

        private void addComplexScriptProperties(final List<Property> properties, final AvailableRunProperties combined) {
            if (combined.boldPresent() && !combined.boldProperty.equals(combined.complexScriptBoldProperty)) {
                replaceOrAdd(
                    properties,
                    wpmlToggleRunPropertyWith(
                        this.complexScriptBoldName,
                        combined.boldProperty.getEvents().get(0).asStartElement().getAttributes()
                    )
                );
            }
            if (combined.italicsPresent() && !combined.italicsProperty.equals(combined.complexScriptItalicsProperty)) {
                replaceOrAdd(
                    properties,
                    wpmlToggleRunPropertyWith(
                        this.complexScriptItalicsName,
                        combined.italicsProperty.getEvents().get(0).asStartElement().getAttributes()
                    )
                );
            }
            if (combined.fontSizePresent() && !combined.fontSizeProperty.equals(combined.complexScriptFontSizeProperty)) {
                replaceOrAdd(
                    properties,
                    genericRunPropertyWith(
                        this.complexScriptFontSizeName,
                        combined.fontSizeProperty.getEvents().get(0).asStartElement().getAttributes()
                    )
                );
            }
        }

        private void replaceOrAdd(final List<Property> properties, final RunProperty runProperty) {
            boolean replaced = false;
            final ListIterator<Property> iterator = properties.listIterator();
            while (iterator.hasNext()) {
                final RunProperty currentProperty = (RunProperty) iterator.next();
                if (runProperty.equalsProperty(currentProperty)) {
                    iterator.set(runProperty);
                    replaced = true;
                    break;
                }
            }
            if (!replaced) {
                properties.add(runProperty);
            }
        }

        private RunProperty wpmlToggleRunPropertyWith(final String name, final Iterator<Attribute> attributes) {
            return new RunProperty.WpmlToggleRunProperty(
                Arrays.asList(
                    startElementWith(name, attributes),
                    endElementWith(name)
                )
            );
        }

        private RunProperty genericRunPropertyWith(final String name, final Iterator<Attribute> attributes) {
            return new RunProperty.GenericRunProperty(
                Arrays.asList(
                    startElementWith(name, attributes),
                    endElementWith(name)
                )
            );
        }

        private StartElement startElementWith(final String name, final Iterator<Attribute> attributes) {
            return this.clarificationContext.creationalParameters().getEventFactory().createStartElement(
                this.clarificationContext.creationalParameters().getPrefix(),
                this.clarificationContext.creationalParameters().getNamespaceUri(),
                name,
                attributes,
                null
            );
        }

        private EndElement endElementWith(final String name) {
            return this.clarificationContext.creationalParameters().getEventFactory().createEndElement(
                this.clarificationContext.creationalParameters().getPrefix(),
                this.clarificationContext.creationalParameters().getNamespaceUri(),
                name
            );
        }

        private static final class AvailableRunProperties {
            private RunProperty boldProperty;
            private RunProperty complexScriptBoldProperty;
            private RunProperty italicsProperty;
            private RunProperty complexScriptItalicsProperty;
            private RunProperty fontSizeProperty;
            private RunProperty complexScriptFontSizeProperty;

            boolean boldPresent() {
                return null != this.boldProperty;
            }

            boolean complexScriptBoldPresent() {
                return null != this.complexScriptBoldProperty;
            }

            boolean italicsPresent() {
                return null != this.italicsProperty;
            }

            boolean complexScriptItalicsPresent() {
                return null != this.complexScriptItalicsProperty;
            }

            boolean fontSizePresent() {
                return null != this.fontSizeProperty;
            }

            boolean complexScriptFontSizePresent() {
                return null != this.complexScriptFontSizeProperty;
            }
        }
    }
}
