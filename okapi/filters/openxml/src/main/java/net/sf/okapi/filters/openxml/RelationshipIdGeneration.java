/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.regex.Pattern;

interface RelationshipIdGeneration {
    void refineLastIndexWith(final String currentId);
    String nextId();

    final class Default implements RelationshipIdGeneration {
        private static final String R_ID = "rId";
        private final Pattern nonDigits;
        private final String nonDigitReplacement;
        private int lastIndex;

        Default() {
            this.nonDigits = Pattern.compile("\\D");
            this.nonDigitReplacement = "";
        }

        @Override
        public void refineLastIndexWith(final String currentId) {
            final String digits = this.nonDigits.matcher(currentId).replaceAll(this.nonDigitReplacement);
            final int number;
            if (!digits.isEmpty()) {
                number = Integer.parseUnsignedInt(digits);
            } else {
                number = 0;
            }
            if (number > this.lastIndex) {
                this.lastIndex = number;
            }
        }

        @Override
        public String nextId() {
            return R_ID.concat(String.valueOf(++this.lastIndex));
        }
    }
}
