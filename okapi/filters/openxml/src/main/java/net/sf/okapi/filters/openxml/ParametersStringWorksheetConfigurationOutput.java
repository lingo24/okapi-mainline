/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.ParametersString;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

final class ParametersStringWorksheetConfigurationOutput implements WorksheetConfiguration.Output<ParametersString> {
    @Override
    public ParametersString writtenWith(
        final Pattern namePattern,
        final Set<Integer> excludedRows,
        final Set<String> excludedColumns,
        final Set<Integer> metadataRows,
        final Set<String> metadataColumns
    ) {
        final ParametersString ps = new ParametersString();
        ps.setString(ParametersStringWorksheetConfiguration.NAME_PATTERN, namePattern.toString());
        if (!excludedRows.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.EXCLUDED_ROWS,
                excludedRows.stream()
                    .map(r -> Integer.toUnsignedString(r))
                    .collect(Collectors.joining(ParametersStringWorksheetConfiguration.DELIMITER))
            );
        }
        if (!excludedColumns.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.EXCLUDED_COLUMNS,
                String.join(ParametersStringWorksheetConfiguration.DELIMITER, excludedColumns)
            );
        }
        if (!metadataRows.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.METADATA_ROWS,
                metadataRows.stream()
                    .map(r -> Integer.toUnsignedString(r))
                    .collect(Collectors.joining(ParametersStringWorksheetConfiguration.DELIMITER))
            );
        }
        if (!metadataColumns.isEmpty()) {
            ps.setString(
                ParametersStringWorksheetConfiguration.METADATA_COLUMNS,
                String.join(ParametersStringWorksheetConfiguration.DELIMITER, metadataColumns)
            );
        }
        return ps;
    }
}