/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.resource.DocumentPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;

import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createEndMarkupComponent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isPresentationEndEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isPresentationStartEvent;

/**
 * Provides a modifiable part.
 */
class ModifiablePart extends NonTranslatablePart {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final Document.General generalDocument;
    private final ZipEntry entry;
    private final InputStream inputStream;
    private final PresetColorValues highlightColorValues;
    private StrippableAttributes drawingDirectionStrippableAttributes;
    private XMLEventReader eventReader;

    ModifiablePart(
        final Document.General generalDocument,
        final ZipEntry entry,
        final InputStream inputStream,
        final PresetColorValues highlightColorValues
    ) {
        this.generalDocument = generalDocument;
        this.entry = entry;
        this.inputStream = inputStream;
        this.highlightColorValues = highlightColorValues;
    }

    @Override
    public Event open() throws IOException, XMLStreamException {
        this.drawingDirectionStrippableAttributes = new StrippableAttributes.DrawingDirection(
            this.generalDocument.eventFactory()
        );
        this.eventReader = this.generalDocument.inputFactory().createXMLEventReader(
            this.inputStream,
            this.generalDocument.encoding()
        );
        try {
            return new Event(EventType.DOCUMENT_PART, read());
        } finally {
            if (null != this.eventReader) {
                this.eventReader.close();
            }
        }
    }

    private DocumentPart read() throws XMLStreamException {
        final MarkupBuilder markupBuilder = new MarkupBuilder(new Block.Markup(new Markup.General(new ArrayList<>())));

        while (this.eventReader.hasNext()) {
            final XMLEvent event = this.eventReader.nextEvent();

            if (isPresentationStartEvent(event)) {
                markupBuilder.add(
                    new MarkupComponent.Start(
                        this.generalDocument.eventFactory(),
                        this.drawingDirectionStrippableAttributes.strip(event.asStartElement())
                    )
                );
            } else if (isPresentationEndEvent(event)) {
                markupBuilder.add(createEndMarkupComponent(event.asEndElement()));
            } else if (event.isStartElement()
                && PowerpointStyleDefinitions.NAMES.contains(event.asStartElement().getName().getLocalPart())) {
                final PowerpointStyleDefinitionsReader reader = new PowerpointStyleDefinitionsReader(
                    this.generalDocument.conditionalParameters(),
                    this.generalDocument.eventFactory(),
                    this.generalDocument.presetColorValues(),
                    this.highlightColorValues,
                    this.eventReader,
                    event.asStartElement(),
                    event.asStartElement().getName().getLocalPart()
                );
                final PowerpointStyleDefinitions powerpointStyleDefinitions =
                    new PowerpointStyleDefinitions(this.generalDocument.eventFactory());
                powerpointStyleDefinitions.readWith(reader);
                markupBuilder.add(powerpointStyleDefinitions.asMarkup());
            } else if (event.isStartElement()
                && (RunProperties.RPR.equals(event.asStartElement().getName().getLocalPart())
                    || RunProperties.END_PARA_RPR.equals(event.asStartElement().getName().getLocalPart()))) {
                markupBuilder.add(runProperties(event.asStartElement()));
            } else {
                markupBuilder.add(event);
            }
        }

        DocumentPart documentPart = new DocumentPart(this.entry.getName(), false);
        documentPart.setSkeleton(
            new MarkupZipSkeleton(
                this.generalDocument.zipFile(),
                this.entry,
                this.generalDocument.outputFactory(),
                this.generalDocument.encoding(),
                markupBuilder.build()
            )
        );
        return documentPart;
    }

    private RunProperties runProperties(final StartElement startElement) throws XMLStreamException {
        final StartElementContext startElementContext = new StartElementContext(
            startElement,
            this.eventReader,
            this.generalDocument.presetColorValues(),
            this.highlightColorValues,
            this.generalDocument.eventFactory(),
            this.generalDocument.conditionalParameters()
        );
        return new RunPropertiesParser(
            startElementContext,
            new RunSkippableElements(startElementContext)
        ).parse();
    }

    @Override
    public void logEvent(Event e) {
        LOGGER.trace("[[ {}: {} ]]", getClass().getSimpleName(), entry.getName());
    }
}
