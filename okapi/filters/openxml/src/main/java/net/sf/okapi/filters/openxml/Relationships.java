/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndDocument;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

interface Relationships {
	String NAME = "Relationships";
	String UNEXPECTED_NUMBER_OF_RELATIONSHIPS = "Unexpected number of relationships";

	StartDocument startDocument();
	StartElement startElement();
	EndElement endElement();
	EndDocument endDocument();
	void readWith(final XMLEventReader eventReader) throws XMLStreamException;
	void addFrom(final String type, final String target) throws XMLStreamException;
	Iterator<Relationship> iterator();
	Relationships with(final String id);
	Relationships of(final String type);
	boolean availableWith(final String targetMode);
	Markup asMarkup();

	final class Default implements Relationships {
		private final XMLEventFactory eventFactory;
		private final RelationshipIdGeneration relationshipIdGeneration;
		private final PartPath basePath;
		private final List<Relationship> relationships;
		private StartDocument startDocument;
		private StartElement startElement;
		private EndElement endElement;
		private EndDocument endDocument;

		Default(
			final XMLEventFactory eventFactory,
			final RelationshipIdGeneration relationshipIdGeneration,
			final PartPath basePath
		) {
			this(
				eventFactory,
				relationshipIdGeneration,
				basePath,
				new LinkedList<>()
			);
		}

		Default(
			final XMLEventFactory eventFactory,
			final RelationshipIdGeneration relationshipIdGeneration,
			final PartPath basePath,
			final List<Relationship> relationships
		) {
			this.eventFactory = eventFactory;
			this.relationshipIdGeneration = relationshipIdGeneration;
			this.basePath = basePath;
			this.relationships = relationships;
		}

		@Override
		public StartDocument startDocument() {
			return this.startDocument;
		}

		@Override
		public StartElement startElement() {
			return this.startElement;
		}

		@Override
		public EndElement endElement() {
			return this.endElement;
		}

		@Override
		public EndDocument endDocument() {
			return this.endDocument;
		}

		@Override
		public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
			while (eventReader.hasNext()) {
				final XMLEvent e = eventReader.nextEvent();
				if (e.isEndDocument()) {
					this.endDocument = (EndDocument) e;
					break;
				}
				if (e.isStartDocument()) {
					this.startDocument = (StartDocument) e;
					continue;
				}
				if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
					this.endElement = e.asEndElement();
					continue;
				}
				if (e.isStartElement() && Relationships.NAME.equals(e.asStartElement().getName().getLocalPart())) {
					this.startElement = e.asStartElement();
					continue;
				}
				if (e.isStartElement() && Relationship.NAME.equals(e.asStartElement().getName().getLocalPart())) {
					final Relationship r = new Relationship.Default(
						this.eventFactory,
						this.basePath,
						e.asStartElement()
					);
					r.readWith(eventReader);
					this.relationshipIdGeneration.refineLastIndexWith(r.id());
					this.relationships.add(r);
				}
			}
		}

		@Override
		public void addFrom(final String type, final String target) throws XMLStreamException {
			final List<Attribute> attributes = Arrays.asList(
				this.eventFactory.createAttribute(Relationship.ID, this.relationshipIdGeneration.nextId()),
				this.eventFactory.createAttribute(Relationship.TYPE, type),
				this.eventFactory.createAttribute(Relationship.TARGET, target)
			);
			this.relationships.add(
				new Relationship.Default(
					this.eventFactory,
					this.basePath,
					this.eventFactory.createStartElement(
						this.startElement.getName().getPrefix(),
						this.startElement.getName().getNamespaceURI(),
						Relationship.NAME,
						attributes.iterator(),
						null
					)
				)
			);
		}

		@Override
		public Iterator<Relationship> iterator() {
			return this.relationships.iterator();
		}

		@Override
		public Relationships with(final String id) {
			return filteredBy(r -> id.equals(r.id()));
		}

		private Relationships filteredBy(final Predicate<Relationship> predicate) {
			final List<Relationship> relationshipList = this.relationships.stream()
				.filter(predicate)
				.collect(Collectors.toList());
			final Default relationships = new Default(
				this.eventFactory,
				this.relationshipIdGeneration,
				this.basePath,
				relationshipList
			);
			relationships.startDocument = this.startDocument;
			relationships.startElement = this.startElement;
			relationships.endElement = this.endElement;
			relationships.endDocument = this.endDocument;
			return relationships;
		}

		@Override
		public Relationships of(final String type) {
			return filteredBy(r -> type.equals(r.type()));
		}

		@Override
		public boolean availableWith(final String targetMode) {
			return filteredBy(r -> targetMode.equals(r.targetMode())).iterator().hasNext();
		}

		@Override
		public Markup asMarkup() {
			final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new ArrayList<>()));
			mb.add(this.startDocument);
			mb.add(this.startElement);
			this.relationships.forEach(r -> mb.add(r.asMarkup()));
			mb.add(this.endElement);
			mb.add(this.endDocument);
			return mb.build();
		}
	}
}
