/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import com.twelvemonkeys.io.ole2.CompoundDocument;
import com.twelvemonkeys.io.ole2.CorruptDocumentException;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.exceptions.OkapiEncryptedDataException;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.skeleton.ZipSkeleton;
import net.sf.okapi.filters.openxml.ContentTypes.Values.Excel;
import net.sf.okapi.filters.openxml.ContentTypes.Values.Powerpoint;
import net.sf.okapi.filters.openxml.ContentTypes.Values.Word;

import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.ByteOrder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

interface Document {
	Event open() throws IOException, XMLStreamException;
	PresetColorValues highlightColorValues();
	boolean isStyledTextPart(final ZipEntry entry);
	StyleDefinitions styleDefinitionsFor(final ZipEntry entry);
	boolean hasNextPart();
	Part nextPart() throws IOException, XMLStreamException;
	void close() throws IOException;

	class General implements Document {
		private static final String UNSUPPORTED_MAIN_DOCUMENT_PART = "Unsupported main document part";
		private static final String UNSUPPORTED_NUMBER_OF_THEMES = "Unsupported number of themes";
		private static final String EMPTY = "";

		private static final String OFFICE_DOCUMENT = "/officeDocument";
		private static final String DOCUMENT = "/document";
		private static final String THEME = "/theme";

		private final ConditionalParameters conditionalParameters;
		private final XMLInputFactory inputFactory;
		private final XMLOutputFactory outputFactory;
		private final XMLEventFactory eventFactory;
		private final DispersedTranslations dispersedTranslations;
		private final String startDocumentId;
		private final URI uri;
		private final LocaleId sourceLocale;
		private final String encoding;
		private final EncoderManager encoderManager;
		private final IFilter subfilter;
		private final IFilterWriter filterWriter;

		private ZipFile zipFile;
		private int currentSubDocumentId;
		private RelationshipIdGeneration relationshipIdGeneration;
		private Relationships rootRelationships;
		private ContentTypes contentTypes;
		private PartPath mainPartBasePath;
		private String mainPartPath;
		private Namespaces2 mainPartNamespaces;
		private Namespace mainPartRelationshipsNamespace;
		private Relationships mainPartRelationships;
		private PresetColorValues presetColorValues;
		private Theme mainPartTheme;
		private Document categorisedDocument;

		General(
			final ConditionalParameters conditionalParameters,
			final XMLInputFactory inputFactory,
			final XMLOutputFactory outputFactory,
			final XMLEventFactory eventFactory,
			final DispersedTranslations dispersedTranslations,
			final String startDocumentId,
			final URI uri,
			final LocaleId sourceLocale,
			final String encoding,
			final EncoderManager encoderManager,
			final IFilter subfilter,
			final IFilterWriter filterWriter
		) {
			this.conditionalParameters = conditionalParameters;
			this.inputFactory = inputFactory;
			this.outputFactory = outputFactory;
			this.eventFactory = eventFactory;
			this.dispersedTranslations = dispersedTranslations;
			this.startDocumentId = startDocumentId;
			this.uri = uri;
			this.sourceLocale = sourceLocale;
			this.encoding = encoding;
			this.encoderManager = encoderManager;
			this.subfilter = subfilter;
			this.filterWriter = filterWriter;
		}

		@Override
		public Event open() throws IOException, XMLStreamException {
			File fZip = new File(uri.getPath());

			if (isEncrypted(fZip)) {
				throw new OkapiEncryptedDataException();
			}

			this.zipFile = new ZipFile(new File(uri.getPath()), ZipFile.OPEN_READ);
			this.currentSubDocumentId = 0;
			this.relationshipIdGeneration = new RelationshipIdGeneration.Default();
			this.rootRelationships = relationshipsFor(EMPTY);
			initializeContentTypes();
			initializeMainPartPathAndRelationshipsNamespace();
			this.mainPartNamespaces = mainPartNamespaces();
			this.mainPartBasePath = basePathFor(this.mainPartPath);
			this.mainPartRelationships = relationshipsFor(this.mainPartPath);
			this.presetColorValues = new PresetColorValues.Default();
			initializeCategorisedDocument();

			return this.categorisedDocument.open();
		}

		private boolean isEncrypted(File file) throws IOException {
			// If you pass the File parameter to the CompoundDocument constructor it
			// opens a file pointer that is not released properly and remains open for
			// the entire lifetime of the application. Using a regular InputStream
			// *also* leaks inside CompoundDocument (see https://github.com/haraldk/TwelveMonkeys/issues/438).
			// By using ImageInputStream we take control of the file pointer and release
			// it properly after the check.
			try (ImageInputStream is = new FileImageInputStream(file)) {
				is.setByteOrder(ByteOrder.LITTLE_ENDIAN);
				new CompoundDocument(is);
				return true;
			} catch (CorruptDocumentException e) {
				return false;
			}
		}

		private void initializeContentTypes() throws XMLStreamException, IOException {
			this.contentTypes = new ContentTypes.Default(this.eventFactory);
			try (final Reader reader = getPartReader(ContentTypes.PART_NAME)) {
				contentTypes.readWith(this.inputFactory.createXMLEventReader(reader));
			}
		}

		private void initializeMainPartPathAndRelationshipsNamespace() throws IOException, XMLStreamException {
			final Iterator<Relationship> relationshipsOfOfficeDocumentSourceTypeIterator =
				this.rootRelationships.of(Namespace.DOCUMENT_RELATIONSHIPS.concat(OFFICE_DOCUMENT)).iterator();
			final Iterator<Relationship> relationshipsOfStrictOfficeDocumentSourceTypeIterator =
				this.rootRelationships.of(Namespace.STRICT_DOCUMENT_RELATIONSHIPS.concat(OFFICE_DOCUMENT)).iterator();
			final Iterator<Relationship> relationshipsVisioDocumentSourceTypeIterator =
				this.rootRelationships.of(Namespace.VISIO_DOCUMENT_RELATIONSHIPS.concat(DOCUMENT)).iterator();
			if (relationshipsOfOfficeDocumentSourceTypeIterator.hasNext()) {
				this.mainPartPath = relationshipsOfOfficeDocumentSourceTypeIterator.next().target();
				this.mainPartRelationshipsNamespace = new Namespace.Default(Namespace.DOCUMENT_RELATIONSHIPS);
			} else if (relationshipsOfStrictOfficeDocumentSourceTypeIterator.hasNext()) {
				this.mainPartPath = relationshipsOfStrictOfficeDocumentSourceTypeIterator.next().target();
				this.mainPartRelationshipsNamespace = new Namespace.Default(Namespace.STRICT_DOCUMENT_RELATIONSHIPS);
			} else if (relationshipsVisioDocumentSourceTypeIterator.hasNext()) {
				this.mainPartPath = relationshipsVisioDocumentSourceTypeIterator.next().target();
				this.mainPartRelationshipsNamespace = new Namespace.Default(Namespace.VISIO_DOCUMENT_RELATIONSHIPS);
			} else {
				throw new OkapiBadFilterInputException(UNSUPPORTED_MAIN_DOCUMENT_PART);
			}
		}

		/**
		 * Initialises a categorised document which can be one of the following types:
		 *   WordProcessingML or Word,
		 *   PresentationML or Powerpoint,
		 *   SpreadsheetML or Excel,
		 *   Visio
		 */
		private void initializeCategorisedDocument()  {
			switch (contentTypes.with(mainPartPath).iterator().next().value()) {
				case Word.MAIN_DOCUMENT_TYPE:
				case Word.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
				case Word.TEMPLATE_DOCUMENT_TYPE:
				case Word.MACRO_ENABLED_TEMPLATE_DOCUMENT_TYPE:
					this.categorisedDocument = new WordDocument(this);
					break;

				case Powerpoint.MAIN_DOCUMENT_TYPE:
				case Powerpoint.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
				case Powerpoint.SLIDE_SHOW_DOCUMENT_TYPE:
				case Powerpoint.MACRO_ENABLED_SLIDE_SHOW_DOCUMENT_TYPE:
				case Powerpoint.TEMPLATE_DOCUMENT_TYPE:
				case Powerpoint.MACRO_ENABLED_TEMPLATE_DOCUMENT_TYPE:
					this.categorisedDocument = new PowerpointDocument(this);
					break;

				case Excel.MAIN_DOCUMENT_TYPE:
				case Excel.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
				case Excel.TEMPLATE_DOCUMENT_TYPE:
				case Excel.MACRO_ENABLED_TEMPLATE_DOCUMENT_TYPE:
					this.categorisedDocument = new ExcelDocument(this, encoderManager, subfilter);
					break;

				case ContentTypes.Values.Visio.MAIN_DOCUMENT_TYPE:
				case ContentTypes.Values.Visio.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
					this.categorisedDocument = new VisioDocument(this);
					break;

				default:
					throw new OkapiBadFilterInputException(String.format("%s: %s", UNSUPPORTED_MAIN_DOCUMENT_PART, mainPartPath));
			}
		}

		Event startDocumentEvent() {
			StartDocument startDoc = new StartDocument(this.startDocumentId);
			startDoc.setName(this.uri.getPath());
			startDoc.setLocale(this.sourceLocale);
			startDoc.setMimeType(OpenXMLFilter.MIME_TYPE);
			startDoc.setLineBreak(OpenXMLFilter.LINE_BREAK);
			startDoc.setEncoding(this.encoding, false);  // Office 2007 files don't have UTF8BOM
			startDoc.setFilterWriter(this.filterWriter);
			startDoc.setFilterId(OpenXMLFilter.FILTER_ID);
			startDoc.setFilterParameters(this.conditionalParameters);
			ZipSkeleton skel = new ZipSkeleton(this.zipFile, null);

			return new Event(EventType.START_DOCUMENT, startDoc, skel);
		}

		@Override
		public boolean isStyledTextPart(final ZipEntry entry) {
			return this.categorisedDocument.isStyledTextPart(entry);
		}

		@Override
		public StyleDefinitions styleDefinitionsFor(final ZipEntry entry) {
			return this.categorisedDocument.styleDefinitionsFor(entry);
		}

		@Override
		public boolean hasNextPart() {
			return this.categorisedDocument.hasNextPart();
		}

		@Override
		public Part nextPart() throws IOException, XMLStreamException {
			return this.categorisedDocument.nextPart();
		}

		@Override
		public void close() throws IOException {
			if (null != this.categorisedDocument) {
				this.categorisedDocument.close();
			}
			if (null != this.categorisedDocument) {
				zipFile.close();
			}
		}

		ConditionalParameters conditionalParameters() {
			return this.conditionalParameters;
		}

		XMLInputFactory inputFactory() {
			return inputFactory;
		}

		XMLOutputFactory outputFactory() {
			return outputFactory;
		}

		XMLEventFactory eventFactory() {
			return eventFactory;
		}

		DispersedTranslations dispersedTranslations() {
			return this.dispersedTranslations;
		}

		String documentId() {
			return this.startDocumentId;
		}

		String nextSubDocumentId() {
			return String.valueOf(++this.currentSubDocumentId);
		}

		LocaleId sourceLocale() {
			return sourceLocale;
		}

		String encoding() {
			return this.encoding;
		}

		ZipFile zipFile() {
			return zipFile;
		}

		InputStream inputStreamFor(final ZipEntry entry) throws IOException {
			return zipFile.getInputStream(entry);
		}

		Enumeration<? extends ZipEntry> entries() {
			return zipFile.entries();
		}

		RelationshipIdGeneration relationshipIdGeneration() {
			return this.relationshipIdGeneration;
		}

		Relationships rootRelationships() {
			return this.rootRelationships;
		}
		ContentTypes contentTypes() {
			return this.contentTypes;
		}

		String contentTypeFor(final ZipEntry entry) {
			return this.contentTypes.with(entry.getName()).iterator().next().value();
		}

		PartPath mainPartBasePath() {
			return this.mainPartBasePath;
		}

		String mainPartPath() {
			return mainPartPath;
		}

		Namespace mainPartRelationshipsNamespace() {
			return this.mainPartRelationshipsNamespace;
		}

		Relationships mainPartRelationships() {
			return this.mainPartRelationships;
		}

        Namespaces2 mainPartNamespaces() throws XMLStreamException, IOException {
            if (null == this.mainPartNamespaces) {
                this.mainPartNamespaces = namespacesOf(this.zipFile.getEntry(this.mainPartPath));
            }
            return this.mainPartNamespaces;
        }

        Namespaces2 namespacesOf(final ZipEntry entry) throws IOException, XMLStreamException {
			final Namespaces2 namespaces = new Namespaces2.Default();
			try (final Reader reader = getPartReader(entry.getName())) {
				namespaces.readWith(this.inputFactory.createXMLEventReader(reader));
			}
			return namespaces;
		}

		PresetColorValues presetColorValues() {
			return this.presetColorValues;
		}

		@Override
		public PresetColorValues highlightColorValues() {
			return this.categorisedDocument.highlightColorValues();
		}

		Theme mainPartTheme() throws IOException, XMLStreamException {
			if (null == this.mainPartTheme) {
				final Set<String> themes = targetsOf(
					this.mainPartRelationships.of(
						this.mainPartRelationshipsNamespace.uri().concat(THEME)
					)
				);
				if (1 < themes.size()) {
					throw new OkapiBadFilterInputException(UNSUPPORTED_NUMBER_OF_THEMES);
				}
				this.mainPartTheme = new Theme.Default(new PresetColorValues.Default());
				final Iterator<String> iterator = themes.iterator();
				if (iterator.hasNext()) {
					try (final Reader reader = getPartReader(iterator.next())) {
						this.mainPartTheme.readWith(this.inputFactory.createXMLEventReader(reader));
					}
				}
			}
			return this.mainPartTheme;
		}

		/**
		 * Return a reader for the named document part. The encoding passed to
		 * the constructor will be used to decode the content.  Bad things will
		 * happen if you call this on a binary part.
		 *
		 * @param partName name of the part. Should not contain a leading '/'.
		 * @return Reader
		 * @throws IOException if any error is encountered while reading the from the zip file
		 */
		Reader getPartReader(String partName) throws IOException {
			ZipEntry entry = zipFile.getEntry(partName);
			if (entry == null) {
				throw new OkapiBadFilterInputException("File is missing " + partName);
			}
			return new InputStreamReader(zipFile.getInputStream(entry), encoding);
		}

		/**
		 * Find the relationships file for the named part and then parse the relationships.
		 * If no relationships file exists for the specified part, an empty Relationships
		 * object is returned.
		 *
		 * @param partPath The name of the part
		 * @return {@link Relationships} instance
		 * @throws IOException        if any error is encountered while reading the stream
		 * @throws XMLStreamException if any error is encountered while parsing the XML
		 */
		Relationships relationshipsFor(final String partPath) throws IOException, XMLStreamException {
			final Relationships relationships = new Relationships.Default(
				this.eventFactory,
				this.relationshipIdGeneration,
				basePathFor(partPath)
			);
			final PartPath relationshipsPartPath = relationshipsPartPathFor(partPath);
			if (available(relationshipsPartPath.asString())) {
				try (final Reader reader = getPartReader(relationshipsPartPath.asString())) {
					relationships.readWith(this.inputFactory.createXMLEventReader(reader));
				}
			}
			return relationships;
		}

		private PartPath basePathFor(final String partPath) {
			final PartPath p = new PartPath.Default(partPath);
			final PartPath basePath;
			if (1 < p.numberOfParts()) {
				basePath = p.subpath(0, p.numberOfParts() - 1);
			} else {
				basePath = new PartPath.Default(EMPTY);
			}
			return basePath;
		}

		PartPath relationshipsPartPathFor(final String partPath) {
			return basePathFor(partPath)
				.resolve(RelationshipsPart.SUB_PATH)
				.resolve(partNameFrom(partPath).concat(RelationshipsPart.EXTENSION));
		}

		private String partNameFrom(final String partPath) {
			final PartPath p = new PartPath.Default(partPath);
			return p.partFor(p.numberOfParts() - 1).asString();
		}

		PartPath mainPartPathFor(final String relationshipsPartPath) {
			final PartPath basePath = baseRelationshipsPartPathFor(relationshipsPartPath);
			final String name = partNameFrom(relationshipsPartPath);
			return basePath.resolve(name.substring(0, name.indexOf(RelationshipsPart.EXTENSION)));
		}

		Relationships relationshipsFrom(final String relationshipsPartPath) throws IOException, XMLStreamException {
			final Relationships relationships = new Relationships.Default(
				this.eventFactory(),
				this.relationshipIdGeneration(),
				baseRelationshipsPartPathFor(relationshipsPartPath)
			);
			try (final Reader reader = this.getPartReader(relationshipsPartPath)) {
				relationships.readWith(this.inputFactory().createXMLEventReader(reader));
			}
			return relationships;
		}

		private PartPath baseRelationshipsPartPathFor(final String relationshipsPartPath) {
			final PartPath p = new PartPath.Default(relationshipsPartPath);
			final PartPath basePath;
			if (2 < p.numberOfParts()) {
				basePath = p.subpath(0, p.numberOfParts() - 2);
			} else {
				basePath = p.subpath(0, p.numberOfParts() - 1);
			}
			return basePath;
		}

		private boolean available(final String partName) {
			return this.zipFile.getEntry(partName) != null;
		}

		Map<String, String> partsByRelatedPart(final List<String> partPaths, final String relationshipType) throws IOException, XMLStreamException {
			final Map<String, String> result = new HashMap<>();
			for (final String partPath : partPaths) {
				for (final String target : relationshipTargetsFor(partPath, relationshipType)) {
					result.put(target, partPath);
				}
			}
			return result;
		}

		Set<String> targetsOf(final Relationships relationships) {
			final Iterator<Relationship> ri = relationships.iterator();
			final Set<String> targets = new LinkedHashSet<>();
			while (ri.hasNext()) {
				targets.add(ri.next().target());
			}
			return targets;
		}

		Set<String> relationshipTargetsFor(final String name, final String type) throws IOException, XMLStreamException {
			return targetsOf(relationshipsFor(name).of(type));
		}
	}
}
