/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;

interface SerialDateTime {
    LocalDateTime asLocalDateTime();

    class Default implements SerialDateTime {
        private static final int SECONDS_PER_DAY = 24 * 60 * 60;
        private static final BigDecimal NANOSECONDS_PER_DAY = BigDecimal.valueOf(SECONDS_PER_DAY * 1e9);
        private static final BigDecimal HALF_UP_ROUNDING_MILLISECONDS = BigDecimal.valueOf(0.5 * 1e6);
        private final String value;
        private final LocalDateTime baseDateTime;

        Default(final String value, final LocalDateTime baseDateTime) {
            this.value = value;
            this.baseDateTime = baseDateTime;
        }

        @Override
        public LocalDateTime asLocalDateTime() {
            final BigDecimal bd = new BigDecimal(this.value);
            final int days = bd.intValue();
            final BigDecimal fractionOfDay = bd.subtract(BigDecimal.valueOf(days));
            return this.baseDateTime.plusDays(days)
                .plusNanos(
                    fractionOfDay.multiply(NANOSECONDS_PER_DAY)
                        .add(HALF_UP_ROUNDING_MILLISECONDS) // to compensate further truncation
                        .longValue()
                ).truncatedTo(ChronoUnit.MILLIS);
        }
    }

    class Epoch1900Based implements SerialDateTime {
        /**
         * In the 1900 date base system, the lower limit is January 1, 1900, which has serial value 1.
         * The upper-limit is December 31, 9999, which has serial value 2,958,465.
         *
         * For legacy reasons, an implementation using the 1900 date base system shall treat 1900 as
         * though it was a leap year. That is, serial value 59 corresponds to February 28, and
         * serial value 61 corresponds to March 1, the next day, allowing the (non-existent) date
         * February 29 to have the serial value 60.
         */
        private static final LocalDateTime BASE_DATE_TIME = LocalDate.of(1899, Month.DECEMBER, 31).atStartOfDay();
        private final SerialDateTime serialDateTime;

        Epoch1900Based(final String value) {
            this(new Default(value, BASE_DATE_TIME));
        }

        Epoch1900Based(final SerialDateTime serialDateTime) {
            this.serialDateTime = serialDateTime;
        }

        @Override
        public LocalDateTime asLocalDateTime() {
            return this.serialDateTime.asLocalDateTime().isAfter(BASE_DATE_TIME.plusDays(59))
                ? this.serialDateTime.asLocalDateTime().minusDays(1)
                : this.serialDateTime.asLocalDateTime();
        }
    }

    class Epoch1904Based implements SerialDateTime {
        /**
         * In the 1904 date base system, the lower limit is January 1, 1904, which has serial value 0.
         * The upper-limit is December 31, 9999, which has serial value 2,957,003.
         */
        private static final LocalDateTime BASE_DATE_TIME = LocalDate.of(1904, Month.JANUARY, 1).atStartOfDay();
        private final SerialDateTime serialDateTime;

        Epoch1904Based(final String value) {
            this(new Default(value, BASE_DATE_TIME));
        }

        Epoch1904Based(final SerialDateTime serialDateTime) {
            this.serialDateTime = serialDateTime;
        }

        @Override
        public LocalDateTime asLocalDateTime() {
            return this.serialDateTime.asLocalDateTime();
        }
    }
}
