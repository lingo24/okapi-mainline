/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.LocaleId;

import javax.xml.stream.XMLEventFactory;
import java.util.Collections;

import static net.sf.okapi.filters.openxml.Namespaces.DrawingML;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.DRAWING_ALIGNMENT;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.DRAWING_ALIGNMENT_LEFT;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.DRAWING_ALIGNMENT_RIGHT;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_BIDIRECTIONAL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_BIDI_VISUAL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_PROPERTY_LANGUAGE;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_READING_ORDER;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_RIGHT_TO_LEFT;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_RTL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_RTL_COL;

final class MarkupClarificationConfiguration {
    private static final String TRUE_VALUES_ARE_EMPTY = "True values are empty";
    private static final String FALSE_VALUES_ARE_EMPTY = "False values are empty";
    private final ConditionalParameters cparams;
    private final XMLEventFactory eventFactory;
    private final PresetColorValues presetColorValues;
    private final PresetColorValues highlightColorValues;
    private final LocaleId sourceLocale;
    private final LocaleId targetLocale;
    private final DispersedTranslations dispersedTranslations;
    private MarkupComponentClarification sheetViewClarification;
    private MarkupComponentClarification alignmentClarification;
    private MarkupComponentClarification nameTranslationClarification;
    private MarkupComponentClarification sheetTranslationClarification;
    private MarkupComponentClarification formulaTranslationClarification;
    private MarkupComponentClarification presentationClarification;
    private BlockPropertiesClarification tablePropertiesClarification;
    private BlockPropertiesClarification textBodyPropertiesClarification;
    private BlockPropertiesClarification paragraphPropertiesClarification;
    private RunPropertiesClarification runPropertiesClarification;
    private StylesClarification wordStylesClarification;

    MarkupClarificationConfiguration(
        final ConditionalParameters conditionalParameters,
        final XMLEventFactory eventFactory,
        final PresetColorValues presetColorValues,
        final PresetColorValues highlightColorValues,
        final LocaleId sourceLocale,
        final LocaleId targetLocale,
        final DispersedTranslations dispersedTranslations
    ) {
        this.cparams = conditionalParameters;
        this.eventFactory = eventFactory;
        this.presetColorValues = presetColorValues;
        this.highlightColorValues = highlightColorValues;
        this.sourceLocale = sourceLocale;
        this.targetLocale = targetLocale;
        this.dispersedTranslations = dispersedTranslations;
    }

    MarkupComponentClarification sheetViewClarification() {
        return this.sheetViewClarification;
    }

    MarkupComponentClarification alignmentClarification() {
        return this.alignmentClarification;
    }

    MarkupComponentClarification nameTranslationClarification() {
        return this.nameTranslationClarification;
    }

    MarkupComponentClarification sheetTranslationClarification() {
        return this.sheetTranslationClarification;
    }

    MarkupComponentClarification formulaTranslationClarification() {
        return this.formulaTranslationClarification;
    }

    MarkupComponentClarification presentationClarification() {
        return this.presentationClarification;
    }

    BlockPropertiesClarification tablePropertiesClarification() {
        return this.tablePropertiesClarification;
    }

    BlockPropertiesClarification textBodyPropertiesClarification() {
        return this.textBodyPropertiesClarification;
    }

    BlockPropertiesClarification paragraphPropertiesClarification() {
        return this.paragraphPropertiesClarification;
    }

    RunPropertiesClarification runPropertiesClarification() {
        return this.runPropertiesClarification;
    }

    StylesClarification wordStylesClarification() {
        return this.wordStylesClarification;
    }

    void prepareFor(final Nameable nameableMarkupComponent) {
        final ClarificationContext clarificationContext = new ClarificationContext(
            this.cparams,
            new CreationalParameters(
                eventFactory,
                nameableMarkupComponent.getName().getPrefix(),
                nameableMarkupComponent.getName().getNamespaceURI()
            ),
            this.presetColorValues,
            this.highlightColorValues,
            this.sourceLocale,
            this.targetLocale
        );
        final ContentCategoriesDetection contentCategoriesDetection;
        if (Namespace.PREFIX_W.equals(clarificationContext.creationalParameters().getPrefix())) {
            contentCategoriesDetection = new ContentCategoriesDetection.Default(this.targetLocale);
        } else {
            contentCategoriesDetection = new ContentCategoriesDetection.NonApplicable();
        }
        final String propertyDefaultValue = XMLEventHelpers.booleanAttributeTrueValues().stream()
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(TRUE_VALUES_ARE_EMPTY));
        final String propertyDefaultValueWhenAbsent = XMLEventHelpers.booleanAttributeFalseValues().stream()
            .findFirst()
            .orElseThrow(() -> new IllegalStateException(FALSE_VALUES_ARE_EMPTY));
        final ClarifiableAttribute rtlColClarifiableAttribute = new ClarifiableAttribute(
            Namespace.PREFIX_EMPTY,
            LOCAL_RTL_COL,
            XMLEventHelpers.booleanAttributeTrueValues()
        );
        final AttributesClarification bypassAttributesClarification = new AttributesClarification.Bypass();
        final ElementsClarification bypassElementsClarification = new ElementsClarification.Bypass();
        final AttributesClarification rtlAttributesClarification = new AttributesClarification.Default(
            clarificationContext,
            new ClarifiableAttribute(Namespace.PREFIX_EMPTY, LOCAL_RTL, XMLEventHelpers.booleanAttributeTrueValues())
        );
        final AttributesClarification tablePropertiesAttributesClarification;
        final ElementsClarification tablePropertiesElementsClarification;
        final AttributesClarification textBodyPropertiesAttributesClarification;
        final AttributesClarification paragraphPropertiesAttributesClarification;
        final ElementsClarification paragraphPropertiesElementsClarification;
        final ElementsClarification runPropertiesElementsClarificationForStyles;
        final ElementsClarification runPropertiesElementsClarification;
        if (Namespace.PREFIX_A.equals(clarificationContext.creationalParameters().getPrefix())) {
            tablePropertiesAttributesClarification = rtlAttributesClarification;
            tablePropertiesElementsClarification = bypassElementsClarification;
            textBodyPropertiesAttributesClarification = new AttributesClarification.Default(
                clarificationContext,
                rtlColClarifiableAttribute
            );
            paragraphPropertiesAttributesClarification = new AttributesClarification.AlignmentAndRtl(
                clarificationContext,
                Namespace.PREFIX_EMPTY,
                DRAWING_ALIGNMENT,
                DRAWING_ALIGNMENT_LEFT,
                DRAWING_ALIGNMENT_RIGHT,
                LOCAL_RTL,
                XMLEventHelpers.booleanAttributeFalseValues(),
                XMLEventHelpers.booleanAttributeTrueValues()
            );
            paragraphPropertiesElementsClarification = bypassElementsClarification;
            runPropertiesElementsClarificationForStyles = bypassElementsClarification;
            runPropertiesElementsClarification = new ElementsClarification.RunPropertyLang(
                bypassElementsClarification,
                clarificationContext,
                "",
                LOCAL_PROPERTY_LANGUAGE
            );
        } else {
            tablePropertiesAttributesClarification = bypassAttributesClarification;
            tablePropertiesElementsClarification = new ElementsClarification.TableBlockPropertyDefault(
                clarificationContext,
                LOCAL_BIDI_VISUAL
            );
            textBodyPropertiesAttributesClarification = new AttributesClarification.Default(
                new ClarificationContext(
                    cparams,
                    new CreationalParameters(
                        eventFactory,
                        Namespace.PREFIX_A,
                        DrawingML.getURI() // todo #859: should be dynamically obtained
                    ),
                    this.presetColorValues,
                    this.highlightColorValues,
                    sourceLocale,
                    targetLocale
                ),
                rtlColClarifiableAttribute
            );
            paragraphPropertiesAttributesClarification = bypassAttributesClarification;
            paragraphPropertiesElementsClarification = new ElementsClarification.ParagraphBlockPropertyDefault(
                clarificationContext,
                LOCAL_BIDIRECTIONAL,
                propertyDefaultValue,
                propertyDefaultValueWhenAbsent,
                XMLEventHelpers.booleanAttributeFalseValues(),
                XMLEventHelpers.booleanAttributeTrueValues()
            );
            runPropertiesElementsClarificationForStyles = new ElementsClarification.RunPropertyLang(
                new ElementsClarification.RunPropertyDefault(
                    clarificationContext,
                    LOCAL_RTL,
                    propertyDefaultValue,
                    propertyDefaultValueWhenAbsent,
                    XMLEventHelpers.booleanAttributeFalseValues(),
                    XMLEventHelpers.booleanAttributeTrueValues()
                ),
                clarificationContext,
                LOCAL_PROPERTY_LANGUAGE,
                LOCAL_BIDIRECTIONAL
            );
            runPropertiesElementsClarification = new ElementsClarification.RunPropertyBoldItalicsFontSize(
                runPropertiesElementsClarificationForStyles,
                contentCategoriesDetection,
                clarificationContext,
                SkippableElement.RunProperty.RUN_PROPERTY_BOLD.toName().getLocalPart(),
                SkippableElement.RunProperty.RUN_PROPERTY_COMPLEX_SCRIPT_BOLD.toName().getLocalPart(),
                SkippableElement.RunProperty.RUN_PROPERTY_ITALICS.toName().getLocalPart(),
                SkippableElement.RunProperty.RUN_PROPERTY_COMPLEX_SCRIPT_ITALICS.toName().getLocalPart(),
                SkippableElement.RunProperty.RUN_PROPERTY_FONT_SIZE.toName().getLocalPart(),
                SkippableElement.RunProperty.RUN_PROPERTY_COMPLEX_SCRIPT_FONT_SIZE.toName().getLocalPart()
            );
        }
        this.sheetViewClarification = new MarkupComponentClarification.Default(
            new AttributesClarification.Default(
                clarificationContext,
                new ClarifiableAttribute(Namespace.PREFIX_EMPTY, LOCAL_RIGHT_TO_LEFT, XMLEventHelpers.booleanAttributeTrueValues())
            ),
            bypassElementsClarification
        );
        this.alignmentClarification = new MarkupComponentClarification.Default(
            new AttributesClarification.Default(
                clarificationContext,
                new ClarifiableAttribute(Namespace.PREFIX_EMPTY, LOCAL_READING_ORDER, Collections.singleton(XMLEventHelpers.LOCAL_READING_ORDER_RTL_VALUE))
            ),
            bypassElementsClarification
        );
        this.nameTranslationClarification = new MarkupComponentClarification.Default(
            new AttributesClarification.NameTranslation(
                clarificationContext,
                Namespace.PREFIX_EMPTY,
                "name",
                this.dispersedTranslations
            ),
            bypassElementsClarification
        );
        this.sheetTranslationClarification = new MarkupComponentClarification.Default(
            new AttributesClarification.NameTranslation(
                clarificationContext,
                Namespace.PREFIX_EMPTY,
                "sheet",
                this.dispersedTranslations
            ),
            bypassElementsClarification
        );
        this.formulaTranslationClarification = new MarkupComponentClarification.Default(
            bypassAttributesClarification,
            bypassElementsClarification,
            new CharactersClarification.ReferencesTranslation(
                clarificationContext,
                this.dispersedTranslations
            )
        );
        this.presentationClarification = new MarkupComponentClarification.Default(
            rtlAttributesClarification,
            bypassElementsClarification
        );
        this.tablePropertiesClarification =
            new BlockPropertiesClarification.Default(
                clarificationContext,
                BlockProperties.TBL_PR,
                new MarkupComponentClarification.Default(
                    tablePropertiesAttributesClarification,
                    tablePropertiesElementsClarification
                )
            );
        this.textBodyPropertiesClarification = new BlockPropertiesClarification.Default(
            clarificationContext,
            BlockProperties.BODY_PR,
            new MarkupComponentClarification.Default(
                textBodyPropertiesAttributesClarification,
                bypassElementsClarification
            )
        );
        this.paragraphPropertiesClarification = new BlockPropertiesClarification.Paragraph(
            new BlockPropertiesClarification.Default(
                clarificationContext,
                ParagraphBlockProperties.PPR,
                new MarkupComponentClarification.Default(
                    paragraphPropertiesAttributesClarification,
                    paragraphPropertiesElementsClarification
                )
            )
        );
        this.runPropertiesClarification = new RunPropertiesClarification.Default(
            clarificationContext,
            new MarkupComponentClarification.Default(
                bypassAttributesClarification,
                runPropertiesElementsClarification
            )
        );
        this.wordStylesClarification = new StylesClarification.Word(
            this.tablePropertiesClarification,
            this.paragraphPropertiesClarification,
            new RunPropertiesClarification.Default(
                clarificationContext,
                new MarkupComponentClarification.Default(
                    bypassAttributesClarification,
                    runPropertiesElementsClarificationForStyles
                )
            )
        );
    }
}
