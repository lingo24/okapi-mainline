/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.nio.file.FileSystem;
import java.nio.file.Path;

@RunWith(JUnit4.class)
public class PartPathTest {
    @Test
    public void backslashesReplacedWithSlashes() {
        final FileSystem fileSystem = Mockito.mock(FileSystem.class);
        Mockito.when(fileSystem.getSeparator()).thenReturn("\\");
        final Path path = Mockito.mock(Path.class);
        Mockito.when(path.getFileSystem()).thenReturn(fileSystem);
        Mockito.when(path.toString()).thenReturn("\\1\\2\\3");
        final PartPath partPath = new PartPath.Default(path);
        Assertions.assertThat(partPath.asString()).isEqualTo("/1/2/3");
    }

    @Test
    public void slashesRemainUnchanged() {
        final FileSystem fileSystem = Mockito.mock(FileSystem.class);
        Mockito.when(fileSystem.getSeparator()).thenReturn("/");
        final Path path = Mockito.mock(Path.class);
        Mockito.when(path.getFileSystem()).thenReturn(fileSystem);
        Mockito.when(path.toString()).thenReturn("/1/2/3");
        final PartPath partPath = new PartPath.Default(path);
        Assertions.assertThat(partPath.asString()).isEqualTo("/1/2/3");
    }
}
