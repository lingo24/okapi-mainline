/*===========================================================================
  Copyright (C) 2008 Jim Hargrave
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.abstractmarkup.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import net.sf.okapi.common.exceptions.OkapiException;

import org.yaml.snakeyaml.Yaml;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class YamlConfigurationReader implements AutoCloseable {
	private static final String REGEX_META_CHARS_REGEX = "[({^$|})?*+\\[\\]]+";
	private static final Pattern REGEX_META_CHARS_PATTERN = Pattern.compile(REGEX_META_CHARS_REGEX);

	private boolean preserveWhitespace;
	private final Map<String, Object> config;
	private Map<String, Object> elementRules;
	private Map<String, Object> attributeRules;
	private Map<String, Object> elementRegexRules;
	private Map<String, Pattern> elementCompiledRegexRules;
	private Map<String, Object> attributeRegexRules;
	private Map<String, Pattern> attributeCompiledRegexRules;
	private InputStreamReader reader;

	public boolean isPreserveWhitespace() {
		return preserveWhitespace;
	}

	public void setPreserveWhitespace(boolean preserveWhitespace) {
		this.preserveWhitespace = preserveWhitespace;
	}

	/**
	 * Default Tagged Configuration
	 */
	public YamlConfigurationReader() {
		config = new Yaml().load("collapse_whitespace: false\nassumeWellformed: true");
		initialize();
	}

	public YamlConfigurationReader(URL configurationPathAsResource) {
		try {
			reader = new InputStreamReader(configurationPathAsResource.openStream(), StandardCharsets.UTF_8);
			config = new Yaml().load(reader);
			initialize();
		} catch (IOException e) {
			throw new OkapiException(e);
		}
	}

	public YamlConfigurationReader(File configurationFile) {
		try {
			reader = new InputStreamReader(new FileInputStream(configurationFile), StandardCharsets.UTF_8);
			config = new Yaml().load(reader);
			initialize();
		} catch (FileNotFoundException e) {
			throw new OkapiException(e);
		}
	}

	public YamlConfigurationReader(String configurationScript) {
		config = new Yaml().load(configurationScript);
		initialize();
	}
	
	protected void initialize() {
		elementRules = new LinkedHashMap<>();
		attributeRules = new LinkedHashMap<>();
		Map<String, Object> er = (Map<String, Object>) config.get("elements");
		Map<String, Object> ar = (Map<String, Object>) config.get("attributes");
		
		if (er != null) {
			elementRules = er;
		}
		if (ar != null) {
			attributeRules = ar;
		}
		
		elementRegexRules = new LinkedHashMap<>();
		attributeRegexRules = new LinkedHashMap<>();
		findRegexRules();
		compileRegexRules();
	}

	@Override
	public String toString() {
		// FIXME: If rules are added after the fact this is not up to date
		return new Yaml().dump(config);
	}

	/**
	 * Find element or attribute rules
	 */
	public List<Map> getAllRuleTypes(String ruleName) {
		List<Map> rules = new LinkedList<>();
		List<Map> rule = getElementRules(ruleName);
		if (rule != null) {
			rules.addAll(rule);
		}

		rules.addAll(getAttributeRules(ruleName));

		return rules;
	}

	/*
	 * Find all element rules that apply (including regex).
	 * Resolve conflicts and conditions later
	 */
	public List<Map> getElementRules(String ruleName) {
		List<Map> rules = new LinkedList<>();
		Map rule = (Map)elementRules.get(ruleName);
		if (rule != null) {
			rules.add(rule);
		}

		// check our element regex patterns
		rules.addAll(getRegexElementRule(ruleName));

		return rules;
	}

	/*
	 * Find regex element rules only
	 */
	public List<Map> getRegexElementRule(String ruleName) {
		List<Map> rules = new LinkedList<>();
		// check our element regex patterns
		if (!elementRegexRules.isEmpty()) {
			for (Map.Entry<String, Object> e : elementRegexRules.entrySet()) {
				Matcher m = elementCompiledRegexRules.get(e.getKey()).matcher(ruleName);
				if (m.matches()) {
					rules.add((Map) e.getValue());
				}
			}
		}
		return rules;
	}

	/*
	 * Find attribute rules only (including regex)
	 */
	public List<Map> getAttributeRules(String ruleName) {
		List<Map> rules = new LinkedList<>();
		Map rule = (Map)attributeRules.get(ruleName);
		if (rule != null) {
			rules.add(rule);
		}

		// check our element regex patterns
		rules.addAll(getRegexAttributeRules(ruleName));

		return rules;
	}

	/*
	 * Find regex attribute rules only
	 */
	public List<Map> getRegexAttributeRules(String ruleName) {
		List<Map> rules = new LinkedList<>();

		// check our element regex patterns
		if (!attributeRegexRules.isEmpty()) {
			for (Map.Entry<String, Object> e : attributeRegexRules.entrySet()) {
				Matcher m = attributeCompiledRegexRules.get(e.getKey()).matcher(ruleName);
				if (m.matches()) {
					rules.add((Map) e.getValue());
				}
			}
		}
		return rules;
	}

	public Object getProperty(String property) {
		return config.get(property);
	}

	public void addProperty(String property, boolean value) {
		config.put(property, value);
	}

	public void addProperty(String property, String value) {
		config.put(property, value);
	}

	public void addProperty(String property, int value) {
		config.put(property, value);
	}

	public void clearRules() {
		config.clear();
		elementRules.clear();
		attributeRules.clear();
		elementRegexRules.clear();
		elementCompiledRegexRules.clear();
		attributeRegexRules.clear();
		attributeCompiledRegexRules.clear();
	}

	protected void findRegexRules() {
		for (Map.Entry<String, Object> entry : elementRules.entrySet()) {
			try {
				Matcher m = REGEX_META_CHARS_PATTERN.matcher(entry.getKey());
				if (m.find()) {
					elementRegexRules.put(entry.getKey(), entry.getValue());
				}
			} catch (PatternSyntaxException e) {
				throw new IllegalConditionalAttributeException(e);
			}
		}

		for (Map.Entry<String, Object> entry : attributeRules.entrySet()) {
			try {
				Matcher m = REGEX_META_CHARS_PATTERN.matcher(entry.getKey());
				if (m.find()) {
					attributeRegexRules.put(entry.getKey(), entry.getValue());
				}
			} catch (PatternSyntaxException e) {
				throw new IllegalConditionalAttributeException(e);
			}
		}
	}

	protected void compileRegexRules() {
		if (!elementRegexRules.isEmpty()) {
			elementCompiledRegexRules = new LinkedHashMap<>();
			for (String r : elementRegexRules.keySet()) {
				Pattern compiledRegex = Pattern.compile(r);
				elementCompiledRegexRules.put(r, compiledRegex);
			}
		}

		if (!attributeRegexRules.isEmpty()) {
			attributeCompiledRegexRules = new LinkedHashMap<>();
			for (String r : attributeRegexRules.keySet()) {
				Pattern compiledRegex = Pattern.compile(r);
				attributeCompiledRegexRules.put(r, compiledRegex);
			}
		}
	}

	public 	Map<String, Object> getAttributeRules () {
		return attributeRules;
	}

	public 	Map<String, Object> getElementRules () {
		return elementRules;
	}

	@Override
	public void close() throws Exception {
		reader.close();
	}
}
