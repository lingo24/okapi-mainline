/*===========================================================================
  Copyright (C) 2009-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.abstractmarkup.config;

import net.htmlparser.jericho.StartTagType;
import net.htmlparser.jericho.Tag;
import net.sf.okapi.common.encoder.HtmlEncoder;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.filters.abstractmarkup.AbstractMarkupFilter;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Defines extraction rules useful for markup languages such as HTML and XML.
 * <p>
 * Extraction rules can handle the following cases:
 * <p>
 * Default rule - don't extract it.
 * <p>
 * INLINE - Elements that are included with text.
 * <p>
 * EXCLUDED -Element and children that should be excluded from extraction.
 * <p>
 * INCLUDED - Elements and children within EXCLUDED ranges that should be extracted.
 * <p>
 * GROUP - Elements that are grouped together structurally such as lists, tables etc.
 * <p>
 * ATTRIBUTES - Attributes on specific elements which should be extracted. May be translatable or localizable.
 * <p>
 * ATTRIBUTES ANY ELEMENT - Convenience rule for attributes which can occur on any element. May be translatable or
 * localize.
 * <p>
 * TEXT UNIT - Elements whose start and end tags become part of a {@link TextUnit} rather than {@link DocumentPart}.
 * <p>
 * Any of the above rules may have conditional rules based on attribute names and/or values. Conditional rules may be
 * attached to both elements and attributes. More than one conditional rules are evaluated as OR expressions. For
 * example, "type=button" OR "type=default".
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class TaggedFilterConfiguration {
	private static final String GLOBAL_PRESERVE_WHITESPACE = "preserve_whitespace";
	private static final String GLOBAL_EXCLUDE_BY_DEFAULT = "exclude_by_default";
	private static final String INLINE_CDATA = "inlineCdata";
	private static final String INLINE = "INLINE";
	private static final String INLINE_INCLUDED = "INLINE_INCLUDED";
	private static final String INLINE_EXCLUDED = "INLINE_EXCLUDED";
	private static final String GROUP = "GROUP";
	private static final String EXCLUDE = "EXCLUDE";
	private static final String INCLUDE = "INCLUDE";
	private static final String TEXTUNIT = "TEXTUNIT";
	private static final String SCRIPT = "SCRIPT";
	private static final String SERVER = "SERVER";
	private static final String ALL_ELEMENTS_EXCEPT = "allElementsExcept";
	private static final String ONLY_THESE_ELEMENTS = "onlyTheseElements";
	private static final String EQUALS = "EQUALS";
	private static final String NOT_EQUALS = "NOT_EQUALS";
	private static final String MATCHES = "MATCHES";
	private static final String ELEMENT_TYPE = "elementType";
	private static final String WELLFORMED = "assumeWellformed";
	private static final String CLEANUP_HTML = "cleanupHtml";
	private static final String USECODEFINDER = "useCodeFinder";
	private static final String CODEFINDERRULES = "codeFinderRules";
	private static final String GLOBAL_PCDATA_SUBFILTER = "global_pcdata_subfilter";
	private static final String GLOBAL_CDATA_SUBFILTER = "global_cdata_subfilter";
	private static final String CONDITIONS = "conditions";
	private static final String PRESERVE_CONDITION = "preserve";
	private static final String DEFAULT_CONDITION = "default";
	private static final String SIMPLIFIER_RULES = "simplifierRules";
	private static final String ATTRIBUTE_TRANSLATABLE_ATTRIBUTES = "ATTRIBUTE_TRANS";
	private static final String ATTRIBUTE_WRITABLE_ATTRIBUTES = "ATTRIBUTE_WRITABLE";
	private static final String ATTRIBUTE_READ_ONLY_ATTRIBUTES = "ATTRIBUTE_READONLY";
	private static final String ATTRIBUTE_ID_ATTRIBUTES = "ATTRIBUTE_ID";
	private static final String ELEMENT_TRANSLATABLE_ATTRIBUTES = "translatableAttributes";
	private static final String ELEMENT_WRITABLE_ATTRIBUTES = "writableLocalizableAttributes";
	private static final String ELEMENT_READ_ONLY_ATTRIBUTES = "readOnlyLocalizableAttributes";
	private static final String ELEMENT_ID_ATTRIBUTES = "idAttributes";

	/**
	 * {@link AbstractMarkupFilter} rule types. These rules are listed in YAML configuration files and interpreted by
	 * the {@link TaggedFilterConfiguration} class.
	 * 
	 * @author HargraveJE
	 * 
	 */
	public enum RULE_TYPE {
		/**
		 * Tag that exists inside a text run, i.e., bold, underline etc.
		 */
		INLINE_ELEMENT(INLINE),
		INLINE_ELEMENT_FAIL("INLINE_ELEMENT_FAIL"),
		/**
		 * Tag that exists inside a text run, i.e., bold, underline etc. but has been excluded based on another
		 * conditional rule. Treat as a standalone inline code
		 */
		INLINE_EXCLUDED_ELEMENT(INLINE_EXCLUDED),
		INLINE_EXCLUDED_ELEMENT_FAIL("INLINE_EXCLUDED_ELEMENT_FAIL"),
		/**
		 * Tag that exists inside a text run, i.e., bold, underline etc. but has been included based on another
		 * conditional rule. Treat as a standalone inline code
		 */
		INLINE_INCLUDED_ELEMENT(INLINE_INCLUDED),
		INLINE_INCLUDED_ELEMENT_FAIL("INLINE_INCLUDED_ELEMENT_FAIL"),
		/**
		 * Marks the beginning of an excluded block - all content in this block will be filtered as {@link DocumentPart}
		 * s
		 */
		EXCLUDED_ELEMENT(EXCLUDE),
		EXCLUDED_ELEMENT_FAIL("EXCLUDED_ELEMENT_FAIL"),
		/**
		 * Used inside EXCLUDED_ELEMENT to mark exceptions to the excluded rule. Anything marked as INCLUDED_ELEMENT
		 * will be filtered normally (i.e, not excluded)
		 */
		INCLUDED_ELEMENT(INCLUDE),
		INCLUDED_ELEMENT_FAIL("INCLUDED_ELEMENT_FAIL"),
		/**
		 * Marks a tag that is converted to an Okapi Group resource.
		 */
		GROUP_ELEMENT(GROUP),
		GROUP_ELEMENT_FAIL("GROUP_ELEMENT_FAIL"),
		/**
		 * Marks a tag that is converted to an Okapi TextUnit resource.
		 */
		TEXT_UNIT_ELEMENT(TEXTUNIT),
		TEXT_UNIT_ELEMENT_FAIL("TEXT_UNIT_ELEMENT_FAIL"),
		/**
		 * Marks a tag that triggers a preserve whitespace rule.
		 */
		PRESERVE_WHITESPACE("PRESERVE_WHITESPACE"),
		PRESERVE_WHITESPACE_FAIL("PRESERVE_WHITESPACE_FAIL"),

		/**
		 * Marks a tag begins or ends a web script (PHP, Perl, VBA etc.)
		 */
		SCRIPT_ELEMENT(SCRIPT),
		/**
		 * Marks a tag that begins or ends a server side content (SSI)
		 */
		SERVER_ELEMENT(SERVER),
		/**
		 * Attribute rule that defines the attribute as translatable.
		 */
		ATTRIBUTE_TRANS(ATTRIBUTE_TRANSLATABLE_ATTRIBUTES),
		/**
		 * Attribute rule that defines the attribute as writable (or localizable).
		 */
		ATTRIBUTE_WRITABLE(ATTRIBUTE_WRITABLE_ATTRIBUTES),
		/**
		 * Attribute rule that defines the attribute as read-only.
		 */
		ATTRIBUTE_READONLY(ATTRIBUTE_READ_ONLY_ATTRIBUTES),
		/**
		 * Attribute rule that specifies the attribute has an ID.
		 */
		ATTRIBUTE_ID(ATTRIBUTE_ID_ATTRIBUTES),

		/**
		 * Element rule that defines the attribute as translatable.
		 */
		ELEMENT_ATTRIBUTE_TRANS(ELEMENT_TRANSLATABLE_ATTRIBUTES),
		/**
		 * Element rule that defines the attribute as writable (or localizable).
		 */
		ELEMENT_ATTRIBUTE_WRITABLE(ELEMENT_WRITABLE_ATTRIBUTES),
		/**
		 * Element rule that defines the attribute as read-only.
		 */
		ELEMENT_ATTRIBUTE_READONLY(ELEMENT_READ_ONLY_ATTRIBUTES),
		/**
		 * Element rule that specifies the attribute has an ID.
		 */
		ELEMENT_ATTRIBUTE_ID(ELEMENT_ID_ATTRIBUTES),

		/**
		 * Attribute rule that defines the attribute marking preserve whitespace state.
		 */
		ATTRIBUTE_PRESERVE_WHITESPACE("ATTRIBUTE_PRESERVE_WHITESPACE"),

		/**
		 * Attribute rule that defines the attribute marking default whitespace state.
		 */
		ATTRIBUTE_DEFAULT_WHITESPACE("ATTRIBUTE_DEFAULT_WHITESPACE"),

		/**
		 * Element rule specifies a tag where only the attributes require processing.
		 */
		ATTRIBUTES_ONLY("ATTRIBUTES_ONLY"),

		/**
		 * Rule was not found or failed - default rule.
		 */
		RULE_NOT_FOUND("RULE_NOT_FOUND");

		private final String name;

		RULE_TYPE(String s) {
			name = s;
		}

		// use instead of valueOf!!!
		public static RULE_TYPE toEnum(String value) {
			for(RULE_TYPE v : values())
				if(v.equalsName(value)) return v;
			throw new IllegalArgumentException();
		}

		public boolean equalsName(String otherName) {
			// (otherName == null) check is not needed because name.equals(null) returns false
			return name.equalsIgnoreCase(otherName);
		}

		@Override
		public String toString() {
			return this.name;
		}
	}

	public static final EnumSet<RULE_TYPE> ATTRIBUTE_ON_ELEMENT_RULES = EnumSet.of(RULE_TYPE.ELEMENT_ATTRIBUTE_TRANS,
			RULE_TYPE.ELEMENT_ATTRIBUTE_WRITABLE, RULE_TYPE.ELEMENT_ATTRIBUTE_READONLY, RULE_TYPE.ELEMENT_ATTRIBUTE_ID);

	public static final EnumSet<RULE_TYPE> INLINE_AND_EXCLUDE = EnumSet.of(RULE_TYPE.INLINE_ELEMENT,
			RULE_TYPE.EXCLUDED_ELEMENT);

	public static final EnumSet<RULE_TYPE> INLINE_AND_EXCLUDE_FAIL = EnumSet.of(RULE_TYPE.INLINE_ELEMENT_FAIL,
			RULE_TYPE.EXCLUDED_ELEMENT_FAIL);

	public static final EnumSet<RULE_TYPE> INLINE_AND_INCLUDE = EnumSet.of(RULE_TYPE.INLINE_ELEMENT,
			RULE_TYPE.INCLUDED_ELEMENT);

	public static final EnumSet<RULE_TYPE> INLINE_AND_INCLUDE_FAIL = EnumSet.of(RULE_TYPE.INLINE_ELEMENT_FAIL,
			RULE_TYPE.INCLUDED_ELEMENT_FAIL);

	public static final EnumSet<RULE_TYPE> FAILED = EnumSet.of(RULE_TYPE.INLINE_EXCLUDED_ELEMENT_FAIL,
						RULE_TYPE.INLINE_INCLUDED_ELEMENT_FAIL,
						RULE_TYPE.INLINE_ELEMENT_FAIL,
						RULE_TYPE.GROUP_ELEMENT_FAIL,
						RULE_TYPE.EXCLUDED_ELEMENT_FAIL,
						RULE_TYPE.INCLUDED_ELEMENT_FAIL,
						RULE_TYPE.TEXT_UNIT_ELEMENT_FAIL,
						RULE_TYPE.PRESERVE_WHITESPACE_FAIL);

	private final YamlConfigurationReader configReader;

	public TaggedFilterConfiguration() {
		configReader = new YamlConfigurationReader();
	}

	public TaggedFilterConfiguration(URL configurationPathAsResource) {
		configReader = new YamlConfigurationReader(configurationPathAsResource);
	}

	public TaggedFilterConfiguration(File configurationFile) {
		configReader = new YamlConfigurationReader(configurationFile);
	}

	public TaggedFilterConfiguration(String configurationScript) {
		configReader = new YamlConfigurationReader(configurationScript);
	}

	public YamlConfigurationReader getConfigReader() {
		return configReader;
	}

	@Override
	public String toString() {
		return configReader.toString();
	}

	public boolean isGlobalPreserveWhitespace() {
		Boolean pw = (Boolean) configReader.getProperty(GLOBAL_PRESERVE_WHITESPACE);
		if (pw == null) {
			// default is preserve whitespace
			return true;
		}
		return pw;
	}
	
	public boolean isGlobalExcludeByDefault() {
		Boolean pw = (Boolean) configReader.getProperty(GLOBAL_EXCLUDE_BY_DEFAULT);
		if (pw == null) {
			// The default is to include if no other rule is specified
			return false;
		}
		return pw;
	}

	public boolean isWellformed() {
		Boolean wf = (Boolean) configReader.getProperty(WELLFORMED);
		if (wf == null) {
			return false;
		}
		return wf;
	}

	public boolean shouldCleanupHtml() {
		Boolean cleanup = (Boolean) configReader.getProperty(CLEANUP_HTML);
		if (cleanup == null) {
			// true by default
			return true;
		}
		return cleanup;
	}
	
	public boolean isInlineCdata() {
		Boolean ic = (Boolean) configReader.getProperty(INLINE_CDATA);
		if (ic == null) {
			return false;
		}
		return ic;
	}

	public boolean isUseCodeFinder() {
		Boolean useCF = (Boolean) configReader.getProperty(USECODEFINDER);
		if ( useCF == null ) {
			return false;
		}
		else {
			return useCF;
		}
	}
	
	public boolean getBooleanParameter (String parameterName) {
		Boolean res = (Boolean)configReader.getProperty(parameterName);
		if ( res == null ) {
			return false;
		}
		else {
			return res;
		}
	}
	
	public int getIntegerParameter (String parameterName) {
		Integer res = (Integer)configReader.getProperty(parameterName);
		if ( res == null ) {
			return -1;
		}
		else {
			return res;
		}
	}
	
	public String getStringParameter (String parameterName) {
		return (String)configReader.getProperty(parameterName);
	}

	public void setBooleanParameter(String name, boolean value) {
		configReader.addProperty(name, value);
	}

	public void setStringParameter(String name, String value) {
		configReader.addProperty(name, value);
	}

	public void setIntegerParameter(String name, int value) {
		configReader.addProperty(name, value);
	}


	public String getGlobalPCDATASubfilter() {
		return (String) configReader.getProperty(GLOBAL_PCDATA_SUBFILTER);
	}

	public String getGlobalCDATASubfilter() {
		return (String) configReader.getProperty(GLOBAL_CDATA_SUBFILTER);
	}

	public String getCodeFinderRules() {
		return (String) configReader.getProperty(CODEFINDERRULES);
	}

	public String getElementType(Tag element) {
		if (element.getTagType() == StartTagType.COMMENT) {
			return Code.TYPE_COMMENT;
		}

		if (element.getTagType() == StartTagType.XML_PROCESSING_INSTRUCTION) {
			return Code.TYPE_XML_PROCESSING_INSTRUCTION;
		}

		List<Map> rules = configReader.getElementRules(element.getName().toLowerCase());
		if (rules != null && !rules.isEmpty()) {
			// return the first type found assuming RULE_TYPE's are in priority order
			for(Map r : rules) {
				if (r.containsKey(ELEMENT_TYPE)) {
					return (String)r.get(ELEMENT_TYPE);
				}
			}
		}

		return element.getName();
	}

	public EnumSet<RULE_TYPE> getAttributeRuleTypes(String attribute, String tag, Map<String, String> attributes) {
		List<Map> rules = configReader.getAttributeRules(attribute.toLowerCase());
		EnumSet<RULE_TYPE> types = EnumSet.noneOf(RULE_TYPE.class);
		String elementTag = tag == null ? "" : tag.toLowerCase();

		// shortcut if there are no attributes as we have nothing to test conditions
		if (attributes == null || attributes.isEmpty())  {
			return getAttributeRuleTypes(attribute.toLowerCase(), elementTag);
		}

		// record all the rules that apply.
		if (rules != null && !rules.isEmpty()) {
			for (Map r : rules) {
				if (doesAttributeRuleConditionApply(r, attributes) && !doesElementFilterApply(elementTag, r)) {
					List<String> ruleTypes = (List<String>) r.get("ruleTypes");
					for (String t : ruleTypes) {
						// adjust preserve whitespace
						if (RULE_TYPE.toEnum(t) == RULE_TYPE.ATTRIBUTE_PRESERVE_WHITESPACE) {
							List preserveWhiteSpace = (List) r.get(PRESERVE_CONDITION);
							List defaultWhiteSpace = (List) r.get(DEFAULT_CONDITION);

							// mutually exclusive
							if (preserveWhiteSpace != null && applyConditions(preserveWhiteSpace, attributes)) {
								types.add(RULE_TYPE.ATTRIBUTE_PRESERVE_WHITESPACE);
							} else if (defaultWhiteSpace != null && applyConditions(defaultWhiteSpace, attributes)) {
								types.add(RULE_TYPE.ATTRIBUTE_DEFAULT_WHITESPACE);
							}
						} else {
							types.add(RULE_TYPE.toEnum(t));
						}
					}
				}
			}

			return types;
		}

		return types;
	}

	public EnumSet<RULE_TYPE> getAttributeRuleTypes(String attribute) {
		return getAttributeRuleTypes(attribute, null);
	}

	public EnumSet<RULE_TYPE> getAttributeRuleTypes(String attribute, String tag) {
		String lowerAttribute = attribute.toLowerCase();
		List<Map> rules = configReader.getAttributeRules(lowerAttribute);
		EnumSet<RULE_TYPE> types = EnumSet.noneOf(RULE_TYPE.class);
		String elementTag = tag == null ? "" : tag.toLowerCase();


		// record all the rules that apply.
		if (rules != null && !rules.isEmpty()) {
			for (Map r : rules) {
				// if there is a condition the rule can't apply by default
				// since we have nothing to test
				if (r.get(CONDITIONS) == null && !doesElementFilterApply(elementTag, r)) {
					List<String> ruleTypes = (List<String>) r.get("ruleTypes");
					for (String t : ruleTypes) {
						// cannot evaluate ATTRIBUTE_PRESERVE_WHITESPACE without attributes
						if (RULE_TYPE.toEnum(t) != RULE_TYPE.ATTRIBUTE_PRESERVE_WHITESPACE) {
							types.add(RULE_TYPE.toEnum(t));
						}
					}
				}
			}

			return types;
		}

		return types;
	}

	/**
	 * Get all the {@link RULE_TYPE}s for attributes found on element rules.
	 * @param tag
	 * @param attribute
	 * @param attributes
	 * @return
	 */
	public EnumSet<RULE_TYPE> getAttributeOnElementRuleTypes(String tag, String attribute, Map<String, String> attributes) {
		List<Map> rules = configReader.getElementRules(tag.toLowerCase());
		EnumSet<RULE_TYPE> types = EnumSet.noneOf(RULE_TYPE.class);

		// search for attribute rules on the element rules
		if (rules != null && !rules.isEmpty()) {
			for (Map r : rules) {
				if (hasAttributeRules(r)) {
					for (RULE_TYPE a : ATTRIBUTE_ON_ELEMENT_RULES) {
						if (doesAttributeRuleOnElementRuleConditionApply(r, a.toString(), attribute, attributes)) {
							types.add(a);
						}
					}
				}
			}
		}

		return types;
	}

	private boolean hasAttributeRules(Map r) {
		return r.containsKey(ELEMENT_TRANSLATABLE_ATTRIBUTES) ||
				r.containsKey(ELEMENT_READ_ONLY_ATTRIBUTES) ||
				r.containsKey(ELEMENT_WRITABLE_ATTRIBUTES) ||
				r.containsKey(ELEMENT_ID_ATTRIBUTES);
	}

	public EnumSet<RULE_TYPE> getElementRuleTypes(String tag, Map<String, String> attributes, boolean isStartTag) {
		List<Map> rules = configReader.getElementRules(tag.toLowerCase());
		EnumSet<RULE_TYPE> types = EnumSet.noneOf(RULE_TYPE.class);

		// record all the rules that apply.
		if (rules != null && !rules.isEmpty()) {
			for (Map r : rules) {
				List<String> ruleTypes;
				ruleTypes = (List<String>) r.get("ruleTypes");
				for (String t : ruleTypes) {
					RULE_TYPE rt = RULE_TYPE.toEnum(t);
					boolean ruleApplied = true;
					// end tags have ruleApplied = true since there are no attributes to check.
					// we disambiguate later with the corresponding start tag
					if (isStartTag) {
						ruleApplied	= doesElementRuleConditionApply(r, attributes);
					}
					// must preserve these even if the condition does not apply, so
					// we can later match with end tags otherwise we won't be able to
					// distinguish between dangling, unbalanced tags
					switch(rt) {
						case INLINE_EXCLUDED_ELEMENT:
							types.add(ruleApplied ? rt : RULE_TYPE.INLINE_EXCLUDED_ELEMENT_FAIL);
							break;
						case INLINE_INCLUDED_ELEMENT:
							types.add(ruleApplied ? rt : RULE_TYPE.INLINE_INCLUDED_ELEMENT_FAIL);
							break;
						case INLINE_ELEMENT:
							types.add(ruleApplied ? rt : RULE_TYPE.INLINE_ELEMENT_FAIL);
							break;
						case GROUP_ELEMENT:
							types.add(ruleApplied ? rt : RULE_TYPE.GROUP_ELEMENT_FAIL);
							break;
						case EXCLUDED_ELEMENT:
							types.add(ruleApplied ? rt : RULE_TYPE.EXCLUDED_ELEMENT_FAIL);
							break;
						case INCLUDED_ELEMENT:
							types.add(ruleApplied ? rt : RULE_TYPE.INCLUDED_ELEMENT_FAIL);
							break;
						case TEXT_UNIT_ELEMENT:
							types.add(ruleApplied ? rt : RULE_TYPE.TEXT_UNIT_ELEMENT_FAIL);
							break;
						case PRESERVE_WHITESPACE:
							types.add(ruleApplied ? rt : RULE_TYPE.PRESERVE_WHITESPACE_FAIL);
							break;
						default:
							// if the condition was true then add the rule type
							if (ruleApplied) {
								types.add(rt);
							}
							break;
					}
				}
			}
			return types;
		}
		return types;
	}

	/**
	 * Go through all matched rules (including regex) and record the {@link RULE_TYPE}
	 * Any rules with conditions are automatically false since we have no attributes.
	 * @param tag the markup tag (converted to lowercase for search)
	 * @param isEndTag is this tag an ending tag?
	 * @return all matching {@link RULE_TYPE} as an {@link EnumSet}
	 */
	public EnumSet<RULE_TYPE> getElementRuleTypes(String tag, boolean isStartTag) {
		return getElementRuleTypes(tag, Collections.<String, String>emptyMap(), isStartTag);
	}

	private boolean applyConditions(List condition, Map<String, String> attributes) {
		String conditionalAttribute;
		conditionalAttribute = (String) condition.get(0);

		// '=', '!=' or regex
		String compareType = (String) condition.get(1);

		if (condition.get(2) instanceof List) {
			List<String> conditionValues = (List<String>) condition.get(2);

			// multiple condition values of type NOT_EQUAL are AND'ed together
			// any single failed match returns false for the expression
			if (compareType.equalsIgnoreCase(NOT_EQUALS)) {
				for (String value : conditionValues) {
					if (!applyCondition(attributes.get(conditionalAttribute.toLowerCase()),
							compareType, value)) {
						return false;
					}
				}
				return true;
			} else {
				// multiple condition values of type EQUAL or MATCH are OR'ed together
				// any single successful match returns true for the expression
				for (String value : conditionValues) {
					if (applyCondition(attributes.get(conditionalAttribute.toLowerCase()),
							compareType, value)) {
						return true;
					}
				}
			}
		} else if (condition.get(2) instanceof String) {
			String conditionValue = (String) condition.get(2);
			return applyCondition(attributes.get(conditionalAttribute.toLowerCase()), compareType,
					conditionValue);
		} else {
			throw new IllegalConditionalAttributeException(
					"Error reading conditions. Have you quoted values such as 'true', 'false', 'yes', and 'no'?");
		}

		return false;
	}

	private boolean applyCondition(String attributeValue, String compareType, String conditionValue) {
		if (compareType.equalsIgnoreCase(EQUALS)) {
			return conditionValue.equalsIgnoreCase(attributeValue);
		} else if (compareType.equalsIgnoreCase(NOT_EQUALS)) {
			return !conditionValue.equalsIgnoreCase(attributeValue);
		} else if (compareType.equalsIgnoreCase(MATCHES)) {
			boolean result;
			// in some cases callers send down an attribute from a condition that is
			// not an actual attribute on the current tag. Rule immediately fails in this case
			if (attributeValue == null) {
				return false;
			}
			
			Pattern matchPattern = Pattern.compile(conditionValue);
			try {
				Matcher m = matchPattern.matcher(attributeValue);
				result = m.matches();
			} catch (PatternSyntaxException e) {
				throw new IllegalConditionalAttributeException(e);
			}
			return result;
		} else {
			throw new IllegalConditionalAttributeException(String.format("Unknown match type: %s", compareType));
		}
	}

	public boolean doesElementRuleConditionApply(Map elementRule, Map<String, String> attributes) {
		if (elementRule == null) {
			return false;
		}
		List conditions = (List) elementRule.get(CONDITIONS);
		if (conditions != null) {
			return applyConditions(conditions, attributes);
		}

		return true;
	}


	private boolean doesAttributeRuleOnElementRuleConditionApply(Map elementRule,
																 String attributeRuleName,
																 String attribute,
																 Map<String, String> attributes) {
		if (elementRule == null) {
			return false;
		}

		Object ta = elementRule.get(attributeRuleName);
		if (ta == null) {
			return false;
		}

		if (ta instanceof List) {
			List<String> actionableAttributes = (List<String>) elementRule.get(attributeRuleName);
			for (String a : actionableAttributes) {
				if (a.equalsIgnoreCase(attribute)) {
					return true;
				}
			}

		} else if (ta instanceof Map) {
			Map actionableAttributes = (Map) elementRule.get(attributeRuleName);
			if (actionableAttributes.containsKey(attribute.toLowerCase())) {
				List condition = (List) actionableAttributes.get(attribute.toLowerCase());
				// case where there is no condition applied to attribute
				if (condition == null) {
					return true;
				} else {
					// apply conditions
					if (condition.get(0) instanceof List) {
						// We have multiple conditions - individual results are
						// OR'ed together
						// so only one condition need be true for the rule to
						// apply
						for (int i = 0; i <= condition.size() - 1; i++) {
							List c = (List) condition.get(i);
							if (applyConditions(c, attributes)) {
								return true;
							}
						}
					} else {
						// single condition
						if (applyConditions(condition, attributes)) {
							return true;
						}
					}
				}
			}
		}

		// if we get to this point the condition must have failed
		return false;
	}

	private boolean doesAttributeRuleConditionApply(Map attributeRule, Map<String, String> attributes) {
		if (attributeRule == null) {
			return false;
		}
		List conditions = (List) attributeRule.get(CONDITIONS);
		if (conditions != null) {
			return applyConditions(conditions, attributes);
		}

		return true;
	}

	private boolean doesElementFilterApply(String tag, Map attributeRule) {
		List<String> excludedElements;
		List<String> onlyTheseElements;

		excludedElements = (List<String>) attributeRule.get(ALL_ELEMENTS_EXCEPT);
		onlyTheseElements = (List<String>) attributeRule.get(ONLY_THESE_ELEMENTS);

		if (excludedElements == null && onlyTheseElements == null) {
			// means no element exceptions - all tags can have this attribute/rule
			return false;
		}

		// ALL_ELEMENTS_EXCEPT and ONLY_THESE_ELEMENTS are mutually exclusive
		// categories, either one or the other must be true, not both
		if (excludedElements != null) {
			for (int i = 0; i <= excludedElements.size() - 1; i++) {
				String elem = excludedElements.get(i);
				if (elem.equalsIgnoreCase(tag)) {
					return true;
				}
			}
			return false;
		} else {
			for (int i = 0; i <= onlyTheseElements.size() - 1; i++) {
				String elem = onlyTheseElements.get(i);
				if (elem.equalsIgnoreCase(tag)) {
					return false;
				}
			}
			return true;
		}
	}

	public boolean isTranslatableAttribute(String tag, String attribute, Map<String, String> attributes) {
		EnumSet<RULE_TYPE> ruleTypes;
		ruleTypes = getAttributeRuleTypes(attribute, tag, attributes);
		ruleTypes.addAll(getAttributeOnElementRuleTypes(tag, attribute, attributes));
		return ruleTypes.contains(RULE_TYPE.ATTRIBUTE_TRANS) || ruleTypes.contains(RULE_TYPE.ELEMENT_ATTRIBUTE_TRANS);
	}

	public boolean isReadOnlyLocalizableAttribute(String tag, String attribute, Map<String, String> attributes) {
		EnumSet<RULE_TYPE> ruleTypes;
		ruleTypes = getAttributeRuleTypes(attribute, tag, attributes);
		ruleTypes.addAll(getAttributeOnElementRuleTypes(tag, attribute, attributes));
		return ruleTypes.contains(RULE_TYPE.ATTRIBUTE_READONLY) || ruleTypes.contains(RULE_TYPE.ELEMENT_ATTRIBUTE_READONLY);
	}

	public boolean isWritableLocalizableAttribute(String tag, String attribute, Map<String, String> attributes) {
		EnumSet<RULE_TYPE> ruleTypes;
		ruleTypes = getAttributeRuleTypes(attribute, tag, attributes);
		ruleTypes.addAll(getAttributeOnElementRuleTypes(tag, attribute, attributes));
		return ruleTypes.contains(RULE_TYPE.ATTRIBUTE_WRITABLE) || ruleTypes.contains(RULE_TYPE.ELEMENT_ATTRIBUTE_WRITABLE);
	}

	public boolean isIdAttribute(String tag, String attribute, Map<String, String> attributes) {
		EnumSet<RULE_TYPE> ruleTypes;
		ruleTypes = getAttributeRuleTypes(attribute, tag, attributes);
		ruleTypes.addAll(getAttributeOnElementRuleTypes(tag, attribute, attributes));
		return ruleTypes.contains(RULE_TYPE.ATTRIBUTE_ID) || ruleTypes.contains(RULE_TYPE.ELEMENT_ATTRIBUTE_ID);
	}

	public boolean isPreserveWhitespaceCondition(String attribute, Map<String, String> attributes) {
		List<Map> attributeRules = configReader.getAttributeRules(attribute);
		for(Map r: attributeRules) {
			if (doesAttributeRuleConditionApply(r, attributes)) {
				List preserveWhiteSpace = (List<String>) r.get(PRESERVE_CONDITION);
				if (preserveWhiteSpace != null) {
					return applyConditions(preserveWhiteSpace, attributes);
				}
			}
		}
		return false;
	}

	public boolean isDefaultWhitespaceCondition(String attribute, Map<String, String> attributes) {
		List<Map> attributeRules = configReader.getAttributeRules(attribute);
		for(Map r: attributeRules) {
			if (doesAttributeRuleConditionApply(r, attributes)) {
				List defaultWhiteSpace = (List<String>) r.get(DEFAULT_CONDITION);
				if (defaultWhiteSpace != null) {
					return applyConditions(defaultWhiteSpace, attributes);
				}
			}
		}
		return false;
	}

	public String getElementType(String elementName) {
		List<Map> rules = configReader.getElementRules(elementName.toLowerCase());
		for (Map r : rules) {
			if (r != null && r.containsKey(TaggedFilterConfiguration.ELEMENT_TYPE)) {
				return (String) r.get(TaggedFilterConfiguration.ELEMENT_TYPE);
			}
		}
		return elementName;
	}

	public String getSimplifierRules() {
		return (String)configReader.getProperty(SIMPLIFIER_RULES); 
	}
	
	public void setSimplifierRules(String rules) {
		configReader.addProperty(SIMPLIFIER_RULES, rules);
	}
	
	public boolean getQuoteModeDefined() {
		Boolean res = (Boolean)configReader.getProperty(HtmlEncoder.QUOTEMODEDEFINED);
		if ( res == null ) {
			return false;
		}
		else {
			return res;
		} 
	}
	
	public void setQuoteModeDefined(boolean defined) {
		configReader.addProperty(HtmlEncoder.QUOTEMODEDEFINED, defined);
	}
	
	public int getQuoteMode() {
		Integer res = (Integer)configReader.getProperty(HtmlEncoder.QUOTEMODE);
		if ( res == null ) {
			// default for HTML encoder is NUMERIC_SINGLE_QUOTES
			return 2;
		}
		else {
			return res;
		} 
	}
	
	public void setQuoteMode(String quoteMode) {
		configReader.addProperty(HtmlEncoder.QUOTEMODE, quoteMode);
	}
}
