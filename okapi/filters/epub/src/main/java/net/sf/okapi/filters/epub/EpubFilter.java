/*===========================================================================
  Copyright (C) 2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.epub;

import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.archive.ArchiveFilter;
import net.sf.okapi.filters.archive.Parameters;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements the {@link IFilter} interface for EPUB files.
 * This is written on top of the ArchiveFilter class.
 * The entries generated are the ones from the .xhtml files in the sub-folder that
 * is named for the target language.
 */
public class EpubFilter extends ArchiveFilter {
	public static final String MIME_TYPE = "application/epub+zip";
	private final List<FilterConfiguration> configs;
	
	public EpubFilter () {
		super();
		Parameters params = getParameters();
		params.setMimeType(MIME_TYPE);
		params.setSimplifierRules(null);

		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations("net.sf.okapi.filters.html.HtmlFilter");
		setFilterConfigurationMapper(fcMapper);
	
		configs = new ArrayList<>();
		FilterConfiguration fc = new FilterConfiguration(getName(), MIME_TYPE,
			this.getClass().getName(), "EPUB Files", "Electronic Publication Files (EPUB)", null, ".epub;");
		configs.add(fc);
	}
	
	@Override
	public String getName() {
		return "okf_epub";
	}
	
	@Override
	public String getDisplayName() {
		return "EPUB Filter";
	}

	@Override
	// We override the method in AbstractFilter because we cannot set the proper
	// information through the super class (not completely)
	public List<FilterConfiguration> getConfigurations() {
		return configs;
	}

	@Override
	public void open(RawDocument input,
		boolean generateSkeleton)
	{
		// Adjust the filename filter to get only the files in target folder
		// This must be done first because the parameters are used in super.open() 
		// to set the internal ArchiveFilter filenames variable
		Parameters params = getParameters();
		params.setFileNames("*.xhtml,*.html,*.htm");
		params.setConfigIds("okf_html,okf_html,okf_html");

		// Then call the super method
		super.open(input, generateSkeleton);
	}
}
