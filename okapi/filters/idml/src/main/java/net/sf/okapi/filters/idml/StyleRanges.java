/*
 * =============================================================================
 *   Copyright (C) 2010-2017 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.idml;

import net.sf.okapi.common.filters.fontmappings.FontMappings;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static net.sf.okapi.filters.idml.StyleRange.defaultCharacterStyleRange;
import static net.sf.okapi.filters.idml.StyleRange.defaultParagraphStyleRange;

class StyleRanges {

    private final StyleRange paragraphStyleRange;
    private final StyleRange characterStyleRange;

    StyleRanges(StyleRange paragraphStyleRange, StyleRange characterStyleRange) {
        this.paragraphStyleRange = paragraphStyleRange;
        this.characterStyleRange = characterStyleRange;
    }

    static StyleRanges defaultStyleRanges(XMLEventFactory eventFactory) {
        return new StyleRanges(defaultParagraphStyleRange(eventFactory), defaultCharacterStyleRange(eventFactory));
    }

    StyleRange paragraphStyleRange() {
        return paragraphStyleRange;
    }

    StyleRange characterStyleRange() {
        return characterStyleRange;
    }

    // TODO: rewrite when full styles hierarchy is available
    StyleRange combinedStyleRange() {
        final StyleRange.StyleRangeBuilder styleRangeBuilder = new StyleRange.StyleRangeBuilder(
            this.characterStyleRange.eventFactory(),
            new StyleRange.AttributesComparator()
        );
        styleRangeBuilder.setName(this.characterStyleRange.name());
        final List<Attribute> attributes = new ArrayList<>();
        attributes.addAll(this.paragraphStyleRange.attributes());
        attributes.addAll(this.characterStyleRange.attributes());
        styleRangeBuilder.setAttributes(attributes);
        final List<Property> properties = new ArrayList<>(this.paragraphStyleRange.properties().properties());
        properties.addAll(this.characterStyleRange.properties().properties());
        final Properties.Builder propertiesBuilder = new Properties.Builder(new Properties.Comparator());
        propertiesBuilder.setStartElement(this.characterStyleRange.properties().startElement())
            .setEndElement(this.characterStyleRange.properties().endElement())
            .addProperties(properties);
        return styleRangeBuilder.setProperties(propertiesBuilder.build()).build();
    }

    boolean isSubsetOf(StyleRanges other) {
        return paragraphStyleRange().isSubsetOf(other.paragraphStyleRange())
            && characterStyleRange().isSubsetOf(other.characterStyleRange());
    }

    int amount() {
        return paragraphStyleRange.attributes().size()
            + paragraphStyleRange.properties().properties().size()
            + characterStyleRange.attributes().size()
            + characterStyleRange.properties().properties().size();
    }

    void apply(final FontMappings fontMappings) {
        this.paragraphStyleRange.apply(fontMappings);
        this.characterStyleRange.apply(fontMappings);
    }

    StyleRanges mergedWith(final StyleRanges styleRanges) {
        return new StyleRanges(
            this.paragraphStyleRange.mergedWith(styleRanges.paragraphStyleRange()),
            this.characterStyleRange.mergedWith(styleRanges.characterStyleRange())
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (null == o || getClass() != o.getClass()) return false;

        StyleRanges that = (StyleRanges) o;

        return Objects.equals(paragraphStyleRange(), that.paragraphStyleRange())
            && Objects.equals(characterStyleRange(), that.characterStyleRange());
    }

    @Override
    public int hashCode() {
        return Objects.hash(paragraphStyleRange(), characterStyleRange());
    }
}
