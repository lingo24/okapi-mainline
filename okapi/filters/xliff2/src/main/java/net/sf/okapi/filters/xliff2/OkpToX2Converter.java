/*===========================================================================
  Copyright (C) 2019 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================*/

package net.sf.okapi.filters.xliff2;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.exceptions.OkapiMergeException;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.StartSubDocument;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextPartComparator;
import net.sf.okapi.common.resource.WhitespaceStrategy;
import net.sf.okapi.filters.xliff2.model.XLIFF2PropertyStrings;
import net.sf.okapi.filters.xliff2.util.NotesMapper;
import net.sf.okapi.filters.xliff2.util.PropertiesMapper;
import net.sf.okapi.lib.xliff2.core.CTag;
import net.sf.okapi.lib.xliff2.core.Fragment;
import net.sf.okapi.lib.xliff2.core.MTag;
import net.sf.okapi.lib.xliff2.core.MidFileData;
import net.sf.okapi.lib.xliff2.core.Part;
import net.sf.okapi.lib.xliff2.core.StartFileData;
import net.sf.okapi.lib.xliff2.core.StartGroupData;
import net.sf.okapi.lib.xliff2.core.StartXliffData;
import net.sf.okapi.lib.xliff2.core.Tag;
import net.sf.okapi.lib.xliff2.core.TagType;
import net.sf.okapi.lib.xliff2.core.TargetState;
import net.sf.okapi.lib.xliff2.core.Unit;
import net.sf.okapi.lib.xliff2.reader.Event;
import net.sf.okapi.lib.xliff2.reader.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class is designed to convert the Okapi Core structure back into Xliff
 * Toolkit structure.
 */
public class OkpToX2Converter {
	private static final Map<TextFragment.TagType, TagType> codeTagTypeMap = new HashMap<>();
	private static final String ID = "id";

	static {
		codeTagTypeMap.put(TextFragment.TagType.OPENING, TagType.OPENING);
		codeTagTypeMap.put(TextFragment.TagType.CLOSING, TagType.CLOSING);
		codeTagTypeMap.put(TextFragment.TagType.PLACEHOLDER, TagType.STANDALONE);
	}

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public OkpToX2Converter() {
	}

	/**
	 * Takes an Okapi Core event and produces a list of XLIFF Toolkit
	 * {@link Event}s. A list of events is produced because Okapi Core isn't a 1 to
	 * 1 map of the XLIFF Toolkit.
	 *
	 * @param okapiEvent         The Okapi Core event
	 * @param xliff2FilterWriter The filter writer being used to write the XLIFF 2.0
	 *                           file
	 * @return The XLIFF Toolkit events that can be saved out to a file.
	 */
	public List<Event> handleEvent(net.sf.okapi.common.Event okapiEvent, XLIFF2FilterWriter xliff2FilterWriter) {
		net.sf.okapi.common.EventType eventType = okapiEvent.getEventType();

		switch (eventType) {

		case START_DOCUMENT:
			return startDocument(okapiEvent.getStartDocument(), xliff2FilterWriter);
		case END_DOCUMENT:
			return endDocument(okapiEvent.getEnding());
		case START_SUBDOCUMENT:
			return startSubDocument(okapiEvent.getStartSubDocument());
		case END_SUBDOCUMENT:
			return endSubDocument();
		case START_GROUP:
			return startGroup(okapiEvent.getStartGroup());
		case END_GROUP:
			return endGroup(okapiEvent.getEndGroup());
		case TEXT_UNIT:
			return textUnit(okapiEvent.getTextUnit(), xliff2FilterWriter.getTargetLocale());
		case DOCUMENT_PART:
			return documentPart(okapiEvent.getDocumentPart());
		case CUSTOM:
			return Collections.emptyList();
		default:
			throw new OkapiException(
					"Event " + okapiEvent.getEventType() + " is not implemented in XLIFF 2.0 Filter Writer");
		}

	}

	private List<Event> documentPart(DocumentPart documentPart) {
		// check for metadata and skeleton cases
		if (documentPart.getSkeleton() != null) {
			if (documentPart.getSkeleton() instanceof MetadataSkeleton) {
				MidFileData xliffMidFileData = new MidFileData();
				xliffMidFileData.setMetadata(((MetadataSkeleton) documentPart.getSkeleton()).getMetaData());
				Event event = new Event(EventType.MID_FILE, null, xliffMidFileData);
				NotesMapper.setNotes(documentPart, xliffMidFileData);
				return Collections.singletonList(event);
			} else if (documentPart.getSkeleton() instanceof Xliff2Skeleton) {
				Event event = new Event(EventType.SKELETON, null,
						((Xliff2Skeleton) documentPart.getSkeleton()).getXliff2Skeleton());
				return Collections.singletonList(event);
			}
		}

		MidFileData xliffMidFileData = new MidFileData();
		Event event = new Event(EventType.MID_FILE, null, xliffMidFileData);
		NotesMapper.setNotes(documentPart, xliffMidFileData);
		return Collections.singletonList(event);
	}

	private List<Event> textUnit(ITextUnit okapiTextUnit, LocaleId targetLocale) {
		Set<String> sourceCodeIds = new HashSet<>();
		Set<String> targetCodeIds = new HashSet<>();
		Unit xliffTextUnit = new Unit(okapiTextUnit.getId());
		Event event = new Event(EventType.TEXT_UNIT, null, xliffTextUnit);
		Set<LocaleId> availableTargetLocales = okapiTextUnit.getTargetLocales();

		xliffTextUnit.setId(okapiTextUnit.getId());
		xliffTextUnit.setName(okapiTextUnit.getName());
		xliffTextUnit.setType(okapiTextUnit.getType());
		xliffTextUnit.setTranslate(okapiTextUnit.isTranslatable());
		xliffTextUnit.setPreserveWS(okapiTextUnit.preserveWhitespaces());
		if (okapiTextUnit.getSkeleton() != null) {
			xliffTextUnit.setMetadata(((MetadataSkeleton) okapiTextUnit.getSkeleton()).getMetaData());
		}

		PropertiesMapper.setTextUnitProperties(okapiTextUnit, xliffTextUnit);
		NotesMapper.setNotes(okapiTextUnit, xliffTextUnit);

		// Iterate over both the target the source parts together.
		TextContainer okapiSources = okapiTextUnit.getSource();
		TextContainer okapiTargets;
		final TextPartComparator cmp = new TextPartComparator();
		boolean [] matched = new boolean[0];
		if (targetLocale != null && availableTargetLocales.contains(targetLocale)) {
			okapiTargets = okapiTextUnit.getTarget(targetLocale);
			if (okapiTargets.count() != 0 && okapiSources.count() != okapiTargets.count()) {
				logger.warn("Target count doesn't match source count. Misalignment is possible.");
			}
			matched = new boolean[okapiTargets.count()];
			Arrays.fill(matched, false);
		} else {
			okapiTargets = null;
		}

		for (TextPart okapiSourcePart: okapiSources.getParts()){
			// Add the source segments and ignorables
			Part xliffPart;
			if (okapiSourcePart.isSegment()) {
				xliffPart = xliffTextUnit.appendSegment();
				if (xliffPart.getSource() == null) {
					xliffPart.setSource("");
				}
			} else {
				xliffPart = xliffTextUnit.appendIgnorable();
			}

			// if INHERIT then look at parent for preserve whitespace
			if (okapiSourcePart.getWhitespaceStrategy() == WhitespaceStrategy.INHERIT) {
				xliffPart.setPreserveWS(okapiTextUnit.preserveWhitespaces());
			} else {
				xliffPart.setPreserveWS(okapiSourcePart.preserveWhitespaces());
			}

			// reset id back to original - may be null
			String id = okapiSourcePart.getId();
			if (!Util.isEmpty(okapiSourcePart.getOriginalId())) {
				id = okapiSourcePart.getOriginalId();
			}
			xliffPart.setId(id);
			copyOver(okapiSourcePart.getContent(), xliffPart.getSource(), sourceCodeIds);
			PropertiesMapper.setPartProperties(okapiSourcePart, xliffPart);

			// Add the targets and target ignorables
			// Matching source and target must be done with id's ideally. Need to make sure the original xliff2
			// id's always survive. But there are cases where xliff2 doesn't have id's which makes it hard to align.
			// Also roundtrip through xliff 1.2 can lose id's as non-segment content doesn't have them.
			TextPart okapiTargetPart;
			if (okapiTargets != null) {
				okapiTargetPart = Util.findMatch(okapiSourcePart, okapiTargets.getParts(), matched, cmp);
				if (okapiTargetPart == null) {
					logger.warn("TextUnit id='{}' TextPart id='{}': Can't find matching target TextPart",
							okapiTextUnit.getId(),
							okapiSourcePart.id);
				} else {
					// if the segment state is not "initial" then we must write out a target even if it is empty
					if ((okapiSourcePart.hasProperty(Property.STATE) &&
							!TargetState.INITIAL.toString().equals(okapiSourcePart.getProperty(Property.STATE).getValue())) ||
							!okapiTargetPart.getContent().isEmpty()) {
							if (xliffPart.getTarget() == null) {
								xliffPart.setTarget("");
							}
							copyOver(okapiTargetPart.getContent(), xliffPart.getTarget(), targetCodeIds);
							PropertiesMapper.setStateProperty(okapiTargets, xliffPart);
					}
				}
			}
		}
		return Collections.singletonList(event);
	}

	private List<Event> endGroup(Ending endGroup) {
		Event event = new Event(EventType.END_GROUP, null);
		return Collections.singletonList(event);
	}

	private List<Event> startGroup(StartGroup startGroup) {
		Property propertyId = startGroup.getProperty(ID);
		String propertyIdString = (propertyId == null) ? null : propertyId.getValue();

		StartGroupData startGroupData = new StartGroupData(propertyIdString);

		startGroupData.setId(startGroup.getId());
		startGroupData.setTranslate(startGroup.isTranslatable());

		Event event = new Event(EventType.START_GROUP, null, startGroupData);

		if (startGroup.getSkeleton() != null) {
			startGroupData.setMetadata(((MetadataSkeleton) startGroup.getSkeleton()).getMetaData());
		}

		PropertiesMapper.setGroupProperties(startGroup, startGroupData);
		NotesMapper.setNotes(startGroup, startGroupData);

		return Collections.singletonList(event);
	}

	private List<Event> startDocument(StartDocument startDocument, XLIFF2FilterWriter xliff2FilterWriter) {
		Event startDocumentEvent = new Event(EventType.START_DOCUMENT, null);
		LocaleId locale = startDocument.getLocale();

		Property xliffVersionProperty = startDocument.getProperty(XLIFF2PropertyStrings.VERSION);
		String xliffVersion = xliffVersionProperty != null ? xliffVersionProperty.getValue() : "2.0";

		StartXliffData startXliffData = new StartXliffData(xliffVersion);
		startXliffData.setSourceLanguage(startDocument.getLocale().toString());

		Event startXliffEvent = new Event(EventType.START_XLIFF, null, startXliffData);

		xliff2FilterWriter.initializeWriter(locale);

		PropertiesMapper.setStartXliffProperties(startDocument, startXliffData);

		return Arrays.asList(startDocumentEvent, startXliffEvent);
	}

	private List<Event> endDocument(Ending okapiEvent) {
		Event endXliffEvent = new Event(EventType.END_XLIFF, null);
		Event endDocumentEvent = new Event(EventType.END_DOCUMENT, null);
		return Arrays.asList(endXliffEvent, endDocumentEvent);
	}

	private List<Event> startSubDocument(StartSubDocument okapiStartSubDoc) {
		List<Event> events = new ArrayList<>();
		StartFileData startFileData = new StartFileData(okapiStartSubDoc.getId());
		startFileData.setOriginal(okapiStartSubDoc.getName());
		startFileData.setId(okapiStartSubDoc.getId());
		startFileData.setTranslate(okapiStartSubDoc.isTranslatable());
		startFileData.setOriginal(okapiStartSubDoc.getName());
		PropertiesMapper.setStartFileProperties(okapiStartSubDoc, startFileData);
		NotesMapper.setNotes(okapiStartSubDoc, startFileData);

		Event startFileEvent = new Event(EventType.START_FILE, null, startFileData);

		events.add(startFileEvent);

		return events;
	}

	private List<Event> endSubDocument() {
		Event event = new Event(EventType.END_FILE, null);
		return Collections.singletonList(event);
	}

	/**
	 * Copies the text and codes from Okapi Core {@link TextFragment} to an XLIFF
	 * Toolkit {@link Fragment}.
	 * 
	 * @param source          The text to read.
	 * @param out             The destination of the read text.
	 * @param existingCodeIds current list of code id's
	 */
	private void copyOver(TextFragment source, Fragment out, Set<String> existingCodeIds) {
		String codedText = source.getCodedText();

		int nextCodeIndex = -1;
		int nextCodePosition = -1;
		if (source.hasCode()) {
			nextCodeIndex = 0;
			nextCodePosition = source.getCodePosition(nextCodeIndex);
		}

		for (int i = 0; i < codedText.length(); i++) {
			char c = codedText.charAt(i);

			if (nextCodePosition == i) {
				char codePosition = codedText.charAt(i + 1);
				Code okapiCode = source.getCode(codePosition);
				TagType tagType = codeTagTypeMap.get(okapiCode.getTagType());
				String id = okapiCode.getOriginalId();

				if (id == null) {
					// use okapi code id with a warning
					id = String.valueOf(okapiCode.getId());
					logger.warn("Original code id was null in segment: {}. " + "Using Okapi code integer id instead:" +
							" {}",	source, id);
				}

				// Checks if Placeholder use the same ID as other placeholder in the text unit
				// THE XLIFF Toolkit writer should have done this check, but it doesn't. So we
				// have to prevent it from writing out invalid XLIFF 2.0
				if (tagType.equals(TagType.CLOSING) || tagType.equals(TagType.STANDALONE)) {
					if (existingCodeIds.contains(id)) {
						throw new OkapiMergeException("Tried writing placeholder to XLIFF 2 with the same ID as " +
								"another placeholder in the same text unit. Previous ID: " + okapiCode.getId() +
								" | XLIFF 2 ID: " + id + " | Placeholder: " + okapiCode);
					}
					existingCodeIds.add(id);
				}

				Tag xliff2Tag;

				// MTag
				if (okapiCode.hasProperty(XLIFF2PropertyStrings.MTAG)) {
					// set type later in setCodeProperties
					xliff2Tag = new MTag(id, null);
					// MTag can't be PLACEHOLDER only OPENING or CLOSING
					xliff2Tag.setTagType(okapiCode.getTagType() == TextFragment.TagType.OPENING ?
							TagType.OPENING : TagType.CLOSING);
					PropertiesMapper.setCodeProperties(okapiCode, (MTag)xliff2Tag);
				} else {
					xliff2Tag = new CTag(tagType, id, okapiCode.getData());
					((CTag) xliff2Tag).setCanCopy(okapiCode.isCloneable());
					((CTag) xliff2Tag).setCanDelete(okapiCode.isDeleteable());
					((CTag) xliff2Tag).setData(okapiCode.getData());
					((CTag) xliff2Tag).setDisp(okapiCode.getDisplayText());
					PropertiesMapper.setCodeProperties(okapiCode, (CTag)xliff2Tag);
				}

				out.append(xliff2Tag);
				nextCodeIndex += 1;
				nextCodePosition = source.getCodePosition(nextCodeIndex);
				i++;
			} else {
				out.append(c);
			}
		}
	}
}
