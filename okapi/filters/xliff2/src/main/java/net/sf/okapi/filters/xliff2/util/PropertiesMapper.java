/*===========================================================================
  Copyright (C) 2019 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
============================================================================*/

package net.sf.okapi.filters.xliff2.util;

import net.sf.okapi.common.Util;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import net.sf.okapi.filters.xliff2.model.XLIFF2PropertyStrings;
import net.sf.okapi.lib.xliff2.InvalidParameterException;
import net.sf.okapi.lib.xliff2.core.CTag;
import net.sf.okapi.lib.xliff2.core.CanReorder;
import net.sf.okapi.lib.xliff2.core.Directionality;
import net.sf.okapi.lib.xliff2.core.ExtAttributes;
import net.sf.okapi.lib.xliff2.core.IWithExtAttributes;
import net.sf.okapi.lib.xliff2.core.MTag;
import net.sf.okapi.lib.xliff2.core.Part;
import net.sf.okapi.lib.xliff2.core.Segment;
import net.sf.okapi.lib.xliff2.core.StartFileData;
import net.sf.okapi.lib.xliff2.core.StartGroupData;
import net.sf.okapi.lib.xliff2.core.StartXliffData;
import net.sf.okapi.lib.xliff2.core.TargetState;
import net.sf.okapi.lib.xliff2.core.Unit;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Saves parameters and other data from the XLIFF Toolkit into Okapi Core and
 * back. These properties are all saved as read only.
 * <p>
 * Since the mapping operation to and from should be equivalent, we put both
 * operations in here to make it easier to compare them. All the methods in here
 * have at least 2 parameters. The first parameter is where the data is being
 * read from, and the second parameter is where the data is being written to.
 */
public class PropertiesMapper {

	private static final Pattern EXTENDED_ATTRIBUTE_KEY_PATTERN = Pattern
			.compile("^" + Pattern.quote(XLIFF2PropertyStrings.EXTENDED_ATTRIBUTE_PREFIX) + "(.*)"
					+ Pattern.quote(XLIFF2PropertyStrings.EXTENDED_ATTRIBUTE_DELIMITER) + "\\{(.*?)\\}" + "(.*)$");

	/**
	 * Transfers properties from the XLIFF Toolkit StartXliffData to the Okapi Core
	 * StartDocument
	 * <p>
	 * Relates to &lt;xliff> elements in XLIFF 2.0 file
	 *
	 * @param xliffStartXliff    The properties to read from
	 * @param okapiStartDocument The properties to write to
	 */
	public static void setStartXliffProperties(StartXliffData xliffStartXliff, StartDocument okapiStartDocument) {
		setProperty(XLIFF2PropertyStrings.SRC_LANG, xliffStartXliff.getSourceLanguage(), okapiStartDocument);
		setProperty(XLIFF2PropertyStrings.TRG_LANG, xliffStartXliff.getTargetLanguage(), okapiStartDocument);
		setProperty(XLIFF2PropertyStrings.VERSION, xliffStartXliff.getVersion(), okapiStartDocument);
		setExtendedAttributes(xliffStartXliff, okapiStartDocument);
	}

	/**
	 * Transfers properties from the Okapi Core StartDocument to the XLIFF Toolkit
	 * StartXliffData
	 * <p>
	 * Relates to &lt;xliff> elements in XLIFF 2.0 file
	 *
	 * @param okapiStartDocument The properties to read from
	 * @param xliffStartXliff    The properties to write to
	 */
	public static void setStartXliffProperties(StartDocument okapiStartDocument, StartXliffData xliffStartXliff) {
		setExtendedAttributes(okapiStartDocument, xliffStartXliff);
	}

	/**
	 * Transfers properties from the XLIFF Toolkit StartFileData to the Okapi Core
	 * StartSubDocument
	 * <p>
	 * Relates to &lt;file> elements in XLIFF 2.0 file
	 *
	 * @param xliffStartFileData The properties to read from
	 * @param okapiStartSubDoc   The properties to write to
	 */
	public static void setStartFileProperties(StartFileData xliffStartFileData, StartSubDocument okapiStartSubDoc) {
		setProperty(XLIFF2PropertyStrings.SRC_DIR, xliffStartFileData.getSourceDir(), okapiStartSubDoc);
		setProperty(XLIFF2PropertyStrings.TRG_DIR, xliffStartFileData.getTargetDir(), okapiStartSubDoc);
		setProperty(XLIFF2PropertyStrings.CAN_RESEGMENT, xliffStartFileData.getCanResegment(), okapiStartSubDoc);
		setExtendedAttributes(xliffStartFileData, okapiStartSubDoc);
	}

	/**
	 * Transfers properties from the Okapi Core StartSubDocument to the XLIFF
	 * Toolkit StartFileData
	 * <p>
	 * Relates to &lt;file> elements in XLIFF 2.0 file
	 *
	 * @param xliffStartFileData The properties to read from
	 * @param okapiStartSubDoc   The properties to write to
	 */
	public static void setStartFileProperties(StartSubDocument okapiStartSubDoc, StartFileData xliffStartFileData) {
		xliffStartFileData
				.setSourceDir(stringToDirection(getProperty(XLIFF2PropertyStrings.SRC_DIR, okapiStartSubDoc)));
		xliffStartFileData
				.setTargetDir(stringToDirection(getProperty(XLIFF2PropertyStrings.TRG_DIR, okapiStartSubDoc)));
		xliffStartFileData
				.setCanResegment(stringToBoolean(getProperty(XLIFF2PropertyStrings.CAN_RESEGMENT, okapiStartSubDoc)));

		setExtendedAttributes(okapiStartSubDoc, xliffStartFileData);
	}

	/**
	 * Transfers properties from the XLIFF 2.0 Start Group Data to the Okapi Core
	 * Start Group
	 *
	 * @param xliffStartGroupData The properties to read from
	 * @param okapiStartGroup     The properties to write to
	 */
	public static void setGroupProperties(StartGroupData xliffStartGroupData, StartGroup okapiStartGroup) {
		setProperty(XLIFF2PropertyStrings.CAN_RESEGMENT, xliffStartGroupData.getCanResegment(), okapiStartGroup);
		setProperty(XLIFF2PropertyStrings.SRC_DIR, xliffStartGroupData.getSourceDir(), okapiStartGroup);
		setProperty(XLIFF2PropertyStrings.TRG_DIR, xliffStartGroupData.getTargetDir(), okapiStartGroup);
		setExtendedAttributes(xliffStartGroupData, okapiStartGroup);
	}

	/**
	 * Transfers properties from the Okapi Core Start Group to the XLIFF 2.0 Start
	 * Group Data
	 *
	 * @param xliffStartGroupData The properties to write to
	 * @param okapiStartGroup     The properties to read from
	 */
	public static void setGroupProperties(StartGroup okapiStartGroup, StartGroupData xliffStartGroupData) {
		final String canResegment = getProperty(XLIFF2PropertyStrings.CAN_RESEGMENT, okapiStartGroup);
		if (canResegment != null) {
			xliffStartGroupData.setCanResegment(stringToBoolean(canResegment));
		}
		xliffStartGroupData
				.setSourceDir(stringToDirection(getProperty(XLIFF2PropertyStrings.SRC_DIR, okapiStartGroup)));
		xliffStartGroupData
				.setTargetDir(stringToDirection(getProperty(XLIFF2PropertyStrings.TRG_DIR, okapiStartGroup)));
		xliffStartGroupData.setType(okapiStartGroup.getType());
		setExtendedAttributes(okapiStartGroup, xliffStartGroupData);
		xliffStartGroupData.setName(okapiStartGroup.getName());
	}

	/**
	 * Transfers properties from the XLIFF Toolkit TextUnit to the Okapi Core
	 * TextUnit
	 * <p>
	 * Relates to &lt;unit> elements in XLIFF 2.0 file
	 *
	 * @param unit The properties to read from
	 * @param tu   The properties to write to
	 */
	public static void setTextUnitProperties(Unit unit, ITextUnit tu) {
		setProperty(XLIFF2PropertyStrings.NAME, unit.getName(), tu);
		setProperty(XLIFF2PropertyStrings.CAN_RESEGMENT, unit.getCanResegment(), tu);
		setProperty(XLIFF2PropertyStrings.TYPE, unit.getType(), tu);
		setProperty(XLIFF2PropertyStrings.SRC_DIR, unit.getSourceDir(), tu);
		setProperty(XLIFF2PropertyStrings.TRG_DIR, unit.getTargetDir(), tu);

		setExtendedAttributes(unit, tu);
	}

	/**
	 * Transfers properties from the Okapi Core TextUnit to the XLIFF Toolkit
	 * TextUnit
	 * <p>
	 * Relates to &lt;unit> elements in XLIFF 2.0 file
	 *
	 * @param okapiTextUnit The properties to read from
	 * @param xliffTextUnit The properties to write to
	 */
	public static void setTextUnitProperties(ITextUnit okapiTextUnit, Unit xliffTextUnit) {
		final String canResegment = getProperty(XLIFF2PropertyStrings.CAN_RESEGMENT, okapiTextUnit);
		if (canResegment != null) {
			xliffTextUnit.setCanResegment(stringToBoolean(canResegment));
		}

		final String sourceDirection = getProperty(XLIFF2PropertyStrings.SRC_DIR, okapiTextUnit);
		if (sourceDirection != null) {
			xliffTextUnit.setTargetDir(stringToDirection(sourceDirection));
		}

		final String targetDirection = getProperty(XLIFF2PropertyStrings.TRG_DIR, okapiTextUnit);
		if (targetDirection != null) {
			xliffTextUnit.setSourceDir(stringToDirection(targetDirection));
		}

		setExtendedAttributes(okapiTextUnit, xliffTextUnit);

	}

	/**
	 * Transfers properties from the XLIFF Toolkit Part to the Okapi Core TextPart
	 * <p>
	 * Relates to &lt;segment> and &lt;ignorable> elements in XLIFF 2.0 file
	 *
	 * @param xliffPart The properties to write to
	 * @param okapiPart The properties to read from
	 */
	public static void setPartProperties(Part xliffPart, net.sf.okapi.common.resource.TextPart okapiPart) {
		if (xliffPart.isSegment()) {
			final Segment xliffSegment = (Segment) xliffPart;
			setProperty(XLIFF2PropertyStrings.CAN_RESEGMENT, xliffSegment.getCanResegment(), okapiPart);
			setProperty(XLIFF2PropertyStrings.STATE, xliffSegment.getState(), okapiPart);
			setProperty(XLIFF2PropertyStrings.SUB_STATE, xliffSegment.getSubState(), okapiPart);

		}
	}

	/**
	 * Transfers properties from the Okapi Core TextPart to the XLIFF Toolkit Part
	 * <p>
	 * Relates to &lt;segment> and &lt;ignorable> elements in XLIFF 2.0 file
	 *  @param okapiPart The properties to read from
	 * @param xliffPart       The properties to write to
	 */
	public static void setPartProperties(TextPart okapiPart, Part xliffPart) {
		if (xliffPart.isSegment()) {
			final Segment xliffSegment = (Segment) xliffPart;
			final net.sf.okapi.common.resource.Segment okapiSegment = (net.sf.okapi.common.resource.Segment) okapiPart;

			final String canResegment = getProperty(XLIFF2PropertyStrings.CAN_RESEGMENT, okapiSegment);
			if (!Util.isEmpty(canResegment)) {
				xliffSegment.setCanResegment(stringToBoolean(canResegment));
			}

			final String state = getProperty(XLIFF2PropertyStrings.STATE, okapiSegment);
			if (!Util.isEmpty(state)) {
				xliffSegment.setState(stringToTargetState(state));
			}

			final String substate = getProperty(XLIFF2PropertyStrings.SUB_STATE, okapiSegment);
			if (!Util.isEmpty(substate)) {
				xliffSegment.setSubState(substate);
			}
		}
	}

	public static void setStateProperty(TextContainer okapiContainer, Part xliffPart) {
		if (xliffPart.isSegment()) {
			final Segment xliffSegment = (Segment) xliffPart;
			final String state = getProperty(XLIFF2PropertyStrings.STATE, okapiContainer);
			if (!Util.isEmpty(state)) {
				xliffSegment.setState(stringToTargetState(state));
			}
		}
	}

	/**
	 * Transfers properties from the Okapi Core Code Tag to the XLIFF Toolkit Code
	 * Tag
	 * <p>
	 * Relates to &lt;ph>, &lt;pc>, and &lt;sc> elements in XLIFF 2.0 file
	 *
	 * @param okapiCode  The properties to read from
	 * @param xliff2Ctag The properties to write to
	 */
	public static void setCodeProperties(Code okapiCode, CTag xliff2Ctag) {
		if (okapiCode.hasProperty(XLIFF2PropertyStrings.CAN_COPY)) {
			xliff2Ctag.setCanCopy(stringToBoolean(okapiCode.getProperty(XLIFF2PropertyStrings.CAN_COPY).getValue()));
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.CAN_DELETE)) {
			xliff2Ctag.setCanDelete(stringToBoolean(okapiCode.getProperty(XLIFF2PropertyStrings.CAN_DELETE).getValue()));
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.CAN_REORDER)) {
			xliff2Ctag.setCanReorder(stringToCanReorder(okapiCode.getProperty(XLIFF2PropertyStrings.CAN_REORDER).getValue()));
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.CAN_OVERLAP)) {
			xliff2Ctag.setCanOverlap(
					stringToBoolean(okapiCode.getProperty(XLIFF2PropertyStrings.CAN_OVERLAP).getValue()));
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.SUB_FLOWS)) {
			xliff2Ctag.setSubFlows(okapiCode.getProperty(XLIFF2PropertyStrings.SUB_FLOWS).getValue());
		}

		// Can't use Okapi Code.type as we use that to mark other information like Code.TYPE_ANNOTATION_ONLY
		if (okapiCode.hasProperty(XLIFF2PropertyStrings.TYPE)) {
			String type = okapiCode.getProperty(XLIFF2PropertyStrings.TYPE).getValue();
			if (type != null && !okapiCode.getType().equals(Code.TYPE_NULL))
				try {
					// remove xliff 1.2 prefix
					if (type.startsWith("x-")) {
						type = type.substring(2);
					}
					xliff2Ctag.setType(type);
				} catch (InvalidParameterException e) {
					LoggerFactory.getLogger(PropertiesMapper.class).debug("Could net set MTag type of {}: {}", xliff2Ctag, e.getMessage());
				}
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.SUB_TYPE)) {
			xliff2Ctag.setSubType(okapiCode.getProperty(XLIFF2PropertyStrings.SUB_TYPE).getValue());
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.DISP)) {
			xliff2Ctag.setDisp(okapiCode.getProperty(XLIFF2PropertyStrings.DISP).getValue());
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.DIR)) {
			xliff2Ctag.setDir(stringToDirection(okapiCode.getProperty(XLIFF2PropertyStrings.DIR).getValue()));
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.DATA_DIR)) {
			xliff2Ctag.setDataDir(stringToDirection(okapiCode.getProperty(XLIFF2PropertyStrings.DATA_DIR).getValue()));
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.DATA_REF)) {
			xliff2Ctag.setDataRef(okapiCode.getProperty(XLIFF2PropertyStrings.DATA_REF).getValue());
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.EQUIV)) {
			xliff2Ctag.setEquiv(okapiCode.getProperty(XLIFF2PropertyStrings.EQUIV).getValue());
		}

		setExtendedAttributes(okapiCode, xliff2Ctag);
	}

	/**
	 * Transfers properties from the Okapi Core Code Tag to the XLIFF Toolkit Code
	 * Tag
	 * <p>
	 * Relates to &lt;ph>, &lt;pc>, and &lt;sc> elements in XLIFF 2.0 file
	 *
	 * @param okapiCode  The properties to read from
	 * @param xliff2Mtag The properties to write to
	 */
	public static void setCodeProperties(Code okapiCode, MTag xliff2Mtag) {
		if (okapiCode.hasProperty(XLIFF2PropertyStrings.TRANSLATE)) {
			xliff2Mtag.setTranslate(stringToBoolean(okapiCode.getProperty(XLIFF2PropertyStrings.TRANSLATE).getValue()));
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.VALUE)) {
			xliff2Mtag.setValue(okapiCode.getProperty(XLIFF2PropertyStrings.VALUE).getValue());
		}

		if (okapiCode.hasProperty(XLIFF2PropertyStrings.REF)) {
			xliff2Mtag.setRef(okapiCode.getProperty(XLIFF2PropertyStrings.REF).getValue());
		}

		// Can't use Okapi Code.type as we use that to mark other information like Code.TYPE_ANNOTATION_ONLY
		if (okapiCode.hasProperty(XLIFF2PropertyStrings.TYPE)) {
			String type = okapiCode.getProperty(XLIFF2PropertyStrings.TYPE).getValue();
			if (type != null && !okapiCode.getType().equals(Code.TYPE_NULL))
			try {
				// remove xliff 1.2 prefix
				if (type.startsWith("x-")) {
					type = type.substring(2);
				}
				xliff2Mtag.setType(type);
			} catch (InvalidParameterException e) {
				LoggerFactory.getLogger(PropertiesMapper.class).debug("Could net set MTag type of {}: {}", xliff2Mtag, e.getMessage());
			}
		}

		setExtendedAttributes(okapiCode, xliff2Mtag);
	}

	/**
	 * Transfers properties from the XLIFF Toolkit Code Tag to the Okapi Core Code
	 * Tag.
	 * <p>
	 * Relates to &lt;ph>, &lt;pc>, and &lt;sc> elements in XLIFF 2.0 file
	 *
	 * @param okapiCode  The properties to read from
	 * @param xliff2Ctag The properties to write to
	 */
	public static void setCodeProperties(CTag xliff2Ctag, Code okapiCode) {
		okapiCode.setProperty(new Property(XLIFF2PropertyStrings.CAN_DELETE, booleanToString(xliff2Ctag.getCanDelete())));

		okapiCode.setProperty(new Property(XLIFF2PropertyStrings.CAN_COPY, booleanToString(xliff2Ctag.getCanCopy())));

		okapiCode.setProperty(new Property(XLIFF2PropertyStrings.CAN_OVERLAP, booleanToString(xliff2Ctag.getCanOverlap())));

		if (xliff2Ctag.getCanReorder() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.CAN_REORDER, xliff2Ctag.getCanReorder().toString()));
		}

		if (xliff2Ctag.getSubFlows() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.SUB_FLOWS, xliff2Ctag.getSubFlows()));
		}

		if (xliff2Ctag.getDisp() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.DISP, xliff2Ctag.getDisp()));
		}

		if (xliff2Ctag.getDir() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.DIR, directionToString(xliff2Ctag.getDir())));
		}

		if (xliff2Ctag.getDataDir() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.DATA_DIR, directionToString(xliff2Ctag.getDataDir())));
		}

		if (xliff2Ctag.getDataRef() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.DATA_REF, xliff2Ctag.getDataRef()));
		}

		if (xliff2Ctag.getEquiv() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.EQUIV, xliff2Ctag.getEquiv()));
		}

		// Can't use Okapi Code.type as we use that to mark other information like Code.TYPE_ANNOTATION_ONLY
		if (xliff2Ctag.getType() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.TYPE, xliff2Ctag.getType()));
		}

		// subType must also have type
		if (xliff2Ctag.getSubType() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.SUB_TYPE, xliff2Ctag.getSubType()));
		}

		setExtendedAttributes(xliff2Ctag, okapiCode);
	}

	public static void setCodeProperties(MTag xliff2Mtag, Code okapiCode) {
		// remember this is an MTag not CTag
		okapiCode.setProperty(new Property(XLIFF2PropertyStrings.MTAG, xliff2Mtag.getType()));

		if (xliff2Mtag.getTranslate() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.TRANSLATE, booleanToString(xliff2Mtag.getTranslate())));
		}

		if (xliff2Mtag.getValue() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.VALUE, xliff2Mtag.getValue()));
		}

		if (xliff2Mtag.getRef() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.REF, xliff2Mtag.getRef()));
		}

		// Can't use Okapi Code.type as we use that to mark other information like Code.TYPE_ANNOTATION_ONLY
		if (xliff2Mtag.getType() != null) {
			okapiCode.setProperty(new Property(XLIFF2PropertyStrings.TYPE, xliff2Mtag.getType()));
		}

		setExtendedAttributes(xliff2Mtag, okapiCode);
	}

		/**
         * Transfers extended attributes from the XLIFF Toolkit to the Okapi Core
         * <p>
         * This relates to the &lt;code> elements, since their attributes have to be
         * stored differently.
         *
         * @param xliffElement The element to read from
         * @param code         The Code to write to
         */
	private static void setExtendedAttributes(IWithExtAttributes xliffElement, Code code) {
		final ExtAttributes extAttributes = xliffElement.getExtAttributes();
		final Set<String> namespaces = extAttributes.getNamespaces();
		namespaces.forEach(namespaceURI -> {
			final String propertyName = XLIFF2PropertyStrings.EXTENDED_NAMESPACE_PREFIX + namespaceURI;
			final String value = extAttributes.getNamespacePrefix(namespaceURI);
			code.setProperty(new Property(propertyName, value));

		});

		extAttributes.forEach(extAttribute -> {
			// Most of the data is stored in the key, to reduce the chance of collisions.
			final String propertyName = XLIFF2PropertyStrings.EXTENDED_ATTRIBUTE_PREFIX + extAttribute.getPrefix()
					+ XLIFF2PropertyStrings.EXTENDED_ATTRIBUTE_DELIMITER + extAttribute.getQName();
			final String value = extAttribute.getValue();
			code.setProperty(new Property(propertyName, value));
		});
	}

	/**
	 * Transfers extended attributes from the Okapi Core to the XLIFF Toolkit
	 * <p>
	 * This relates to the &lt;code> elements, since their attributes have to be
	 * stored differently.
	 *
	 * @param code         The Code to read from
	 * @param xliffElement The element to write to
	 */
	private static void setExtendedAttributes(Code code, IWithExtAttributes xliffElement) {
		final ExtAttributes extAttributes = xliffElement.getExtAttributes();

		code.getAnnotationsTypes().stream().filter(p -> p.startsWith(XLIFF2PropertyStrings.EXTENDED_NAMESPACE_PREFIX))
				.forEach(annotationName -> {
					final String namespaceURI = annotationName.replace(XLIFF2PropertyStrings.EXTENDED_NAMESPACE_PREFIX,
							"");
					final String prefix = code.getProperty(annotationName).getValue();
					extAttributes.setNamespace(prefix, namespaceURI);
				});

		code.getAnnotationsTypes().stream().filter(p -> p.startsWith(XLIFF2PropertyStrings.EXTENDED_ATTRIBUTE_PREFIX))
				.forEach(annotationName -> {
					final Matcher matcher = EXTENDED_ATTRIBUTE_KEY_PATTERN.matcher(annotationName);
					final boolean found = matcher.find();
					if (found) {
						final String namespaceURI = matcher.group(2);
						final String localeName = matcher.group(3);
						final String value = code.getProperty(annotationName).getValue();
						extAttributes.setAttribute(namespaceURI, localeName, value);
					} else {
						LoggerFactory.getLogger(PropertiesMapper.class)
								.warn("Could not find extended attribute information from {}", annotationName);
					}
				});
	}

	/**
	 * Transfers extended attributes from the XLIFF Toolkit to the Okapi Core.
	 * <p>
	 * This relates to any attribute which is not part of the XLIFF Toolkit spec,
	 * and therefore can affect any element.
	 *
	 * @param xliffElement    The attributes to read from
	 * @param okapiProperties The properties to write to
	 */
	private static void setExtendedAttributes(IWithExtAttributes xliffElement, IWithProperties okapiProperties) {
		final ExtAttributes extAttributes = xliffElement.getExtAttributes();
		final Set<String> namespaces = extAttributes.getNamespaces();
		namespaces.forEach(namespaceURI -> {
			final String propertyName = XLIFF2PropertyStrings.EXTENDED_NAMESPACE_PREFIX + namespaceURI;
			final String value = extAttributes.getNamespacePrefix(namespaceURI);
			setProperty(propertyName, value, okapiProperties);

		});

		extAttributes.forEach(extAttribute -> {
			// Most of the data is stored in the key, to reduce the chance of collisions.
			final String propertyName = XLIFF2PropertyStrings.EXTENDED_ATTRIBUTE_PREFIX + extAttribute.getPrefix()
					+ XLIFF2PropertyStrings.EXTENDED_ATTRIBUTE_DELIMITER + extAttribute.getQName();
			final String value = extAttribute.getValue();
			setProperty(propertyName, value, okapiProperties);
		});
	}

	/**
	 * Transfers extended attributes from the Okapi Core to the XLIFF Toolkit
	 * <p>
	 * This relates to any attribute which is not part of the XLIFF Toolkit spec,
	 * and therefore can affect any element.
	 *
	 * @param xliffElement    The properties to write to
	 * @param okapiProperties The attributes to read from
	 */
	private static void setExtendedAttributes(IWithProperties okapiProperties, IWithExtAttributes xliffElement) {
		final ExtAttributes extAttributes = xliffElement.getExtAttributes();

		okapiProperties.getPropertyNames().stream()
				.filter(p -> p.startsWith(XLIFF2PropertyStrings.EXTENDED_NAMESPACE_PREFIX)).forEach(propertyName -> {
					final String namespaceURI = propertyName.replace(XLIFF2PropertyStrings.EXTENDED_NAMESPACE_PREFIX,
							"");
					final String prefix = getProperty(propertyName, okapiProperties);
					extAttributes.setNamespace(prefix, namespaceURI);
				});

		okapiProperties.getPropertyNames().stream()
				.filter(p -> p.startsWith(XLIFF2PropertyStrings.EXTENDED_ATTRIBUTE_PREFIX)).forEach(propertyName -> {
					final Matcher matcher = EXTENDED_ATTRIBUTE_KEY_PATTERN.matcher(propertyName);
					final boolean found = matcher.find();
					if (found) {
						final String namespaceURI = matcher.group(2);
						final String localeName = matcher.group(3);
						final String value = getProperty(propertyName, okapiProperties);
						extAttributes.setAttribute(namespaceURI, localeName, value);
					} else {
						LoggerFactory.getLogger(PropertiesMapper.class)
								.warn("Could not find extended attribute information from {}", propertyName);
					}
				});
	}

	/**
	 * This is almost the same as {@link IWithProperties#getProperty(String)}, but
	 * will also get the value too. This way, you don't have to perform a null check
	 * on the property.
	 *
	 * @param key                 The key that the property is stored under
	 * @param eventWithProperties The event which has the properties
	 * @return The String value of the property, or null if the property wasn't
	 *         found.
	 */
	private static String getProperty(String key, IWithProperties eventWithProperties) {
		final Property property = eventWithProperties.getProperty(key);
		if (property == null) {
			return null;
		}
		return property.getValue();
	}

	// Ensure that the conversion to String happens with the conversion methods

	private static void setProperty(String key, String value, IWithProperties eventWithProperties) {
		eventWithProperties.setProperty(new Property(key, value, true));
	}

	private static void setProperty(String key, boolean value, IWithProperties eventWithProperties) {
		eventWithProperties.setProperty(new Property(key, booleanToString(value), true));
	}

	private static void setProperty(String key, Directionality value, IWithProperties eventWithProperties) {
		eventWithProperties.setProperty(new Property(key, directionToString(value), true));
	}

	private static void setProperty(String key, TargetState value, IWithProperties eventWithProperties) {
		eventWithProperties.setProperty(new Property(key, targetStateToString(value), true));
	}

	// Okapi Core only allows storing Properties as strings. So to ensure that no
	// data converted improperly, the
	// conversion happens in these methods.

	private static boolean stringToBoolean(String value) {
		return "yes".equals(value);
	}

	private static String booleanToString(boolean value) {
		return value ? "yes" : "no";
	}

	private static Directionality stringToDirection(String value) {
		if (value == null)
			return Directionality.AUTO;
		if ("NOT ALLOWED".equals(value))
			return Directionality.INHERITED;
		return Directionality.valueOf(value.toUpperCase());
	}
	
	private static CanReorder stringToCanReorder(String value) {
		if (value == null)
			return CanReorder.YES;
		return CanReorder.valueOf(value.toUpperCase());
	}

	private static String directionToString(Directionality value) {
		return value.toString();
	}

	private static TargetState stringToTargetState(String value) {
		switch (XLIFFFilter.State.fromString(value)) {
			case TRANSLATED:
				return TargetState.TRANSLATED;
			case SIGNED_OFF:
				return TargetState.REVIEWED;
			case FINAL:
				return TargetState.FINAL;
			case DEFAULT:
			default:
				return TargetState.INITIAL;
		}
	}

	private static String targetStateToString(TargetState value) {
		switch (value) {
			case TRANSLATED:
				return XLIFFFilter.State.TRANSLATED.toString();
			case REVIEWED:
				return XLIFFFilter.State.SIGNED_OFF.toString();
			case FINAL:
				return XLIFFFilter.State.FINAL.toString();
			case INITIAL:
			default:
				return XLIFFFilter.State.DEFAULT.toString();
		}
	}
}
