/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml.ui;

import net.sf.okapi.filters.idml.Parameters;
import org.assertj.core.api.Assertions;
import org.eclipse.swt.widgets.Shell;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ResourceBundle;

@RunWith(JUnit4.class)
public class InputsTest {
    @Test @Ignore
    public void configured() {
        final Parameters expectedParams = new Parameters();
        expectedParams.fromString(
            "#v1\n" +
                "maxAttributeSize.i=4444444\n" +
                "untagXmlStructures.b=true\n" +
                "extractNotes.b=true\n" +
                "extractMasterSpreads.b=true\n" +
                "extractHiddenLayers.b=true\n" +
                "extractHiddenPasteboardItems.b=true\n" +
                "skipDiscretionaryHyphens.b=true\n" +
                "extractBreaksInline.b=true\n" +
                "extractHyperlinkTextSourcesInline.b=false\n" +
                "extractCustomTextVariables.b=true\n" +
                "extractIndexTopics.b=true\n" +
                "extractExternalHyperlinks.b=true\n" +
                "ignoreCharacterKerning.b=true\n" +
                "ignoreCharacterTracking.b=true\n" +
                "ignoreCharacterLeading.b=true\n" +
                "ignoreCharacterBaselineShift.b=true\n" +
                "characterKerningMinIgnoranceThreshold=-15.15\n" +
                "characterKerningMaxIgnoranceThreshold=15.0\n" +
                "characterTrackingMinIgnoranceThreshold=\n" +
                "characterTrackingMaxIgnoranceThreshold=25.25\n" +
                "characterLeadingMinIgnoranceThreshold=4.2\n" +
                "characterLeadingMaxIgnoranceThreshold=\n" +
                "characterBaselineShiftMinIgnoranceThreshold=0.1\n" +
                "characterBaselineShiftMaxIgnoranceThreshold=0.2\n" +
                "specialCharacterPattern=\u2028|\u200B\n" +
                "fontMappings.0.sourceLocalePattern=en\n" +
                "fontMappings.0.targetLocalePattern=ru\n" +
                "fontMappings.0.sourceFontPattern=Times.*\n" +
                "fontMappings.0.targetFont=Arial\n" +
                "fontMappings.1.sourceLocalePattern=en\n" +
                "fontMappings.1.targetLocalePattern=ru\n" +
                "fontMappings.1.sourceFontPattern=Arial\n" +
                "fontMappings.1.targetFont=Times New Roman\n" +
                "fontMappings.number.i=2\n"
        );
        final Shell shell = new Shell();
        final Inputs inputs = new Inputs.Default(
            ResourceBundle.getBundle("net.sf.okapi.filters.idml.ui.Inputs"),
            shell
        );
        inputs.configureFrom(expectedParams);
        final Parameters actualParams = new Parameters();
        inputs.saveTo(actualParams);
        shell.close();
        Assertions.assertThat(actualParams.toString()).isEqualTo(expectedParams.toString());
    }
}
