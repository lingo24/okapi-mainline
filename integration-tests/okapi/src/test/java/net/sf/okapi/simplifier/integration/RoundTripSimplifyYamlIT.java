package net.sf.okapi.simplifier.integration;

import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.SimplifyRoundTripIT;
import net.sf.okapi.filters.yaml.YamlFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripSimplifyYamlIT extends SimplifyRoundTripIT {
	private static final String CONFIG_ID = "okf_yaml";
	private static final String DIR_NAME = "/yaml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".yml", ".yaml");
	private static final String XLIFF_EXTRACTED_EXTENSION = ".simplify_xliff";

	public RoundTripSimplifyYamlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XLIFF_EXTRACTED_EXTENSION, YamlFilter::new);
		addKnownFailingFile("unknown-tags-example.yaml");
		addKnownFailingFile("no-children-1-pretty.yaml");
		if ("\r\n".equals(System.lineSeparator())) {
			addKnownFailingFile("example2_14.yaml");
			addKnownFailingFile("example2_18.yaml");
			addKnownFailingFile("example2_27.yaml");
			addKnownFailingFile("example2_28.yaml");
			addKnownFailingFile("plain_wrapped.yml");
		}
	}

	@Test
	public void yamlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.Utf8FilePerLineComparator());
	}

	@Test
	public void yamlFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.Utf8FilePerLineComparator());
	}
}
