package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.ts.TsFilter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripTsIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_ts";
	private static final String DIR_NAME = "/ts/";
	private static final List<String> EXTENSIONS = Arrays.asList(".ts");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = TsFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripMarkdownIT.class);

	public RoundTripTsIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);
		addKnownFailingFile("issue531.ts");
		addKnownFailingFile("Test_nautilus.af.ts");
	}

	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/ts/tstest.ts").asFile();
		runTest(new TestJob(CONFIG_ID, false, file, null, null,
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void tsFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(true, new FileComparator.EventComparator());
	}

	@Test
	public void tsSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(true, new FileComparator.EventComparator());
	}
}
