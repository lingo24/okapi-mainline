package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.tex.TEXFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripTexIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_tex";
	private static final String DIR_NAME = "/tex/";
	private static final List<String> EXTENSIONS = Arrays.asList(".tex");

	public RoundTripTexIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, TEXFilter::new);
	}

	@Test
	public void texFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void texSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(CONFIG_ID, false, new FileComparator.EventComparator());
	}
}
