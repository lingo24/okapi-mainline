package net.sf.okapi.roundtrip.integration;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.XmlOrTextRoundTripIT;
import net.sf.okapi.filters.transtable.TransTableFilter;

@RunWith(JUnit4.class)
public class RoundTripTranstableIT extends XmlOrTextRoundTripIT {
	private static final String CONFIG_ID = "okf_transtable";
	private static final String DIR_NAME = "/transtable/";
	private static final List<String> EXTENSIONS = Arrays.asList(".txt");

	public RoundTripTranstableIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, TransTableFilter::new);
		addKnownFailingFile("test01.xml.txt");
	}

	@Test
	public void transtableFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.Utf8FilePerLineComparator());
	}

	@Test
	public void transtableSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(CONFIG_ID, false, new FileComparator.Utf8FilePerLineComparator());
	}
}
