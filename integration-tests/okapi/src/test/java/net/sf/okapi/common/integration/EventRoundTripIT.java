package net.sf.okapi.common.integration;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileUtil;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.resource.RawDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Supplier;

import static org.junit.Assert.assertTrue;

/**
 * @author jimh
 *
 */
public class EventRoundTripIT extends BaseRoundTripIT {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * @param configId
	 * @param dirName
	 * @param extensions
	 */
	public EventRoundTripIT(final String configId, final String dirName,
			final List<String> extensions, Supplier<IFilter> filterConstructor) {
		this(/*parallel*/ true, configId, dirName, extensions, filterConstructor);
	}

	public EventRoundTripIT(boolean parallel, final String configId, final String dirName,
			final List<String> extensions, Supplier<IFilter> filterConstructor) {
		super(parallel, configId, dirName, extensions, filterConstructor);
	}

	/**
	 * @param configId
	 * @param dirName
	 * @param extensions
	 * @param defaultTargetLocale
	 */
	public EventRoundTripIT(final String configId, final String dirName, final List<String> extensions,
			final LocaleId defaultTargetLocale, Supplier<IFilter> filterConstructor) {
		this(/*parallel*/ true, configId, dirName, extensions, defaultTargetLocale, filterConstructor);
	}

	public EventRoundTripIT(boolean parallel, final String configId, final String dirName, final List<String> extensions,
			final LocaleId defaultTargetLocale, Supplier<IFilter> filterConstructor) {
		super(parallel, configId, dirName, extensions, defaultTargetLocale, filterConstructor);
	}

	@Override
	public void runTest(final TestJob testJob) {
		final String f = testJob.file.getName();
		final String root = testJob.file.getParent() + File.separator;
		final String original = root + f;
		final String tkitMerged = root + f + ".tkitMerged";
		final String golden = root + f + ".golden";
		final String golden_xliff = root + f + ".golden_xliff";
		final String golden_ser = root + f + ".golden_ser";

		String extractedOutput = root + f + xliffExtractedExtension;
		extractedOutput = isSerializedOutput() ? root + f + serializedExtractedExtension : extractedOutput;
		LocaleId source = LocaleId.ENGLISH;
		LocaleId target = defaultTargetLocale;
		if (testJob.detectLocales) {
			final List<String> locales = FileUtil.guessLanguages(testJob.file.getAbsolutePath());
			if (locales.size() >= 1) {
				source = LocaleId.fromString(locales.get(0));
			}
			if (locales.size() >= 2) {
				target = LocaleId.fromString(locales.get(1));
			}
		}

		try {
			logger.info(f);
			IFilterConfigurationMapper mapper = RoundTripUtils.extract(source, target, original, extractedOutput,
					testJob.configId, testJob.customConfigPath, isSerializedOutput());

			// if there is a golden xliff file use that with the merge
			final String extracted;
			if (Paths.get(golden_xliff).toFile().isFile() && !isSerializedOutput()) {
				extracted = golden_xliff;
			} else if (Paths.get(golden_ser).toFile().isFile() && isSerializedOutput()) {
				extracted = golden_ser;
			} else {
				extracted = extractedOutput;
			}
			RoundTripUtils.merge(source, target, original, extracted,
					tkitMerged, testJob.configId, isSerializedOutput());

			// if there is a golden output file compare that as there may
			// be legitimate changes
			final String expected;
			if (Paths.get(golden).toFile().isFile()) {
				expected = golden;
			} else {
				expected = original;
			}
			final List<Event> e = loadEventsFromFile(expected, testJob, mapper, source, target);
			final List<Event> a = loadEventsFromFile(tkitMerged, testJob, mapper, source, target);
			assertTrue("Compare Events: " + f, testJob.comparator.compare(a, e));
		} catch (final Throwable e) {
			if (!knownFailingFiles.contains(f)) {
				errCol.addError(new OkapiTestException(f, e));
				logger.error("Failing test: {}\n{}", f, e.getMessage());
			} else {
				logger.info("Ignored known failing file: {}", f);
			}
		}
	}

	protected List<Event> loadEventsFromFile(String fileName, TestJob testJob, IFilterConfigurationMapper mapper,
											 LocaleId sourceLocale, LocaleId targetLocale) {
		try (RawDocument trd = new RawDocument(Util.toURI(fileName), "UTF-8", sourceLocale, targetLocale)) {
			try (IFilter filter = testJob.constructor.get()) {
				filter.setFilterConfigurationMapper(mapper);
				IParameters params = mapper.getParameters(mapper.getConfiguration(testJob.configId));
				return IntegrationtestUtils.getEvents(filter, trd, params);
			}
		}
		
	}
}
